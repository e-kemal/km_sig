include "km_dscp_post.gs"

class kmDscpKu isclass kmDscpPost {
	void loadObjects() {
		stationName = "Курортная";
		stationCode = "Ku";

		SceneryWithTrack J;
		int i;
		for (i = 0; i < 2; ++i) {
			J = cast<SceneryWithTrack>Router.GetGameObject(Router.SerialiseGameObjectIDFromString("Ku_" + (i + 1)));
			junctions[i] = new kmJunctionInfo();
			junctions[i].JM = J.GetAttachedJunctions()[0];
			junctions[i].name = (i + 1);
			J.SetFXNameText("number", i + 1);
		}
		junctions[0].defaultDirection = JunctionBase.DIRECTION_LEFT;
		junctions[1].defaultDirection = JunctionBase.DIRECTION_LEFT;

		kmRC tmp = null;
		GSTrackSearch ts = junctions[1].JM.BeginTrackSearch(JunctionBase.DIRECTION_RIGHT);
		while (ts.SearchNextObject() and !(tmp = cast<kmRC>ts.GetObject()));

		kmSection section = new kmSection();
		section.name = "1";
		section.endObject = tmp;
		section.endDir = true;
		section.sectionCount = 1;
		section.postSectionOpenCount = 0;
		section.junctions[0] = junctions[1];
		sections[0] = section;

		section = new kmSection();
		section.name = "1с";
		section.endObject = cast<kmRC>Router.GetGameObject(Router.SerialiseGameObjectIDFromString("Ku_D"));
		section.endDir = false;
		section.sectionCount = 2;
		section.postSectionOpenCount = 0;
		section.junctions[0] = junctions[0];
		sections[1] = section;

		section = new kmSection();
		section.name = "Д";
		section.endObject = cast<kmRC>Router.GetGameObject(Router.SerialiseGameObjectIDFromString("Ku_V"));
		section.endDir = false;
		section.sectionCount = 2;
		section.postSectionOpenCount = 0;
		section.junctions[0] = junctions[0];
		section.junctions[1] = junctions[1];
		sections[2] = section;

		section = new kmSection();
		section.name = "2";
		section.endObject = cast<kmRC>Router.GetGameObject(Router.SerialiseGameObjectIDFromString("Ku_V"));
		section.endDir = false;
		section.sectionCount = 3;
		section.postSectionOpenCount = 0;
		section.junctions[0] = junctions[1];
		sections[3] = section;

		kmRouteAutoKN routeAuto = new kmRouteAutoKN();
		routeAuto.name = "1А";
		routeAuto.desctiption = "Автоматический приём со станции \"Посёлок Восточный\" на 1 главный станционный путь";
		routes[0] = routeAuto;

		routeAuto.route = new kmRouteAuto();
		routeAuto.route.name = "157 - Д (I - 1)";
		routeAuto.route.desctiption = "Приём со станции \"Посёлок Восточный\" на 1 главный станционный путь";
		routeAuto.route.startObject = cast<kmRC>Router.GetGameObject(Router.SerialiseGameObjectIDFromString("Signal 1950"));
		routeAuto.route.preRouteCount = 5;
		routeAuto.route.routeCount = -1;
		routeAuto.route.preRouteOpenCount = 2;
		routeAuto.route.type = 0;
		routeAuto.route.junctions[0] = junctions[0];
		routeAuto.route.jDirs[0] = true;
		routeAuto.route.sections[0] = sections[1];
		routeAuto.route.conflictSections[0] = sections[2];
		routes[1] = routeAuto.route;

		kmRoute route = new kmRoute();
		route.name = "В - Д (4 - 1)";
		route.desctiption = "Подача с 4 станционного пути на 1 главный станционный путь";
		route.startObject = cast<kmRC>Router.GetGameObject(Router.SerialiseGameObjectIDFromString("Ku_V"));
		route.preRouteCount = -2;
		route.routeCount = 3;
		route.preRouteOpenCount = 0;
		route.type = kmRC.O_SHUNT;
		route.junctions[0] = junctions[0];
		route.jDirs[0] = false;
		route.junctions[1] = junctions[1];
		route.jDirs[1] = false;
		route.sections[0] = sections[0];
		route.sections[1] = sections[1];
		route.conflictSections[0] = sections[2];
		route.conflictSections[1] = sections[3];
		routes[2] = route;

		route = new kmRoute();
		route.name = "Д - 2о (1 - II)";
		route.desctiption = "Отправление с 1 главного станционного пути на станцию \"Посёлок Восточный\"";
		route.startObject = cast<kmRC>Router.GetGameObject(Router.SerialiseGameObjectIDFromString("Ku_D"));
		route.preRouteCount = -2;
		route.routeCount = 4;
		route.preRouteOpenCount = 0;
		route.type = kmRC.O_MRT;
		route.junctions[0] = junctions[0];
		route.jDirs[0] = false;
		route.junctions[1] = junctions[1];
		route.jDirs[1] = false;
		route.sections[0] = sections[2];
		route.conflictSections[0] = sections[0];
		route.conflictSections[1] = sections[1];
		route.conflictSections[2] = sections[3];
		routes[3] = route;

		routeAuto = new kmRouteAutoKN();
		routeAuto.name = "2А";
		routeAuto.desctiption = "Автоматическое отправление со 2 главного станционного пути на станцию \"Посёлок Восточный\"";
		routes[4] = routeAuto;

		routeAuto.route = new kmRouteAuto();
		routeAuto.route.name = "152 - 2о (2 - II)";
		routeAuto.route.desctiption = "Отправление со 2 главного станционного пути на станцию \"Посёлок Восточный\"";
		routeAuto.route.startObject = cast<kmRC>Router.GetGameObject(Router.SerialiseGameObjectIDFromString("Signal 1932"));
		routeAuto.route.preRouteCount = 2;
		routeAuto.route.routeCount = -1;
		routeAuto.route.preRouteOpenCount = 0;
		routeAuto.route.type = kmRC.O_MU_PS;
		routeAuto.route.mu = "2";
		routeAuto.route.junctions[0] = junctions[1];
		routeAuto.route.jDirs[0] = true;
		routeAuto.route.sections[0] = sections[3];
		routeAuto.route.conflictSections[0] = sections[0];
		routeAuto.route.conflictSections[1] = sections[2];
		routes[5] = routeAuto.route;

		route = new kmRoute();
		route.name = "152 - В (2 - 4)";
		route.desctiption = "Подача со 2 главного станционного пути на 4 станционный путь (за светофор В)";
		route.startObject = (cast<kmRoute>routes[5]).startObject;
		route.preRouteCount = 2;
		route.routeCount = -1;
		route.preRouteOpenCount = 0;
		route.type = kmRC.O_SHUNT | kmRC.O_MU_PS;
		route.mu = "V";
		route.junctions[0] = junctions[1];
		route.jDirs[0] = true;
		route.sections[0] = sections[3];
		route.conflictSections[0] = sections[0];
		route.conflictSections[1] = sections[2];
		routes[6] = route;

		kmAutoTurn autoTurn = new kmAutoTurn();
		autoTurn.name = "АД";
		autoTurn.desctiption = "Автоматический оборот по 4 станционному пути (за светофором В)";
		autoTurn.inRoute = (cast<kmRoute>routes[6]);
		autoTurn.outRoute = (cast<kmRoute>routes[2]);
		autoTurn.outCheckCount = -2;
		routes[7] = autoTurn;

		routes[0].conflictRoutes[0] = routes[2];
		routes[0].conflictRoutes[1] = routes[3];
		routes[0].conflictRoutes[2] = routes[7];
		routes[1].conflictRoutes[0] = routes[0];
		routes[1].conflictRoutes[1] = routes[2];
		routes[1].conflictRoutes[2] = routes[3];
		routes[1].conflictRoutes[3] = routes[7];
		routes[2].conflictRoutes[0] = routes[0];
		routes[2].conflictRoutes[1] = routes[1];
		routes[2].conflictRoutes[2] = routes[3];
		routes[2].conflictRoutes[3] = routes[4];
		routes[2].conflictRoutes[4] = routes[5];
		routes[2].conflictRoutes[5] = routes[6];
		routes[2].conflictRoutes[6] = routes[7];
		routes[3].conflictRoutes[0] = routes[0];
		routes[3].conflictRoutes[1] = routes[1];
		routes[3].conflictRoutes[2] = routes[2];
		routes[3].conflictRoutes[3] = routes[4];
		routes[3].conflictRoutes[4] = routes[5];
		routes[3].conflictRoutes[5] = routes[6];
		routes[3].conflictRoutes[6] = routes[7];
		routes[4].conflictRoutes[0] = routes[2];
		routes[4].conflictRoutes[1] = routes[3];
		routes[4].conflictRoutes[2] = routes[6];
		routes[4].conflictRoutes[3] = routes[7];
		routes[5].conflictRoutes[0] = routes[2];
		routes[5].conflictRoutes[1] = routes[3];
		routes[5].conflictRoutes[2] = routes[4];
		routes[5].conflictRoutes[3] = routes[6];
		routes[5].conflictRoutes[4] = routes[7];
		routes[6].conflictRoutes[0] = routes[2];
		routes[6].conflictRoutes[1] = routes[3];
		routes[6].conflictRoutes[2] = routes[4];
		routes[6].conflictRoutes[3] = routes[5];
		routes[6].conflictRoutes[4] = routes[7];
		routes[7].conflictRoutes[0] = routes[0];
		routes[7].conflictRoutes[1] = routes[1];
		routes[7].conflictRoutes[2] = routes[3];
		routes[7].conflictRoutes[3] = routes[4];
		routes[7].conflictRoutes[4] = routes[5];

		kmWelcomeAuto welcomeAuto = new kmWelcomeAuto();
		welcomeAuto.name = "Ку157";
		welcomeAuto.sign = cast<kmSignal>Router.GetGameObject(Router.SerialiseGameObjectIDFromString("Signal 244"));
		kmWelcomePath wp = new kmWelcomePath();
		wp.conflictRoutes[0] = routes[7];
		welcomeAuto.paths[0] = wp;
		welcomeAuto.autoRoutes[0] = cast<kmRouteAutoKN>routes[0];
		welcomes[0] = welcomeAuto;

		welcomeAuto = new kmWelcomeAuto();
		welcomeAuto.name = "Ку155";
		welcomeAuto.sign = cast<kmSignal>Router.GetGameObject(Router.SerialiseGameObjectIDFromString("Signal 1952"));
		welcomeAuto.paths[0] = wp;
		welcomeAuto.autoRoutes[0] = cast<kmRouteAutoKN>routes[0];
		welcomes[1] = welcomeAuto;

		welcomeAuto = new kmWelcomeAuto();
		welcomeAuto.name = "Ку153";
		welcomeAuto.sign = cast<kmSignal>(cast<kmRoute>routes[1]).startObject;
		wp = new kmWelcomePath();
		wp.conflictRoutes[0] = routes[7];
		wp.junctions[0] = junctions[0];
		wp.jDirs[0] = true;
		welcomeAuto.paths[0] = wp;
		welcomeAuto.autoRoutes[0] = cast<kmRouteAutoKN>routes[0];
		welcomes[2] = welcomeAuto;

		kmWelcome welcome = new kmWelcome();
		welcome.name = "В";
		welcome.sign = cast<kmSignal>(cast<kmRoute>routes[2]).startObject;
		wp = new kmWelcomePath();
		wp.conflictRoutes[0] = routes[7];
		wp.junctions[0] = junctions[1];
		wp.jDirs[0] = false;
		wp.junctions[1] = junctions[0];
		wp.jDirs[1] = false;
		welcome.paths[0] = wp;
		welcomes[3] = welcome;

		welcomeAuto = new kmWelcomeAuto();
		welcomeAuto.name = "Ку152";
		welcomeAuto.sign = cast<kmSignal>(cast<kmRoute>routes[5]).startObject;
		wp = new kmWelcomePath();
		wp.conflictRoutes[0] = routes[7];
		wp.junctions[0] = junctions[1];
		wp.jDirs[0] = true;
		wp.mu = "2";
		welcomeAuto.paths[0] = wp;
		welcomeAuto.timeout = 45;
		welcomeAuto.autoRoutes[0] = cast<kmRouteAutoKN>routes[4];
		welcomes[4] = welcomeAuto;

		welcome = new kmWelcome();
		welcome.name = "Ку152/В";
		welcome.sign = cast<kmSignal>(cast<kmRoute>routes[5]).startObject;
		wp = new kmWelcomePath();
		wp.conflictRoutes[0] = routes[4];
		wp.conflictRoutes[1] = routes[7];
		wp.junctions[0] = junctions[1];
		wp.jDirs[0] = true;
//		wp.mu = "V";
		wp.mu = "4";
		welcome.paths[0] = wp;
		welcomes[5] = welcome;

		welcome = new kmWelcome();
		welcome.name = "Д";
		welcome.sign = cast<kmSignal>(cast<kmRoute>routes[3]).startObject;
		wp = new kmWelcomePath();
		wp.conflictRoutes[0] = routes[7];
		wp.junctions[0] = junctions[0];
		wp.jDirs[0] = false;
		wp.junctions[1] = junctions[1];
		wp.jDirs[1] = false;
		welcome.paths[0] = wp;
		welcomes[6] = welcome;
	}
};
