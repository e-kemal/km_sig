include "km_double_signal.gs"

class kmSignalInvisible isclass kmSignal{
//	kmDoubleSignal MainSig;
	GameObjectID MainSigId;

	void SetMeshes(int lens, bool state, float t) {
		if (MainSigId and cast<kmDoubleSignal>Router.GetGameObject(MainSigId)) {
			inherited(lens, state, t);
		}
	}

	void SetLenses(int value){
		lenses=value;
		if (!MainSigId) {
			return;
		}
		int i, tmp;
		kmDoubleSignal MainSig=cast<kmDoubleSignal>Router.GetGameObject(MainSigId);
		Asset MainSigAsset=null;
		if(MainSig)MainSigAsset=MainSig.GetAsset();
		for(i=0;i<10;i++){
			tmp=(value&7);
			value=value>>3;
			if(!MainSigAsset)LightMeshes[i]=null;
			else if(tmp>0 and tmp<7)//индусский код, чё уж...
				LightMeshes[i]=MainSig.SetFXAttachment("d"+i, MainSigAsset.FindAsset("lens"+tmp));
			else
				LightMeshes[i]=MainSig.SetFXAttachment("d"+i, null);
		}
		MyState=-1;
	}

	public void SetNameRC(string value){
		inherited(value);
		if (MainSigId) {
			kmDoubleSignal MainSig = cast<kmDoubleSignal>Router.GetGameObject(MainSigId);
			if (MainSig) {
				MainSig.SetBackNameRC(value);
			}
		}
	}

	public void SetName(string value){
		inherited(value);
		if (MainSigId) {
			kmDoubleSignal MainSig = cast<kmDoubleSignal>Router.GetGameObject(MainSigId);
			if (MainSig) {
				MainSig.SetBackName(value);
			}
		}
	}

	void UpdateMainSig(){
		GSTrackSearch ts=BeginTrackSearch(true);
		kmDoubleSignal sig=null;
		while(ts.SearchNextObject() and ts.GetDistance()<10.0 and !(cast<Signal>ts.GetObject()));
		if(ts.GetDistance()<10.0 and !ts.GetFacingRelativeToSearchDirection())sig=cast<kmDoubleSignal>ts.GetObject();
		if (sig) {
			MainSigId = sig.GetGameObjectID();
		}
	}

	public string GetDescriptionHTML(void){
		string s=inherited();
		s=s+"<table border='1' width=90%>";

		s=s+"<tr><td bgcolor='#666666'>MainSig:</td><td bgcolor='#AAAAAA'><a href='live://property/MainSig'>";
		kmDoubleSignal sig;
		if(MainSigId and (sig=cast<kmDoubleSignal>Router.GetGameObject(MainSigId)))s=s+sig.GetPropertyValue("name");
		else s=s+"<i>none</i>";
		s=s+"</a></td></tr>";

		s=s+"</table>";
		return s;
	}

	string GetPropertyType(string id){
		if(id=="MainSig")return "link";
		return inherited(id);
	}

	void LinkPropertyValue(string id){
		if(id=="MainSig")UpdateMainSig();
		else inherited(id);
	}

	public void SetProperties(Soup sp){
		inherited(sp);
		UpdateMainSig();
	}

	public void Init(Asset asset){
		MainSigId = null;
		UpdateMainSig();
		inherited(asset);
	}
};