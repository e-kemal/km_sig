@echo off
set gsc=D:\TS12\bin\trainzutil.exe
set include=D:\TS12\scripts\
set kuid="<kuid:216981:24109>"
set include2=..\24100_km_lib

if "%1"=="" goto help
if "%1"=="help" goto help
if "%1"=="encrypt" goto encrypt
if "%1"=="compile" goto compile
if "%1"=="install" goto install
if "%1"=="status" goto status
if "%1"=="commit" goto commit
if "%1"=="all" goto compile
goto end

:help
echo Use ./%0 [all^|encrypt^|compile^|install^|commit^|status^|help]
echo help - print this page and exit
echo install - install asset(s) from this folder
echo compile - compile scripts in this folder
goto end

:encrypt
for %%a in (*.gs) do %gsc% encrypt %%a
goto end

:compile
for %%a in (*.gs) do %gsc% compile %%a -i%include% -i%include2%

rem %gsc% compile %1 -i%include%

del *.gsl
if not "%1"=="all" goto end
set Choice=
set /p Choice=Install [y/n/r(recompile)]?
if /i '%Choice%'=='y' goto install
if /i '%Choice%'=='r' goto compile
goto end

:install
rem %gsc% installfrompath "%~dp0"
rem однако.... всё оказалось на много проще
%gsc% installfrompath .
if not "%1"=="all" goto end

:commit
%gsc% commit %kuid%
if not "%1"=="all" goto end

:status
%gsc% status %kuid%
goto end

:end
set gsc=
set include=
set kuid=
set include2=