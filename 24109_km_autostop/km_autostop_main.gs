include "km_autostop.gs"
include "km_signal.gs"

class kmAutoStopMain isclass kmAutoStop{
	Asset self;
	int state;
	kmSignal MySig=null;

	public bool CheckSpeed(float speed){
		return (state&1)!=0;
	}

	public void Update(Message msg){
		if(msg and msg.src!=MySig)return;
		if(!MySig)return;
		if(state&1){
			if((state&2) or MySig.GetMyState()>2){
				state=state&~1;
//				SetMeshAnimationState("default", true);
				SetMeshAnimationFrame("default", 29, 2.8);
			}
		}
		else{
			if((state&2)==0 and MySig.GetMyState()<3){
				state=state|1;
//				SetMeshAnimationState("default", false);
				SetMeshAnimationFrame("default", 2, 3.36);
			}
		}
	}

	void UpdateMySig(){
		if(MySig)
			Sniff(MySig, "kmAutoStopMode", "", false);
		GSTrackSearch ts=BeginTrackSearch(true);
		MySig=null;
		while(ts.SearchNextObject() and ts.GetDistance()<25.0 and !(ts.GetFacingRelativeToSearchDirection() and (MySig=cast<kmSignal>ts.GetObject())));
		Update(null);
		if(MySig)
			AddHandler(MySig, "kmAutoStopMode", "", "SetMode");
	}

	void SetMode(Message msg){
		if(msg.dst!=MySig and msg.dst!=me)return;
		if(msg.minor=="open")state=state|2;
		else state=state&~2;
		Update(null);
	}

	public string GetDescriptionHTML(void){
		string s=inherited();
		s=s+"<table border='1' width=90%>";

		if(MySig)
			Sniff(MySig, "kmAutoStopMode", "", false);
		GSTrackSearch ts=BeginTrackSearch(true);
		MySig=null;
		while(ts.SearchNextObject() and ts.GetDistance()<25.0 and !(ts.GetFacingRelativeToSearchDirection() and (MySig=cast<kmSignal>ts.GetObject())));

		s=s+"<tr><td bgcolor='#666666'>MySig:</td><td bgcolor='#AAAAAA'><a href='live://property/MySig'>";
		if(MySig)s=s+MySig.GetPropertyValue("name_clean")+" ("+ts.GetDistance()+"m)";
		else s=s+"<i>none</i>";
		s=s+"</a></td></tr>";

		if(MySig){
			AddHandler(MySig, "kmAutoStopMode", "", "SetMode");
			int n=Str.ToInt(MySig.GetPropertyValue("protective_count"));
			s=s+"<tr><td bgcolor='#666666'>protective:</td><td bgcolor='#AAAAAA'>";
			kmRC sig=null;
			if(n<=0)
				sig=MySig;//защитного участка нет, т.е. защитный участок - это мы и есть
			s=s+n+"&nbsp;(";
//		GSTrackSearch ts=BeginTrackSearch(true);
			while(!sig and ts.SearchNextObject()){
				if(!ts.GetFacingRelativeToSearchDirection())continue;
				object obj=ts.GetObject();
				if(cast<kmRC>obj){
					if((cast<kmRC>obj).GetMyType()&kmRC.ST_SKIP_RC)
						continue;
					if(!(--n))
						sig=cast<kmRC>obj;
					continue;
				}
				if(cast<Signal>obj)break;//от дефолта мы ничего не получим
			}
			if(sig){
				string str=sig.GetPropertyValue("nameRC_clean");
				if(str=="")
					str="<i>uncknown</i>";
				s=s+str+"</a>,&nbsp;"+ts.GetDistance()+"m";
			}
			else s=s+"<i>uncknown</i></a>";
			s=s+")</td></tr>";
		}

		s=s+"<tr><td bgcolor='#666666'>Mode:</td><td bgcolor='#AAAAAA'><a href='live://property/mode'>"+HTMLWindow.CheckBox("", state&2)+"Open</a></td></tr>";

		s=s+"</table>";
		return s;
	}

	string GetPropertyType(string id){
		if(id=="MySig")return "link";
		if(id=="mode")return "link";
		return inherited(id);
	}

	void LinkPropertyValue(string id){
		if(id=="MySig")UpdateMySig();
		else if(id=="mode"){
			state=state^2;
			Update(null);
		}
		else inherited(id);
	}

	public Soup GetProperties(){
		Soup sp=inherited();
		sp.SetNamedTag("state", state);
		return sp;
	}

	public void SetProperties(Soup sp){
		UpdateMySig();
		inherited(sp);
		state=sp.GetNamedTagAsInt("state", 0);
	}

	public void Init(Asset asset){
		inherited(self=asset);
		UpdateMySig();
		AddHandler(me, "Signal", "StateChanged", "Update");
		AddHandler(me, "kmAutoStopMode", "", "SetMode");
	}
};