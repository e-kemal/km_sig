
/**
	*	Интерфейс вагона, оборудованного автостопом и АЛС-АРС
	*
	*	@package		km_lib
	*	@version		0.1
	*	@date			19.10.2014
	*	@author			kemal, Kovalsky
	*	@site			http://RusTram.org
	*/

class kmVehicle{
	/**
	* Сохранение кодов
	*/
	int m_code, m_code2;

	/**
	* Передача кодов
	* @param bool dir [Направление. true - спереди, false - сзади]
	* @param int code ["Частота"]
	* @param int code2 [Предупредительная "частота"]
	*/
	public void kmSetCode(bool dir, int code, int code2){
		if(dir){
			m_code=code;
			m_code2=code2;
		}
	}

	/**
	* Получение сохранённого кода АЛС-АРС. Вызывается из кабины.
	* @return int [Код АЛС-АРС]
	*/
	public int kmGetCode(){
		return m_code;
	}

	/**
	* Получение сохранённого кода АЛС-АРС (предупредительная). Вызывается из кабины.
	* @return int [Код АЛС-АРС]
	*/
	public int kmGetCode2(){
		return m_code2;
	}

	/**
	* Проверка доступности АЛС-АРС у вагона
	* @return bool [True - если включена, false - выключена]
	*/
	public bool ALS_ARSEnabled() {
		return false;
	}
};
