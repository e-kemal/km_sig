include "km_route.gs"

class kmAutoTurn isclass kmRouteAbstract, kmObserver
{
    public kmRoute inRoute;
    public int inCheckCount;
    public kmRoute outRoute;
    public int outCheckCount;
    public int outCheckOffset;
    public kmAutoTurn neighbor;

    public define int R_NONE    = 0;
    public define int R_KN      = 1;
    public define int R_ACh     = 2;

    int state = R_NONE;

    kmRC endInCheckCache;
    kmRC endOutCheckCache;
    kmRC endOutOffsetCache;

    public string getHtml() {
        string ret = inherited();
        if (owner.debug) {
            ret = ret + "ACh: " + (state & R_ACh) + ", <a href=\"live://property/r" + index + "/touch\">touch</a><br>";
        }

        return ret;
    }

    public bool isKN() {
        return !!(state & R_KN);
    }

    void touch() {
        if (!(state & R_ACh)) {
            return;
        }
        if (inRoute.isKN() or !inRoute.sections[inRoute.sections.size() - 1].isZ()) {
            return;
        }
        if (outRoute.isKN() or !outRoute.sections[0].isZ()) {
            return;
        }
        if (neighbor and neighbor.inRoute.isKN()) {
            int n = 0;
            while (n < outRoute.conflictRoutes.size() and neighbor.inRoute != outRoute.conflictRoutes[n]) {
                ++n;
            }
            if (n < outRoute.conflictRoutes.size()) {
                return;
            }
        }
        if (!endOutCheckCache and !endOutOffsetCache) {
            owner.getExternalObserverContainerForObject(outRoute.startObject).addObserver(me);
        }
        int n;
        GSTrackSearch ts = outRoute.startObject.BeginTrackSearch(false);
        bool clear = true;
        if (outCheckOffset) {
            n = Math.Abs(outCheckOffset);
            while (ts.SearchNextObject()) {
                object obj = ts.GetObject();
                if (cast<Vehicle>obj) {
                    clear = false;
                    if (endOutOffsetCache) {
                        break;
                    }
                }
                kmRC rc = cast<kmRC>obj;
                if (rc and !(rc.GetMyType() & kmRC.ST_SKIP_RC) and (ts.GetFacingRelativeToSearchDirection() != (outCheckOffset > 0)) and !(--n)) {
                    if (!endOutOffsetCache) {
                        endOutOffsetCache = rc;
                        owner.getExternalObserverContainerForObject(rc).addObserver(me);
                    }
                    clear = clear and owner.getMainLib().checkOverlap(ts.CloneSearch());
                    break;
                }
            }
            if (!clear) {
                return;
            }
        }
        n = Math.Abs(outCheckCount);
        while (ts.SearchNextObject()) {
            object obj = ts.GetObject();
            if (cast<Vehicle>obj) {
                clear = false;
                if (endOutCheckCache) {
                    break;
                }
            }
            kmRC rc = cast<kmRC>obj;
            if (rc and !(rc.GetMyType() & kmRC.ST_SKIP_RC) and (ts.GetFacingRelativeToSearchDirection() != (outCheckCount > 0)) and !(--n)) {
                if (!endOutCheckCache) {
                    endOutCheckCache = rc;
                    owner.getExternalObserverContainerForObject(rc).addObserver(me);
                }
                clear = clear and owner.getMainLib().checkOverlap(ts);
                break;
            }
        }
        clear = clear and owner.getMainLib().checkOverlap(outRoute.startObject.BeginTrackSearch(true));
        if (!clear) {
            outRoute.make();
            outRoute.autoAction = me;
            return;
        }
        if (neighbor and neighbor.inRoute.isKN()) {
            return;
        }
        clear = true;
        if (inCheckCount) {
            if (!endInCheckCache) {
                owner.getExternalObserverContainerForObject(inRoute.startObject).addObserver(me);
            }
            n = Math.Abs(inCheckCount);
            ts = inRoute.startObject.BeginTrackSearch(false);
            while (ts.SearchNextObject()) {
                object obj = ts.GetObject();
                if (cast<Vehicle>obj) {
                    clear = false;
                    if (endInCheckCache) {
                        break;
                    }
                }
                kmRC rc = cast<kmRC>obj;
                if (rc and !(rc.GetMyType() & kmRC.ST_SKIP_RC) and (ts.GetFacingRelativeToSearchDirection() != (inCheckCount > 0)) and !(--n)) {
                    if (!endInCheckCache) {
                        endInCheckCache = rc;
                        owner.getExternalObserverContainerForObject(rc).addObserver(me);
                    }
                    clear = clear and owner.getMainLib().checkOverlap(ts);
                    break;
                }
            }
            clear = clear and owner.getMainLib().checkOverlap(inRoute.startObject.BeginTrackSearch(true));
        }
        else {
            clear = false;
        }
        if (!clear) {
            inRoute.make();
            inRoute.autoAction = me;
            if (neighbor and neighbor.isKN()) {
                state = state & ~R_ACh;
                neighbor.state = neighbor.state | R_ACh;
                neighbor.touch();
            }
        }
    }

    public void make() {
        state = state | R_KN;
        inRoute.sections[inRoute.sections.size() - 1].addObserver(me);
        outRoute.sections[0].addObserver(me);
        if (neighbor) {
            neighbor.inRoute.sections[0].addObserver(me);
        }
        if (inRoute.isKN()) {
            inRoute.autoAction = me;
            inRoute.touch();
        }
        if (outRoute.isKN()) {
            outRoute.autoAction = me;
            outRoute.touch();
        }
        if (!neighbor or !neighbor.isKN()) {
            state = state | R_ACh;
            touch();
        }
    }

    public void cancel() {
        state = state & ~(R_KN | R_ACh);
        if (me == inRoute.autoAction) {
            inRoute.autoAction = null;
        }
        if (me == outRoute.autoAction) {
            outRoute.autoAction = null;
        }
        inRoute.sections[inRoute.sections.size() - 1].removeObserver(me);
        outRoute.sections[0].removeObserver(me);
        if (neighbor) {
            neighbor.inRoute.sections[0].removeObserver(me);
        }
        if (endInCheckCache) {
            owner.getExternalObserverContainerForObject(endInCheckCache).removeObserver(me);
            owner.getExternalObserverContainerForObject(inRoute.startObject).removeObserver(me);
            endInCheckCache = null;
        }
        if (endOutCheckCache) {
            owner.getExternalObserverContainerForObject(endOutCheckCache).removeObserver(me);
            owner.getExternalObserverContainerForObject(outRoute.startObject).removeObserver(me);
            endOutCheckCache = null;
        }
        if (endOutOffsetCache) {
            owner.getExternalObserverContainerForObject(endOutOffsetCache).removeObserver(me);
            endOutOffsetCache = null;
        }
        if (neighbor and neighbor.isKN()) {
            neighbor.cancel();
        }
    }

    void onKN() {
        if (inRoute.isKN() or outRoute.isKN() or (neighbor and neighbor.isKN())) {
            make();
        }
    }

    public void handleEvent(kmObservable source, int type) {
        if (source == inRoute.startObject or source == endInCheckCache or source == outRoute.startObject or source == endOutCheckCache or source == endOutOffsetCache) {
            touch();
        }
        if (kmSection.EVENT_UNLOCKED_AFTER == type and (source == inRoute.sections[inRoute.sections.size() - 1] or source == outRoute.sections[0])) {
            touch();
        }
        if (neighbor and kmSection.EVENT_UNLOCKED_AFTER == type and source == neighbor.inRoute.sections[0]) {
            touch();
        }
    }

    public void init(kmDscpPost post, int i) {
        if (!inRoute) {
            Interface.Exception("post " + post.getStationName() + " (" + post.getStationCode() + "), autoTurn " + name + " inRoute is null");
        }
        if (!outRoute) {
            Interface.Exception("post " + post.getStationName() + " (" + post.getStationCode() + "), autoTurn " + name + " outRoute is null");
        }
        if (!outCheckCount) {
            Interface.Exception("post " + post.getStationName() + " (" + post.getStationCode() + "), autoTurn " + name + " outCheckCount is zero");
        }
        inherited(post, i);
    }

    public void saveProperties(Soup soup) {
        soup.SetNamedTag("routes.state." + index, state);
        if (endInCheckCache) {
            soup.SetNamedTag("routes.endInCheckCache." + index, endInCheckCache.GetGameObjectID());
        }
        if (endOutCheckCache) {
            soup.SetNamedTag("routes.endOutCheckCache." + index, endOutCheckCache.GetGameObjectID());
        }
        if (endOutOffsetCache) {
            soup.SetNamedTag("routes.endOutOffsetCache." + index, endOutOffsetCache.GetGameObjectID());
        }
    }

    public void loadProperties(Soup soup) {
        state = soup.GetNamedTagAsInt("routes.state." + index, state);
        if (state & R_KN) {
            inRoute.sections[inRoute.sections.size() - 1].addObserver(me);
            outRoute.sections[0].addObserver(me);
            if (neighbor) {
                neighbor.inRoute.sections[0].addObserver(me);
            }
        }
        GameObjectID id = soup.GetNamedTagAsGameObjectID("routes.endInCheckCache." + index);
        if (id) {
            endInCheckCache = cast<kmRC>Router.GetGameObject(id);
            owner.getExternalObserverContainerForObject(endInCheckCache).addObserver(me);
            owner.getExternalObserverContainerForObject(inRoute.startObject).addObserver(me);
        }
        else {
            endInCheckCache = null;
        }
        id = soup.GetNamedTagAsGameObjectID("routes.endOutCheckCache." + index);
        if (id) {
            endOutCheckCache = cast<kmRC>Router.GetGameObject(id);
            owner.getExternalObserverContainerForObject(endOutCheckCache).addObserver(me);
            owner.getExternalObserverContainerForObject(outRoute.startObject).addObserver(me);
        }
        else {
            endOutCheckCache = null;
        }
        id = soup.GetNamedTagAsGameObjectID("routes.endOutOffsetCache." + index);
        if (id) {
            endOutOffsetCache = cast<kmRC>Router.GetGameObject(id);
            owner.getExternalObserverContainerForObject(endOutOffsetCache).addObserver(me);
        }
        else {
            endOutOffsetCache = null;
        }
    }
};
