include "gs.gs"

class BinarySortedElement{
	public int key;
	public object value;
};


class BinarySortedArray{
	public BinarySortedElement[] DBSE=new BinarySortedElement[0];	// основной массив элементов
	public int N=0;			// число инициализированных элементов

	public void UdgradeArraySize(int NewN){			// мастер предварительного выделения места массиву
		int i;
		BinarySortedElement[] DBSE2= new BinarySortedElement[NewN];

		for(i=0;i<N;i++)			// пересохраняем старый массив
			DBSE2[i]=DBSE[i];

		for(i=N;i<NewN;i++)
			DBSE2[i]=new BinarySortedElement();

		DBSE=DBSE2;
	}

	int Find(int a, bool mode){ // при mode = true указывает место, где мог бы находиться новый элемент
		int i=0,f=0,b=N-1;
		if(N>0){
			if(DBSE[f].key == a)
				return f;
			if(DBSE[b].key == a)
				return b;

			if(a<DBSE[f].key){
				if(mode)
					return 0;
				else
					return -1;
			}
			if(DBSE[b].key<a){
				if(mode)
					return N;
				else
					return -1;
			}
			while(b>(f+1)){
				i=f + (int)((b-f)/2);				// середина отрезка

				if(DBSE[i].key==a)
					return i;

				if( DBSE[f].key<a and a<DBSE[i].key)	// на отрезке от f до i
					b=i;
				if( DBSE[i].key<a and a<DBSE[b].key)	// на отрезке от i до b
					f=i;
			}
			if(DBSE[f+1].key==a or (mode and DBSE[f].key<a and a<DBSE[f+1].key))
				return f+1;

			if(mode and DBSE[f+1].key<a and a<DBSE[f+2].key)
				return f+2;
		}

		if(mode)
			return i;
		return -1;					// не найден
	}
	public int Find(int a){ // при mode = true указывает место, где мог бы находиться новый элемент
		return Find(a, false);
	}

	public bool AddElement(int id, object NObject){
		if(DBSE.size()>0){
			int t = Find(id,true);

			if(t>=0 and t<=N){
				int i;
				for(i=N-1;i>=t;i--){
					DBSE[i+1].key=DBSE[i].key;
					DBSE[i+1].value=DBSE[i].value;
				}
				DBSE[t].key=id;
				DBSE[t].value=NObject;
				N++;

				if((N+20) > DBSE.size())
					UdgradeArraySize(2*DBSE.size());
				return true;
			}
		}
		return false;
	}

	public void DeleteElementByNmb(int a){
		if(a>=0){
			int i;
			for(i=a;i<N-1;i++){
				DBSE[i].key=DBSE[i+1].key;
				DBSE[i].value=DBSE[i+1].value;
			}
			N--;
		}
	}

	public void DeleteElement(int a){
		DeleteElementByNmb(Find(a,false));
	}
};