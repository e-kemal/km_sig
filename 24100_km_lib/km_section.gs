include "km_junction_info.gs"
include "km_route.gs"
include "km_lib.gs"

class kmRcAndDirPair
{
	public kmRC sig;
	public bool dir;
};

class kmSection isclass kmObserver, kmObservable, kmSignalSwitchInterface
{
	kmDscpPost owner;
	int index;
	public string name;

	public define int S_NONE		= 0;	//замыкающее реле под током
	public define int S_nZR			= 1;	//замыкающее реле без тока
	public define int S_NU			= 2;	//ЧУНУ
	public define int S_M1			= 4;	//зафиксирован наезд на дополнительный конечный объект
	public define int S_M2			= 8;	//зафиксирован заезд за конечный объект

	int state = S_NONE;

	public kmRoute route;

	public kmJunctionInfo[] junctions = new kmJunctionInfo[0];

	public kmRC startObject;
	public bool startDir;
	public kmRC endObject;
	public bool endDir;
	public int additionalEnd = 0;
	kmRC additionalEndCache;

	public int sectionCount;
	public int postSectionOpenCount;

	kmRC[] openedCache = new kmRC[0];

	public int preRouteCount;
	public int postRouteCount;
	public int postRouteProtective = 0;//количество РЦ за закрытым объектом. Включает КП

	public define int EVENT_UNLOCKED		= 0;
	public define int EVENT_UNLOCKED_AFTER	= 1;

	public string getHtml() {
		string ret = index + ". <font color=\"#";
		if ((state & S_nZR) or (route and route.isLockJunctions() and !owner.getMainLib().getBlinkerState())) {
			ret = ret + "000000";
		}
		else {
			ret = ret + "ffffff";
		}
		ret = ret + "\">*</font>&nbsp;<a href=\"live://property/s" + index + "/ir\">" + name + "</a><br>";
		if (owner.debug) {
			ret = ret + "NU: " + (state & S_NU) + ", M1: " + (state & S_M1) + ", M2: " + (state & S_M2);
			if (openedCache.size()) {
				ret = ret + "<br>openedCache: ";
				int i;
				for (i = 0; i < openedCache.size(); ++i) {
					if (i) {
						ret = ret + ", ";
					}
					ret = ret + openedCache[i].GetName();
				}
			}
			ret = ret + "<br>";
		}

		return ret;
	}

	public void resetObjects(bool andReverce) {
		GSTrackSearch ts = startObject.BeginTrackSearch(startDir);

		if ((startObject.GetMyType() & kmRC.ST_ROUTE_ONLY) or startDir) {
			startObject.Close(true);
		}
		else {
			startObject.Open(kmRC.O_TYPE_MAIN, true);
		}
		if (cast<kmSignal>startObject) {
			owner.PostMessage(startObject, "kmAutoStopMode", "auto", 2.0);
		}
		bool inRoute = true;
		int n = postRouteCount;
		kmRC end;
		bool endDir;
		while (ts.SearchNextObject()) {
			kmRC sig = cast<kmRC>ts.GetObject();
			if (!sig) {
				continue;
			}
			end = sig;
			endDir = ts.GetFacingRelativeToSearchDirection();
			if (!(inRoute or ts.GetFacingRelativeToSearchDirection())) {
				continue;
			}
			if ((sig.GetMyType() & kmRC.ST_ROUTE_ONLY) or (inRoute and ts.GetFacingRelativeToSearchDirection())) {
				sig.Close(true);
			}
			else {
				if (andReverce) {
					sig.Open(kmRC.O_TYPE_MAIN, true);
				}
			}
			if (cast<kmSignal>sig and (andReverce or ts.GetFacingRelativeToSearchDirection())) {
				owner.PostMessage(sig, "kmAutoStopMode", "auto", 2.0);
			}
			if (inRoute) {
				if (sig == endObject) {
					inRoute = false;
					if (n == 0) {
						break;
					}
				}
			}
			else {
				if (ts.GetFacingRelativeToSearchDirection()) {
					--n;
					if (n == 0) {
						break;
					}
				}
			}
		}
//		lib.Update(end, endDir, startObject);
//		lib.Update(startObject, !startDir, end);
	}

	public void openSignals(kmRcAndDirPair updateEnd) {
		state = state | S_NU;
		int i;
		for (i = 0; i < junctions.size(); ++i) {
			junctions[i].addObserver(me);
		}

		openedCache = new kmRC[0];
		if (endDir) {
			openedCache[0] = endObject;
		}
		if (sectionCount) {
			int n = Math.Abs(sectionCount);
			GSTrackSearch ts = endObject.BeginTrackSearch(!endDir);
			while (ts.SearchNextObject()) {
				kmRC sig = cast<kmRC>ts.GetObject();
				if (!sig) {
					continue;
				}
				if (!ts.GetFacingRelativeToSearchDirection()) {
					openedCache[openedCache.size()] = sig;
				}
				if (!(sig.GetMyType() & kmRC.ST_SKIP_RC) and (ts.GetFacingRelativeToSearchDirection() != (sectionCount > 0)) and !(--n)) {
					break;
				}
			}
		}
		if (postSectionOpenCount) {
			int n = Math.Abs(postSectionOpenCount);
			GSTrackSearch ts = endObject.BeginTrackSearch(endDir);
			while (ts.SearchNextObject()) {
				kmRC sig = cast<kmRC>ts.GetObject();
				if (!sig) {
					continue;
				}
				if (ts.GetFacingRelativeToSearchDirection()) {
					openedCache[0,0] = new kmRC[1];
					openedCache[0] = sig;

				}
				if (!(sig.GetMyType() & kmRC.ST_SKIP_RC) and (ts.GetFacingRelativeToSearchDirection() == (postSectionOpenCount > 0)) and !(--n)) {
					break;
				}
			}
		}
		for (i = 0; i < openedCache.size(); ++i) {
			int type = kmRC.O_CLEAR | kmRC.O_CODES | route.type;
			if (openedCache[i].hasClearOnSwitchers()) {
				type = type & ~(kmRC.O_MRT | kmRC.O_SHUNT);
				type = type | (openedCache[i].getOpenState() & (kmRC.O_MRT | kmRC.O_SHUNT));
			}
			if (openedCache.size() - 1 != i or sectionCount < 0) {
				openedCache[i].addClearOnSwithcer(me);
			}
			if (openedCache[i].getOpenState() & kmRC.O_SUR) {
				type = type | kmRC.O_SUR;
			}
			openedCache[i].setOpenState(type);

			if (i > Math.Abs(postSectionOpenCount) and cast<kmSignal>openedCache[i]) {
				(cast<kmSignal>openedCache[i]).setMUstate(route.mu);
			}
		}
		if (updateEnd and openedCache.size()) {
			updateEnd.sig = openedCache[0];
			updateEnd.dir = true;
		}
	}

	void closeSignals() {
		int i;
		for (i = 0; i < openedCache.size(); ++i) {
			openedCache[i].removeClearOnSwitcher(me);
			openedCache[i].resetClear();
		}
		if (openedCache.size()) {
			owner.getMainLib().Update(openedCache[0], true, openedCache[openedCache.size() - 1]);//todo: метод ядра, обновляющий по массиву объектов
		}
		openedCache = new kmRC[0];
		for (i = 0; i < junctions.size(); ++i) {
			junctions[i].removeObserver(me);
		}
	}

	public void lock(kmRoute R) {
		state = state | S_nZR | S_M1 | S_M2;
		route = R;
	}

	public void fullLock() {
		state = state & ~(S_M1 | S_M2);
		owner.getExternalObserverContainerForObject(endObject).addObserver(me);
		if (additionalEnd) {
			int n = Math.Abs(additionalEnd);
			GSTrackSearch ts = endObject.BeginTrackSearch(endDir);
			while (ts.SearchNextObject()) {
				kmRC sig = cast<kmRC>ts.GetObject();
				if (!sig or (sig.GetMyType() & kmRC.ST_SKIP_RC)) {
					continue;
				}
				if ((ts.GetFacingRelativeToSearchDirection() == (additionalEnd > 0)) and !(--n)) {
					additionalEndCache = sig;
					owner.getExternalObserverContainerForObject(sig).addObserver(me);
					break;
				}
			}
		}
	}

	void unlock() {
		state = S_NONE;
		if (route and !route.isLockJunctions()) {
			route = null;
		}
		closeSignals();
		//ToDo: закрытие объектов и встречных автостопов, восстановление автоматов
		owner.getExternalObserverContainerForObject(endObject).removeObserver(me);
		if (additionalEndCache) {
			owner.getExternalObserverContainerForObject(additionalEndCache).removeObserver(me);
			additionalEndCache = null;
		}
		notifyObservers(EVENT_UNLOCKED);
		notifyObservers(EVENT_UNLOCKED_AFTER);
	}

	bool checkForUnlock() {
		return (!additionalEnd or (state & S_M1)) and (state & S_M2);
	}

	void tryUnlock() {
		if (checkForUnlock()) {
			unlock();
		}
	}

	public void resetGS() {
//		route = null;
		tryUnlock();
		//ToDo: сбросить ЧУНУ только при свободной секции
	}

	public bool isZind() {
		return !(state & S_nZR);
	}

	public bool isZ() {
		return !(state & S_nZR) and !(route and route.isLockJunctions());
	}

	public bool isM() {
		return !!(state & (S_M1 | S_M2));
	}

	public void guiAction(string cmd) {
		if ("ir" == cmd) {
			if (owner.getMainLib().tryClaimPOK(owner, 2)) {
				if (!route or !route.isGS()) {
					unlock();
				}
			}
		}
	}

	public void handleEvent(kmObservable source, int type) {
		if (source == additionalEndCache and ((additionalEnd > 0 and kmRC.EVENT_ENTER_FORWARD == type) or (additionalEnd < 0 and kmRC.EVENT_ENTER_BACKWARD == type))) {
			state = state | S_M1;
			tryUnlock();
		}
		if (source == endObject and ((endDir and kmRC.EVENT_LEAVE_FORWARD == type) or (!endDir and kmRC.EVENT_LEAVE_BACKWARD == type))) {
			state = state | S_M2;
			tryUnlock();
		}
	}

	public void init(kmDscpPost post, int i) {
		if (!endObject) {
			Interface.Exception("post " + post.getStationName() + " (" + post.getStationCode() + "), section " + name + " endObject is null");
		}
		owner = post;
		index = i;
		state = S_NONE;
		route = null;
		int j;
		for (j = 0; j < junctions.size(); ++j) {
			junctions[j].sections[junctions[j].sections.size()] = me;
		}
	}

	public void saveProperties(Soup soup) {
		soup.SetNamedTag("sections.state." + index, state);
		if (additionalEndCache) {
			soup.SetNamedTag("sections.additionalEnd." + index, additionalEndCache.GetGameObjectID());
		}
		soup.SetNamedTag("sections.openedCacheCount." + index, openedCache.size());
		int i;
		for (i = 0; i < openedCache.size(); ++i) {
			soup.SetNamedTag("sections.openedCache." + index + "." + i, openedCache[i].GetGameObjectID());
		}
	}

	public void loadProperties(Soup soup) {
		state = soup.GetNamedTagAsInt("sections.state." + index, state);
		GameObjectID id = soup.GetNamedTagAsGameObjectID("sections.additionalEnd." + index);
		if (id) {
			additionalEndCache = cast<kmRC>Router.GetGameObject(id);
		}
		int i, n;
		n = soup.GetNamedTagAsInt("sections.openedCacheCount." + index, 0);
		openedCache = new kmRC[n];
		for (i = 0; i < n; ++i) {
			id = soup.GetNamedTagAsGameObjectID("sections.openedCache." + index + "." + i);
			openedCache[i] = cast<kmRC>Router.GetGameObject(id);
		}

		if (state & S_nZR) {
			owner.getExternalObserverContainerForObject(endObject).addObserver(me);
			if (additionalEndCache) {
				owner.getExternalObserverContainerForObject(additionalEndCache).addObserver(me);
			}
		}
		if (state & S_NU) {
			for (i = 0; i < junctions.size(); ++i) {
				junctions[i].addObserver(me);
			}
		}
	}
};

class kmSectionWithKP isclass kmSection
{
	public kmRoute[] resetableByRoutes = new kmRoute[0];
	public int kpState;
	public int timeout1 = 5;
	public int timeout2	= 20 - 5;

	public string getHtml() {
		string ret = inherited();
		if (owner.debug) {
			ret = ret + "kpState: " + kpState + "<br>";
		}

		return ret;
	}

	public void lock(kmRoute R) {
		inherited(R);
		kpState = 3;
	}

	public void fullLock() {
		inherited();
		kpState = 0;
	}

	void unlock() {
		inherited();
		kpState = 0;
		int i;
		for (i = 0; i < resetableByRoutes.size(); ++i) {
			resetableByRoutes[i].removeObserver(me);
		}
	}

	bool checkForUnlock() {
		bool ret = inherited();
		if (!ret) {
			return false;
		}
		if (kpState > 2) {
			return true;
		}
		int i;
		for (i = 0; i < resetableByRoutes.size(); ++i) {
			if (resetableByRoutes[i].isGS()) {
				return true;
			}
		}
		if (0 == kpState) {
			for (i = 0; i < resetableByRoutes.size(); ++i) {
				resetableByRoutes[i].addObserver(me);
			}
			kpState = 1;
			owner.ClearMessages("kmSectionKpTimer", index);
			owner.PostMessage(owner, "kmSectionKpTimer", index, timeout1);
		}

		return false;
	}

	public void handleEvent(kmObservable source, int type) {
		inherited(source, type);
		if (cast<kmRoute>source and kmRoute.EVENT_OPENED == type) {
			tryUnlock();
		}
	}

	public void onKpTimer() {
		if (1 == kpState) {
			state = state & ~S_NU;
			closeSignals();
			kpState = 2;
			owner.ClearMessages("kmSectionKpTimer", index);
			owner.PostMessage(owner, "kmSectionKpTimer", index, timeout2);
		}
		else if (2 == kpState) {
			kpState = 3;
			tryUnlock();
		}
	}

	public void saveProperties(Soup soup) {
		inherited(soup);
		soup.SetNamedTag("sections.kpState." + index, kpState);
	}

	public void loadProperties(Soup soup) {
		inherited(soup);
		kpState = soup.GetNamedTagAsInt("sections.kpState." + index, kpState);
		if (1 == kpState) {
			owner.PostMessage(owner, "kmSectionKpTimer", index, timeout1);
		}
		else if (2 == kpState) {
			owner.PostMessage(owner, "kmSectionKpTimer", index, timeout2);
		}
		if (kpState > 0) {
			int i;
			for (i = 0; i < resetableByRoutes.size(); ++i) {
				resetableByRoutes[i].addObserver(me);
			}
		}
	}
};
