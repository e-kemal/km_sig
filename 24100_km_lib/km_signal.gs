include "km_rc.gs"
include "km_autostop.gs"
include "km_welcome.gs"

class kmSignal isclass kmRC{
	int protective=0;//количество РЦ защитного участка
	int rcForBlue = 1;		//количество свободных РЦ для открытия синего
	string name="";
	int lenses;//линзы (10 линз по 3 бита)

//	public define int EVENT_STATE_CHANGED	= kmRC.EVENT_RC_LAST + 1;
	public define int EVENT_STATE_CHANGED	= 3;

	public define int SIGN_R	= 1;
	public define int SIGN_RR	= 2;
	public define int SIGN_YR	= 4;
	public define int SIGN_Y	= 8;
	public define int SIGN_YG	= 16;
	public define int SIGN_G	= 32;
	public define int SIGN_YY	= 64;
	public define int SIGN_YbY	= 128;
	public define int SIGN_W	= 256;
	public define int SIGN_B	= 512;
	public define int SIGN_Wb	= 1024;
	int EnableSign;

	int[] lensesA, lensesB;//битовые массивы линз на каждый сигнал
	int toBlink;	//битовый массив линз, которые мигают в данный момент
	MeshObject[] LightMeshes = new MeshObject[10];
	MeshObject[] rLightMeshes = new MeshObject[10];

	public define int STATE_R	= 0;
	public define int STATE_RR	= 1;
	public define int STATE_YR	= 2;
	public define int STATE_Y	= 3;
	public define int STATE_YG	= 4;
	public define int STATE_G	= 5;
	public define int STATE_YY	= 6;
	public define int STATE_YbY	= 7;
	public define int STATE_W	= 8;
	public define int STATE_B	= 9;
	public define int STATE_Wb	= 10;
	public define int STATE_no	= 11;
	int MyState;//кэшируем свой розжиг

	MeshObject MU;
	string MUstate="";
	kmWelcome welcome;

	public string GetPropertyValue(string id);

	public int GetEnableSignals(){
		return EnableSign;
	}

	public int GetMyState(){
		return MyState;
	}

	public void setMUstate(string state) {
		MUstate = state;
	}

	void SetMeshes(int lens, bool state, float t){
//		Interface.Print(name+".SetMeshes("+lens+", "+state+", "+t+")");
		lens=lens&1023;
		int i=0;
		while(lens){
			if(LightMeshes[i] and (bool)(lens&1)){
				LightMeshes[i].SetMeshVisible("default", state, t);
//				Interface.Print("SetMeshVisible(\"c"+i+"\", "+state+", "+t+");");
//				if(kozirki[i])
//					SetMeshVisible("k"+i, state, t);
			}
			if(rLightMeshes[i] and (bool)(lens&1)){
				rLightMeshes[i].SetMeshVisible("default", state, t);
			}
			lens=lens >> 1;
			i++;
		}
	}

	thread void NewSignal(){//(int lens, int blens){
		int a, b;
		if(MyState<lensesA.size()){
			a=lensesA[MyState];
			b=lensesB[MyState];
		}
		else
			a=b=0;

		toBlink = toBlink & b & a;
		if (welcome or (openState & O_PRIGL))
			toBlink = toBlink | lensesB[10];	//не трож пригласительный!
		SetMeshes(~(a | lensesA[10] | toBlink), false, 0.5);

		Sleep(0.5);
		SetMeshes(~(a | lensesA[10] | toBlink), false, 0.0);//заглушка от быстрой смены сигналов

		toBlink = b;
		if (welcome or (openState & O_PRIGL))
			toBlink = toBlink | lensesB[10];
		if (toBlink)
			AddHandler(kmLib, "kmSigBlinker", null, "onBlinker");
		else
			AddHandler(kmLib, "kmSigBlinker", null, null);

		a = a & ~lensesA[10];
		if (toBlink) {
			if (kmLib.getBlinkerState())
				a = a | b;
			else
				a = a & ~toBlink;
		}
		SetMeshes(a, true, 0.2);
	}


	void WelcomeBlinker(){
		int a = lensesA[10];
		if (welcome or (openState & O_PRIGL)) {
			toBlink = toBlink | lensesB[10];
			if (kmLib.getBlinkerState())
				a = a | lensesB[10];
			else
				a = a & ~toBlink;
		}
		else {
			toBlink = toBlink & ~lensesB[10];
		}
		if (toBlink)
			AddHandler(kmLib, "kmSigBlinker", null, "onBlinker");
		else
			AddHandler(kmLib, "kmSigBlinker", null, null);
		SetMeshes(a, welcome or (openState & O_PRIGL), 0.2);
	}

	void onBlinker(Message msg) {
		SetMeshes(toBlink, msg.minor != "false", 0.2);
	}

	public int GetARS() {
		if (STATE_R == MyState) {
			return ARS_AO;
		}

		return inherited();
	}

	void ApplySignalState(){
		if (!(EnableSign & SIGN_R) and !(openState & O_MK)) {
			inherited();
			return;
		}
		int sigstate=EX_STOP;
		if (MyState > STATE_YR) {
			if(MyType&S_TYPE_ARS){//при запрещающих будет запрещающий, но при всех остальных действует логика РЦ
				inherited();
				return;
			}
			else{
				switch(MyState){
					case STATE_Y://Ж
					case STATE_YY://ЖЖ
					case STATE_W://Б
						sigstate=EX_CAUTION;
						break;
					case STATE_YbY://ЖмЖ
						sigstate=EX_ADVANCE_CAUTION;
						break;
					case STATE_YG://ЖЗ
					case STATE_G://З
						sigstate=EX_PROCEED;
						break;
		//			case STATE_B://С
					default:
						//В любой непонятной ситуации...
						sigstate = EX_STOP;
						break;
				}
			}
		}
		SetSignalStateEx(null, sigstate, "ARS=" + GetARS());
	}

	public StateInfo GetState(){
		StateInfo ret = inherited();
		if (STATE_R == MyState) {
			if (MyType & ST_SKIP_RC) {
				ret.RCcount = 0;
			}
			else {
				ret.RCcount = 1;
			}
		}
		int cnt = RCcount;
		if (!(openState & O_CLEAR) and cnt > limitRCcount) {
			cnt = limitRCcount;
		}
		if (((EnableSign & SIGN_RR) and (openState & O_MK)) or !(openState & O_CLEAR) ) {
			if (EnableSign & SIGN_R and cnt >= protective)
				ret.BUcount = 1;
			else
				ret.BUcount = 0;
			return ret;
		}
		if (!(EnableSign & SIGN_R)) {
			ret.BUcount = BUcount;
			return ret;
		}
		switch (MyState) {
			case STATE_G:
			case STATE_YG:
				ret.BUcount = 3;
				break;
			case STATE_Y:
			case STATE_YY:
			case STATE_YbY:
				ret.BUcount = 2;
				break;
			case STATE_W:
				ret.BUcount = 1;
				break;
			default:
				if (cnt >= protective) {
					ret.BUcount = 1;
				}
				else {
					ret.BUcount = 0;
				}
				break;
		}

		return ret;
	}

	void updateMU() {
		if (!MU)
			return;
		string newMU = "mu_" + MUstate;
		bool useMU = false;
		if (MUstate != "") {
			if (MyState > STATE_YR) {
				if (!(MyType & S_TYPE_ARS) or openState & O_RAB)
					useMU = openState & O_MU_AB;
				else
					useMU = openState & O_MU_ARS and (STATE_no != MyState or openState & O_CLEAR);
			}
			if (openState & O_MU_PS and openState & O_PRIGL)
				useMU = true;
		}
		if (welcome and !useMU) {
			string wMU = welcome.getCurrentMU();
			if (wMU) {
				useMU = true;
				newMU = "mu_" + wMU;
			}
		}
		if (!useMU or !MU.HasMesh(newMU))
			newMU = "default";
		MU.SetMesh(newMU, 0.0);
	}

	void UpdateMyState(){
		int oldMyState=MyState;
		
		if ((EnableSign & SIGN_RR) and (openState & O_MK)) {
			MyState = STATE_RR;
		}
		else if (!(EnableSign & SIGN_R)) {//не умеем показывать К - skipBU
			if (EnableSign & SIGN_Y)
				MyState = STATE_Y;//пытаемся показать Ж (для заградительных с нормальногорящим Ж)
			else
				MyState = STATE_no;//гасим светофор
		}
		else if ((MyType & S_TYPE_ARS) and !(openState & O_RAB)) {//АБ погашена
			if (EnableSign & SIGN_B) {
				if ((openState & O_SUR) and (openState & O_CLEAR) and (RCcount >= rcForBlue or MyType & ST_ARS_NO_R)) {
					MyState = STATE_B;
				}
				else if ((openState & O_CLEAR or limitRCcount > 0) and EnableSign & SIGN_YR and RCcount > 0) {
					MyState = STATE_YR;
				}
				else {
					MyState = STATE_R;
				}
			}
			else
				MyState = STATE_no;//гасим светофор
		}
		else {
			int t = openState & (O_SUR | O_MRT | O_SHUNT);
			if (BUcount < 1) {
				t = 0;
			}
			if (t & O_SUR) {
				if (openState & O_SHUNT) {
					if (EnableSign & SIGN_W)
						t = O_SUR | O_SHUNT;
					else if (EnableSign & (SIGN_Y | SIGN_YG | SIGN_G))
						t = O_SUR;
					else if (EnableSign & (SIGN_YY | SIGN_YbY))
						t = O_SUR | O_MRT;
					else
						t = 0;
				}
				else if (openState & O_MRT) {
					if (EnableSign & (SIGN_YY | SIGN_YbY))
						t = O_SUR | O_MRT;
					else if (EnableSign & (SIGN_Y | SIGN_YG | SIGN_G))
						t = O_SUR;
					else if (EnableSign & SIGN_W)
						t = O_SUR | O_SHUNT;
					else
						t = 0;
				}
				else {
					if (EnableSign & (SIGN_Y | SIGN_YG | SIGN_G))
						t = O_SUR;
					else if (EnableSign & (SIGN_YY | SIGN_YbY))
						t = O_SUR | O_MRT;
					else if (EnableSign & SIGN_W)
						t = O_SUR | O_SHUNT;
					else
						t = 0;
				}
			}
			if (!(t & O_SUR)) {
				if ((openState & O_CLEAR or limitRCcount > 0) and RCcount > 0 and EnableSign & SIGN_YR and !(MyType & S_TYPE_ARS and openState & O_RAB))
					MyState = STATE_YR;
				else
					MyState = STATE_R;
			}
			else if (t & O_SHUNT) {
				MyState = STATE_W;
			}
			else if (t & O_MRT) {
				if (BUcount == 1) {//если свободен 1 БУ
					if (EnableSign & SIGN_YY)
						MyState = STATE_YY;//пытаемся показать ЖЖ
					else
						MyState = STATE_YbY;//пытаемся показать ЖмЖ
				}
				else{
					if (EnableSign & SIGN_YbY)
						MyState = STATE_YbY;//пытаемся показать ЖмЖ
					else
						MyState = STATE_YY;//пытаемся показать ЖЖ
				}
			}
			else {
				switch (BUcount) {
					case 1://если свободен 1 БУ
						if (EnableSign & SIGN_Y)
							MyState = STATE_Y;//пытаемся показать Ж
						else if (EnableSign & SIGN_YG)
							MyState = STATE_YG;//пытаемся показать ЖЗ
						else
							MyState = STATE_G;//пытаемся показать З
						break;
					case 2://если следующий жёлтый
						if(EnableSign & SIGN_YG)
							MyState = STATE_YG;//пытаемся показать ЖЗ
						else if (EnableSign & SIGN_G)
							MyState = STATE_G;//пытаемся показать З
						else
							MyState = STATE_Y;//пытаемся показать Ж
						break;
					default://если следующий ЖЗ или зелёный
						if (EnableSign & SIGN_G)
							MyState = STATE_G;//пытаемся показать З
						else if (EnableSign & SIGN_YG)
							MyState = STATE_YG;//пытаемся показать ЖЗ
						else
							MyState = STATE_Y;//пытаемся показать Ж
						break;
				}
			}
		}
		updateMU();
		if(oldMyState!=MyState) {
			NewSignal();
			notifyObservers(EVENT_STATE_CHANGED);
		}
	}

	bool UpdateStateBU(StateInfo state){
		int oldBUcount=BUcount;
		BUcount=state.BUcount;

		UpdateMyState();

		if (STATE_R == MyState) {
			state.RCcount = 0;//закрытый светофор закрывает и РЦ
		}

		int cnt = RCcount;
		if (!(openState & O_CLEAR) and cnt > limitRCcount) {
			cnt = limitRCcount;
		}

		if (MyState == STATE_RR or !(openState & O_CLEAR)) {
			if (EnableSign & SIGN_R and (cnt >= protective))
				state.BUcount=1;
			else
				state.BUcount=0;
			return true;
		}

		if (EnableSign & SIGN_R) {
			switch (MyState){
				case STATE_G://З
				case STATE_YG://ЖЗ
					state.BUcount = 3;
					break;
				case STATE_Y://Ж
				case STATE_YY://ЖЖ
				case STATE_YbY://ЖмЖ
					state.BUcount = 2;
					break;
				case STATE_W://Б
//				case STATE_B://С
					state.BUcount = 1;
					break;
				default:
					if (cnt >= protective)
						state.BUcount = 1;
					else
						state.BUcount = 0;
					break;
			}
		}

		return BUcount!=oldBUcount;
	}

//deprecated
	public void Open(int type, bool auto){//Вызывается маршрутизацией при задании маршрута
//		Open(type, auto, "");
		inherited(type, auto);
	}

//deprecated
	public void Open(int type, bool auto, string mu, int useMU){//Вызывается маршрутизацией при задании маршрута
		if(type&O_TYPE){
			MUstate=mu;
			openState = openState &~ (O_MU_AB | O_MU_ARS | O_MU_PS);
			if (useMU & OT_MU_ON_AB)
				openState = openState | O_MU_AB;
			if (useMU & OT_MU_ON_ARS)
				openState = openState | O_MU_ARS;
			if (useMU & OT_MU_ON_PS)
				openState = openState | O_MU_PS;
		}
		Open(type, auto);
	}

	public void WelcomeOpen(kmWelcome w){
		if (!(EnableSign & SIGN_Wb))
			return;
		if (w) {
			welcome = w;
		}
		else {		//Если открывает пост, то все проверки уже выполнены там
			kmSignal sig2=null;
			GSTrackSearch ts=BeginTrackSearch(true);
			while(ts.SearchNextObject()){
				if(!ts.GetFacingRelativeToSearchDirection()){
					Trackside t;
					if((t=cast<Trackside>ts.GetObject()) and t.GetIsSearchLimit()){
						sig2=null;
						break;
					}
					continue;
				}
				if(sig2=cast<kmSignal>ts.GetObject()){
					if(sig2.GetEnableSignals()&1)
						break;
					sig2=null;
					continue;
				}
				if(cast<kmRC>ts.GetObject())
					continue;
				if(cast<Signal>ts.GetObject()){
					sig2=null;//поиск окончен
					break;
				}
			}
			if(!sig2 or sig2.GetMyType()&ST_NO_PS){
				Interface.Print("sig "+name+": welcome signal is forbidden");
				return;
			}
			openState = openState | O_PRIGL;
		}
		updateMU();
		WelcomeBlinker();
		if (mn) {
			updateBrowser(mn);
		}
	}
	public void WelcomeOpen() {
		WelcomeOpen(null);
	}

	public void WelcomeCancel(){
		openState = openState & ~O_PRIGL;
		welcome = null;
		WelcomeBlinker();
		updateMU();
		if (mn) {
			updateBrowser(mn);
		}
	}

	public void Close(bool force){//вызывается библиотекой при наезде на РЦ
		if(force)MUstate/*=wMUstate*/="";//при разборе маршрута гасим МУ
		inherited(force);
		UpdateMyState();
		ApplySignalState();//видимо, и тут проще вызвать дважды.
	}

	public float GetOverlap(){//TODO: для совместимости с дефолтом
		//вернём длину защитного участка
		return 100.0;
	}

	public void SetName(string value){
		name=value;
		MeshObject marker;
		string[] tmp=GetChars(value);
		value="";
		int i;
		for(i=0;i<tmp.size();i++)
			value=value+tmp[i];
		if(value=="")value=" ";
		if(marker=GetFXAttachment("marker"))//А всё почему? Потому что тег surveyor-only доступен только в эффектах, но не в субмешах
			marker.SetFXNameText("name", value);
	}

	void SetLenses(int value){
		lenses=value;
		int i, tmp;
		for(i=0;i<10;i++){
			tmp=(value&7);
			value=value>>3;
			if(tmp>0 and tmp<7) {//индусский код, чё уж...
				LightMeshes[i] = SetFXAttachment("c" + i, self.FindAsset("lens" + tmp));
				rLightMeshes[i] = SetFXAttachment("r_c" + i, self.FindAsset("lens" + tmp));
			}
			else {
				LightMeshes[i] = SetFXAttachment("c" + i, null);
				rLightMeshes[i] = SetFXAttachment("r_c" + i, null);
			}
		}
		MyState=20;//-1//лучше погасим светофор
	}

	string Lenses2str(int x){
		string ret="";
		int i, c, a=lensesA[x], b=lensesB[x], tmp=lenses;
		for(i=0;i<10;i++){
			if(i)ret=ret+"&nbsp;";
			c=tmp&7;
			if(c){
				ret=ret+"<font color=\"#"+ST.GetString("color"+c)+"\"><a href=\"live://property/lens_"+x+"_"+i+"\">";
				if((a&1)or(b&1)){
					ret=ret+ST.GetString("colorcode"+c);
					if(b&1)ret=ret+"b";
				}
				else
					ret=ret+"*";
				ret=ret+"</a></font>";
			}
			tmp=tmp>>3;
			a=a>>1;
			b=b>>1;
		}
		return ret;
	}

	public string GetDescriptionHTML(void){
		string s=inherited();
		s=s+"<table border='1' width=90%>";

		if (MU) {
			s = s + "<tr><td bgcolor='#666666'>MUstate:</td><td bgcolor='#AAAAAA'>";
			s = s + "<trainz-object width='30' height='17' style='text-line' align='left' max-chars='10' id='MUstate' text='" + MUstate + "'></trainz-object>";
			s = s + "&nbsp;<a href='live://property/MU_ON_AB'>" + HTMLWindow.CheckBox("", openState & O_MU_AB) + "on AB</a>";
			s = s + "&nbsp;<a href='live://property/MU_ON_ARS'>" + HTMLWindow.CheckBox("", openState & O_MU_ARS) + "on ARS</a>";
			s = s + "&nbsp;<a href='live://property/MU_ON_PS'>" + HTMLWindow.CheckBox("", openState & O_MU_PS) + "on PS</a>";
			s = s + "</td></tr>";
		}

		s=s+"<tr><td bgcolor='#666666'>name:</td><td bgcolor='#AAAAAA'><a href='live://property/name'>";
		if(name!="")s=s+name;
		else s=s+"<i>none</i>";
		s=s+"</a></td></tr>";

		float ASoffset=0.0;
		GSTrackSearch ts=BeginTrackSearch(false);
		while(ts.SearchNextObject()){
			if(ts.GetFacingRelativeToSearchDirection())continue;
			object obj=ts.GetObject();
			if(cast<kmAutoStop>obj){
				ASoffset=ts.GetDistance();
				continue;
			}
			if(cast<kmSignal>obj)
				break;
			if(cast<kmRC>obj)
				continue;
			if(cast<Signal>obj)break;//от дефолта мы ничего не получим
		}
		s=s+"<tr><td bgcolor='#666666'>protective:</td><td bgcolor='#AAAAAA'>";
		kmRC sig=null;
		if(protective>0)
			s=s+"<a href='live://property/protective_down'>-</a>";
		else{
			s=s+"-";
			sig=me;//защитного участка нет, т.е. защитный участок - это мы и есть
		}
		s=s+"&nbsp;<a href='live://property/protective_count'>"+protective+"</a>&nbsp;<a href='live://property/protective_up'>+</a>&nbsp;(<a href='live://property/protective_name'>";
		int n=protective;
		ts=BeginTrackSearch(true);
		while(!sig and ts.SearchNextObject()){
			if(!ts.GetFacingRelativeToSearchDirection())continue;
			object obj=ts.GetObject();
			if(cast<kmRC>obj){
				if((cast<kmRC>obj).GetMyType()&ST_SKIP_RC)
					continue;
				if(!(--n))
					sig=cast<kmRC>obj;
				continue;
			}
			if(cast<Signal>obj)break;//от дефолта мы ничего не получим
		}
		if(sig){
			string str=sig.GetPropertyValue("nameRC_clean");
			if(str=="")
				str="<i>uncknown</i>";
			s=s+str+"</a>,&nbsp;"+(ASoffset+ts.GetDistance())+"m";
		}
		else s=s+"<i>uncknown</i></a>";
		s=s+")</td></tr>";

		s=s+"<tr><td bgcolor='#666666'>Next Signal:</td><td bgcolor='#AAAAAA'>";
		kmSignal sig2=null;
		ts=BeginTrackSearch(true);
		while(ts.SearchNextObject()){
			if(!ts.GetFacingRelativeToSearchDirection())continue;
			if(sig2=cast<kmSignal>ts.GetObject()){
				if(sig2.GetEnableSignals()&1)
					break;
				sig2=null;
				continue;
			}
			if(cast<kmRC>ts.GetObject())
				continue;
			if(cast<Signal>ts.GetObject()){
				sig2=null;//поиск окончен
				break;
			}
		}
		if(sig2){
			string str=sig2.GetPropertyValue("name_clean");
			if(str=="")
				str="<i>uncknown</i>";
			s=s+str+"&nbsp;("+ts.GetDistance()+"m)";
		}
		else s=s+"<i>none</i>";
		s=s+"</td></tr>";

		if(MyType&S_TYPE_ARS)
			s=s+"<tr><td bgcolor='#666666'>ARS_NO_R:</td><td bgcolor='#AAAAAA'><a href='live://property/ARS_NO_R'>"+HTMLWindow.CheckBox("", MyType&ST_ARS_NO_R)+"enable</a></td></tr>";

		s=s+"<tr><td bgcolor='#666666'>NO_PS:</td><td bgcolor='#AAAAAA'><a href='live://property/NO_PS'>"+HTMLWindow.CheckBox("", MyType&ST_NO_PS)+"enable</a></td></tr>";

		s=s+"<tr><td bgcolor='#666666'>lenses:</td><td bgcolor='#AAAAAA'><a href='live://property/lenses'>"+GetPropertyValue("lenses")+"</a> (<a href='live://property/usekit'>use kit</a>, <a href='live://property/logkit'>log</a>)</td></tr>";

		s = s + "<tr><td bgcolor='#666666'><a href='live://property/EnableSign1'>" + HTMLWindow.CheckBox("", EnableSign & SIGN_R) + "R</a></td><td bgcolor='#000000'>" + Lenses2str(STATE_R) + "</td></tr>";//Может быть отключен, например для заградительного
		s = s + "<tr><td bgcolor='#666666'><a href='live://property/EnableSign2'>" + HTMLWindow.CheckBox("", EnableSign & SIGN_RR) + "RR</a></td><td bgcolor='#000000'>" + Lenses2str(STATE_RR) + "</td></tr>";//Включается только для заградительного
		s = s + "<tr><td bgcolor='#666666'><a href='live://property/EnableSign4'>" + HTMLWindow.CheckBox("", EnableSign & SIGN_YR) + "YR</a></td><td bgcolor='#000000'>" + Lenses2str(STATE_YR) + "</td></tr>";
		s = s + "<tr><td bgcolor='#666666'><a href='live://property/EnableSign1024'>" + HTMLWindow.CheckBox("", EnableSign & SIGN_Wb) + "Wb</a></td><td bgcolor='#000000'>" + Lenses2str(STATE_Wb) + "</td></tr>";//пригласительный

		s = s + "<tr><td bgcolor='#666666'>" + HTMLWindow.CheckBox("", EnableSign & (SIGN_Y | SIGN_YG | SIGN_G)) + "main:</td><td bgcolor='#AAAAAA'></td></tr>";
		s = s + "<tr><td bgcolor='#666666'><a href='live://property/EnableSign8'>" + HTMLWindow.CheckBox("", EnableSign & SIGN_Y) + "Y</a></td><td bgcolor='#000000'>" + Lenses2str(STATE_Y) + "</td></tr>";
		s = s + "<tr><td bgcolor='#666666'><a href='live://property/EnableSign16'>" + HTMLWindow.CheckBox("", EnableSign & SIGN_YG) + "YG</a></td><td bgcolor='#000000'>" + Lenses2str(STATE_YG) + "</td></tr>";
		s = s + "<tr><td bgcolor='#666666'><a href='live://property/EnableSign32'>" + HTMLWindow.CheckBox("", EnableSign & SIGN_G) + "G</a></td><td bgcolor='#000000'>" + Lenses2str(STATE_G) + "</td></tr>";

		s = s + "<tr><td bgcolor='#666666'>" + HTMLWindow.CheckBox("", EnableSign & (SIGN_YY | SIGN_YbY)) + "mrt:</td><td bgcolor='#AAAAAA'></td></tr>";
		s = s + "<tr><td bgcolor='#666666'><a href='live://property/EnableSign64'>" + HTMLWindow.CheckBox("", EnableSign & SIGN_YY) + "YY</a></td><td bgcolor='#000000'>" + Lenses2str(STATE_YY) + "</td></tr>";
		s = s + "<tr><td bgcolor='#666666'><a href='live://property/EnableSign128'>" + HTMLWindow.CheckBox("", EnableSign & SIGN_YbY) + "YbY</a></td><td bgcolor='#000000'>" + Lenses2str(STATE_YbY) + "</td></tr>";

		s = s + "<tr><td bgcolor='#666666'>" + HTMLWindow.CheckBox("", EnableSign & SIGN_W) + "shunt:</td><td bgcolor='#AAAAAA'></td></tr>";
		s = s + "<tr><td bgcolor='#666666'><a href='live://property/EnableSign256'>" + HTMLWindow.CheckBox("", EnableSign & SIGN_W) + "W</a></td><td bgcolor='#000000'>" + Lenses2str(STATE_W) + "</td></tr>";

		s = s + "<tr><td bgcolor='#666666'>" + HTMLWindow.CheckBox("", MyType & S_TYPE_ARS) + "ARS:</td><td bgcolor='#AAAAAA'></td></tr>";
		s = s + "<tr><td bgcolor='#666666'>";
		if(MyType&S_TYPE_ARS)
			s = s + "<a href='live://property/EnableSign512'>" + HTMLWindow.CheckBox("", EnableSign & SIGN_B) + "enable</a>";
		else
			s=s+HTMLWindow.CheckBox("", false)+"enable";
		s = s + "</td><td bgcolor='#000000'>" + Lenses2str(STATE_B) + "</td></tr>";

		if (EnableSign & SIGN_B) {
			s = s + "<tr><td bgcolor='#666666'>rcForBlue:</td><td bgcolor='#AAAAAA'>";
			sig = null;
			if (rcForBlue > 0) {
				s = s + "<a href='live://property/rcForBlue_down'>-</a>";
			}
			else {
				s = s + "-";
				sig = me;
			}
			s = s + "&nbsp;<a href='live://property/rcForBlue_count'>" + rcForBlue + "</a>&nbsp;<a href='live://property/rcForBlue_up'>+</a>&nbsp;(<a href='live://property/rcForBlue_name'>";
			n = rcForBlue;
			ts = BeginTrackSearch(true);
			while(!sig and ts.SearchNextObject()) {
				if (!ts.GetFacingRelativeToSearchDirection()) {
					continue;
				}
				object obj = ts.GetObject();
				if (cast<kmRC>obj) {
					if ((cast<kmRC>obj).GetMyType() & ST_SKIP_RC) {
						continue;
					}
					if (!(--n)) {
						sig = cast<kmRC>obj;
					}
					continue;
				}
				if (cast<Signal>obj) {
					break;//от дефолта мы ничего не получим
				}
			}
			if (sig) {
				string str = sig.GetPropertyValue("nameRC_clean");
				if ("" == str) {
					str = "<i>uncknown</i>";
				}
				s = s + str + "</a>,&nbsp;" + ts.GetDistance() + "m";
			}
			else {
				s = s + "<i>uncknown</i></a>";
			}
			s = s + ")</td></tr>";
		}

		if (EnableSign & SIGN_Wb)
			s = s + "<tr><td bgcolor='#666666'>Welcome:</td><td bgcolor='#AAAAAA'><a href='live://property/welcome'>" + HTMLWindow.CheckBox("", openState & O_PRIGL) + "open</a></td></tr>";

		if (EnableSign & SIGN_RR)
			s = s + "<tr><td bgcolor='#666666'>Zagrad:</td><td bgcolor='#AAAAAA'><a href='live://property/zagrad'>" + HTMLWindow.CheckBox("", openState & O_MK) + "close</a></td></tr>";

		s=s+"</table>";
		return s;
	}

	string GetContentViewDetails(void){
		string s=inherited();
//		s=s+"protective: "+protective+"<br>";
		s=s+"<table border='1' width=90%>";

		if (MU) {
			s = s + "<tr><td bgcolor='#666666'>MUstate:</td><td bgcolor='#AAAAAA'>";
			s = s + "<trainz-object width='30' height='17' style='text-line' align='left' max-chars='10' id='MUstate' text='" + MUstate + "'></trainz-object>";
			s = s + "&nbsp;<a href='live://property/MU_ON_AB'>" + HTMLWindow.CheckBox("", openState & O_MU_AB) + "on AB</a>";
			s = s + "&nbsp;<a href='live://property/MU_ON_ARS'>" + HTMLWindow.CheckBox("", openState & O_MU_ARS) + "on ARS</a>";
			s = s + "&nbsp;<a href='live://property/MU_ON_PS'>" + HTMLWindow.CheckBox("", openState & O_MU_PS) + "on PS</a>";
			s = s + "</td></tr>";
		}

		s=s+"<tr><td bgcolor='#666666'>name:</td><td bgcolor='#AAAAAA'>";
		if(name!="")s=s+name;
		else s=s+"<i>none</i>";
		s=s+"</td></tr>";

		float ASoffset=0.0;
		GSTrackSearch ts=BeginTrackSearch(false);
		while(ts.SearchNextObject()){
			if(ts.GetFacingRelativeToSearchDirection())continue;
			object obj=ts.GetObject();
			if(cast<kmAutoStop>obj){
				ASoffset=ts.GetDistance();
				continue;
			}
			if(cast<kmSignal>obj)
				break;
			if(cast<kmRC>obj)
				continue;
			if(cast<Signal>obj)break;//от дефолта мы ничего не получим
		}
		s=s+"<tr><td bgcolor='#666666'>protective:</td><td bgcolor='#AAAAAA'>"+protective+"&nbsp;(";
		kmRC sig=null;
		if(protective<=0)
			sig=me;//защитного участка нет, т.е. защитный участок - это мы и есть
		int n=protective;
		ts=BeginTrackSearch(true);
		while(!sig and ts.SearchNextObject()){
			if(!ts.GetFacingRelativeToSearchDirection())continue;
			object obj=ts.GetObject();
			if(cast<kmRC>obj){
				if((cast<kmRC>obj).GetMyType()&ST_SKIP_RC)
					continue;
				if(!(--n))
					sig=cast<kmRC>obj;
				continue;
			}
			if(cast<Signal>obj)break;//от дефолта мы ничего не получим
		}
		if(sig){
			string str=sig.GetPropertyValue("nameRC_clean");
			if(str=="")
				str="<i>uncknown</i>";
			s=s+str+",&nbsp;"+(ASoffset+ts.GetDistance())+"m";
		}
		else s=s+"<i>uncknown</i>";
		s=s+")</td></tr>";

		if (EnableSign & SIGN_B) {
			s = s + "<tr><td bgcolor='#666666'>rcForBlue:</td><td bgcolor='#AAAAAA'>" + rcForBlue + "&nbsp;(";
			sig = null;
			if (rcForBlue <= 0) {
				sig = me;
			}
			n = rcForBlue;
			ts = BeginTrackSearch(true);
			while (!sig and ts.SearchNextObject()) {
				if (!ts.GetFacingRelativeToSearchDirection()) {
					continue;
				}
				object obj = ts.GetObject();
				if (cast<kmRC>obj) {
					if ((cast<kmRC>obj).GetMyType() & ST_SKIP_RC) {
						continue;
					}
					if (!(--n)) {
						sig = cast<kmRC>obj;
					}
					continue;
				}
				if (cast<Signal>obj) {
					break;//от дефолта мы ничего не получим
				}
			}
			if (sig) {
				string str=sig.GetPropertyValue("nameRC_clean");
				if ("" == str) {
					str = "<i>uncknown</i>";
				}
				s = s + str + ",&nbsp;" + ts.GetDistance() + "m";
			}
			else {
				s = s + "<i>uncknown</i>";
			}
			s = s + ")</td></tr>";
		}

		if (EnableSign & SIGN_Wb)
			s = s + "<tr><td bgcolor='#666666'>Welcome:</td><td bgcolor='#AAAAAA'><a href='live://property/welcome'>" + HTMLWindow.CheckBox("", openState & O_PRIGL) + "open</a></td></tr>";

		if (EnableSign & SIGN_RR)
			s = s + "<tr><td bgcolor='#666666'>Zagrad:</td><td bgcolor='#AAAAAA'><a href='live://property/zagrad'>" + HTMLWindow.CheckBox("", openState & O_MK) + "close</a></td></tr>";

		s=s+"</table>";
		return s;
	}

	void LoadBrowserParam(Browser b){
		if(MU)
			b.SetElementProperty("MUstate", "text", MUstate);
	}

	string GetPropertyType(string id){
		if(id=="name")return "string";
//		if(id=="name_clean")return "string";
		if(id=="protective_count")return "int,0,10,1";
		if(id=="protective_down")return "link";
		if(id=="protective_up")return "link";
		if(id=="protective_name")return "list";
		if(id=="rcForBlue_count")return "int,0,10,1";
		if(id=="rcForBlue_down")return "link";
		if(id=="rcForBlue_up")return "link";
		if(id=="rcForBlue_name")return "list";
		if(id=="ARS_NO_R")return "link";
		if(id=="NO_PS")return "link";
		if(id=="lenses")return "string,0,10";//может ограничить числом линз?
		if(id[,5]=="lens_")return "link";
		if(id[,10]=="EnableSign")return "link";
		if(id=="MU_ON_AB")return "link";
		if(id=="MU_ON_ARS")return "link";
		if(id=="MU_ON_PS")return "link";
		if(id=="welcome")return "link";
		if(id=="zagrad")return "link";
		if(id=="usekit")return "list";
		if(id=="logkit")return "link";
		return inherited(id);
	}

	public string[] GetPropertyElementList(string id){
		if(id=="protective_name" or "rcForBlue_name" == id){
			string[] ret=new string[1];
			int i=0, n=Math.Max(5, Math.Max(protective, rcForBlue)+1);
			ret[0]=nameRC;
			kmRC sig;
			GSTrackSearch ts=BeginTrackSearch(true);
			while(i<n and ts.SearchNextObject()){
				if(!ts.GetFacingRelativeToSearchDirection())continue;
				object obj=ts.GetObject();
				if(sig=cast<kmRC>obj){
					if(!(sig.GetMyType()&ST_SKIP_RC))
						ret[++i]=sig.GetPropertyValue("nameRC_clean");
					continue;
				}
				if(cast<Signal>obj)break;//от дефолта мы ничего не получим
			}
			return ret;
		}
		if(id=="usekit"){
			string[] ret=new string[0];
			Soup cfg=self.GetConfigSoup().GetNamedSoup("extensions");
			if(!cfg)return ret;
			string type;
			if(MyType&S_TYPE_ARS){
				cfg=cfg.GetNamedSoup("ars_kits");
				type="ars_kit_";
			}
			else{
				cfg=cfg.GetNamedSoup("ab_kits");
				type="ab_kit_";
			}
			int n=cfg.GetNamedTagAsInt("count", 0);
			int i;
			StringTable myST=self.GetStringTable();
			for(i=0;i<n;i++)
				ret[i]=myST.GetString(type+i);
			return ret;
		}
		return inherited(id);
	}

	void SetPropertyValue(string id, string value, int index){
		/*if(id=="Open0")MUstate="";
		else */if(/*id[,4]=="Open" and */MU and mn)
			MUstate=mn.GetElementProperty("MUstate", "text");
		if(id=="protective_name")protective=index;
		else if ("rcForBlue_name" == id) rcForBlue = index;
		else if(id=="usekit"){
			Soup cfg=self.GetConfigSoup().GetNamedSoup("extensions");
			if(!cfg)return;
			if(MyType&S_TYPE_ARS)
				cfg=cfg.GetNamedSoup("ars_kits");
			else
				cfg=cfg.GetNamedSoup("ab_kits");
			if(!cfg)return;
			cfg=cfg.GetNamedSoup(index);
			if(!cfg)return;
			protective=cfg.GetNamedTagAsInt("protective", 1);
			SetLenses(cfg.GetNamedTagAsInt("lenses", 0));
			EnableSign=cfg.GetNamedTagAsInt("enablesign", 0);
			int i;
			for(i=0;i<lensesA.size();i++){
				lensesA[i]=cfg.GetNamedTagAsInt("lensesA"+i, 0);
				lensesB[i]=cfg.GetNamedTagAsInt("lensesB"+i, 0);
			}
		}
		else inherited(id, value, index);
	}

	void SetPropertyValue(string id, string value){
		/*if(id=="Open0")MUstate="";
		else */if(/*id[,4]=="Open" and */MU and mn)
			MUstate=mn.GetElementProperty("MUstate", "text");
		if(id=="name")SetName(value);
		else if(id=="lenses"){
			int i, tmp=0;
			for(i=value.size();i>0;i--){
				tmp=tmp<<3;
				tmp=tmp+(Str.ToInt(value[i-1,i])&7);
			}
			SetLenses(tmp);
//			for(i=0;i<lensesA.size();i++)linsesA[i]=0;//set
//			for(i=0;i<lensesB.size();i++)linsesB[i]=0;//defaults
		}
		else inherited(id, value);
	}

	void SetPropertyValue(string id, int value){
		/*if(id=="Open0")MUstate="";
		else */if(/*id[,4]=="Open" and */MU and mn)
			MUstate=mn.GetElementProperty("MUstate", "text");
		if(id=="protective_count")protective=value;
		else if ("rcForBlue_count" == id) rcForBlue = value;
		else inherited(id, value);
	}

	void LinkPropertyValue(string id){
		/*if(id=="Open0")MUstate="";
		else */if(/*id[,4]=="Open" and */MU and mn)
			MUstate=mn.GetElementProperty("MUstate", "text");
		if(id=="protective_up")protective++;
		else if(id=="protective_down")protective--;
		else if ("rcForBlue_up" == id) ++rcForBlue;
		else if ("rcForBlue_down" == id) --rcForBlue;
		else if(id=="ARS_NO_R")MyType=MyType^ST_ARS_NO_R;
		else if(id=="NO_PS")MyType=MyType^ST_NO_PS;
		else if(id[,5]=="lens_"){
			int i, j;
			string str=id[5,];
			string[] tok=Str.Tokens(id[5,], "_");
			if(tok.size()>1){
				i=Str.ToInt(tok[0]);
				j=Str.ToInt(tok[1]);
				j=1<<j;
				if(lensesA[i]&j){
					if(lensesB[i]&j){
						lensesA[i]=lensesA[i]^j;
						lensesB[i]=lensesB[i]^j;
					}
					else{
						lensesB[i]=lensesB[i]^j;
					}
				}
				else{
					lensesA[i]=lensesA[i]|j;
					lensesB[i]=lensesB[i]&~j;
				}
			}
		}
		else if(id[,10]=="EnableSign")EnableSign=EnableSign^Str.ToInt(id[10,]);
		else if(id=="welcome"){
			if (openState & O_PRIGL)
				WelcomeCancel();
			else{
				string s;
				if(MU and mn and (s=mn.GetElementProperty("MUstate", "text")))
					MUstate=s;
				WelcomeOpen(null);
			}
			if(MultiplayerGame.IsActive()){
				Soup SyncSoup=Constructors.NewSoup();
				SyncSoup.SetNamedTag("function", "WelcomeState");
				SyncSoup.SetNamedTag("id", GetGameObjectID());
				SyncSoup.SetNamedTag("state", openState & O_PRIGL);
				if(openState & O_MU_PS)
					SyncSoup.SetNamedTag("MUstate", MUstate);
				MultiplayerGame.BroadcastGameplayMessage("kmSigMP", "Sync", SyncSoup);
			}
		}
		else if(id=="MU_ON_AB")openState = openState ^ O_MU_AB;
		else if(id=="MU_ON_ARS")openState = openState ^ O_MU_ARS;
		else if(id=="MU_ON_PS")openState = openState ^ O_MU_PS;
		else if(id=="zagrad"){
			openState = openState ^ O_MK;
			kmLib.Update(me, true, null);
		}
		else if(id=="logkit"){
			Interface.Log(self.GetKUID().GetLogString()+", "+self.GetLocalisedName());
			
			string ret="";
			int tmp=lenses;
			int i, n;
			for(i=0;i<10;i++){
				ret=ret+ST.GetString("colorcode"+(tmp&7));
				tmp=tmp>>3;
			}
			if(ret=="")ret="-";
			Interface.Log("lenses: "+ret);
			Interface.Log("Type: "+MyType);
			Interface.Log("**** start lenses kit log ****");
			Interface.Log("lenses "+lenses);
			Interface.Log("enablesign "+EnableSign);
			n=lensesA.size();
			for(i=0;i<n;i++){
				if(lensesA[i])Interface.Log("lensesa"+i+" "+lensesA[i]);
				if(lensesB[i])Interface.Log("lensesb"+i+" "+lensesB[i]);
			}
			if(protective!=1)Interface.Log("protective "+protective);
			Interface.Log("**** end lenses kit log ****");
		}
		else
			inherited(id);
	}

	public string GetPropertyValue(string id){
		if(id=="name")return name;
		if(id=="name_clean"){
			string[] tmp=GetChars(name);
			string ret="";
			int i;
			for(i=0;i<tmp.size();i++)
				ret=ret+tmp[i];
			return ret;
		}
		if(id=="protective_count")return (string)protective;
//		if(id=="protective_name")return (string)protective;
		if(id=="protective_name"){
			if(protective<=0)
				return nameRC;//защитного участка нет, т.е. защитный участок - это мы и есть
			kmRC sig=null;
			int n=protective;
			GSTrackSearch ts=BeginTrackSearch(true);
			while(!sig and ts.SearchNextObject()){
				if(!ts.GetFacingRelativeToSearchDirection())continue;
				object obj=ts.GetObject();
				if(cast<kmRC>obj){
					if((cast<kmRC>obj).GetMyType()&ST_SKIP_RC)
						continue;
					if(!(--n))
						sig=cast<kmRC>obj;
					continue;
				}
				if(cast<Signal>obj)break;//от дефолта мы ничего не получим
			}
			if(sig)return sig.GetPropertyValue("nameRC_clean");
			return "";
		}
		if ("rcForBlue_count" == id) {
			return (string)rcForBlue;
		}
		if ("rcForBlue_name" == id){
			if (rcForBlue <= 0) {
				return nameRC;
			}
			kmRC sig = null;
			int n = rcForBlue;
			GSTrackSearch ts = BeginTrackSearch(true);
			while (!sig and ts.SearchNextObject()) {
				if (!ts.GetFacingRelativeToSearchDirection()) {
					continue;
				}
				object obj = ts.GetObject();
				if (cast<kmRC>obj) {
					if ((cast<kmRC>obj).GetMyType() & ST_SKIP_RC) {
						continue;
					}
					if (!(--n)) {
						sig = cast<kmRC>obj;
					}
					continue;
				}
				if (cast<Signal>obj) {
					break;//от дефолта мы ничего не получим
				}
			}
			if (sig) {
				return sig.GetPropertyValue("nameRC_clean");
			}
			return "";
		}
		if(id=="lenses"){
			string ret="";
			int tmp=lenses;
			int i;
			for(i=0;i<10;i++){
				ret=ret+(tmp&7);
				tmp=tmp>>3;
			}
			if(ret=="")return "-";
			return ret;
		}
		return inherited(id);
	}

	public Soup GetProperties(void){
		Soup sp=inherited();
		int i;
		sp.SetNamedTag("protective", protective);
		sp.SetNamedTag("rcForBlue", rcForBlue);
		sp.SetNamedTag("name", name);
		sp.SetNamedTag("lenses", lenses);
		for(i=0;i<11;i++)
			if(lensesA[i])
				sp.SetNamedTag("lensesA"+i, lensesA[i]);
		for(i=0;i<11;i++)
			if(lensesB[i])
				sp.SetNamedTag("lensesB"+i, lensesB[i]);
		sp.SetNamedTag("EnableSign", EnableSign);
		sp.SetNamedTag("MyState", MyState);
		if(MUstate!="")
			sp.SetNamedTag("MUstate", MUstate);
		return sp;
	}

	public void SetProperties(Soup sp){
		SetName(sp.GetNamedTag("name"));
		MUstate=sp.GetNamedTag("MUstate");
		if(MUstate=="")MUstate=sp.GetNamedTag("wMUstate");
//		MyState=sp.GetNamedTagAsInt("MyState");
		rcForBlue = sp.GetNamedTagAsInt("rcForBlue", 1);
		protective=sp.GetNamedTagAsInt("protective", -1);
		if(protective==-1){
			inherited(sp);
			Soup cfg=self.GetConfigSoup().GetNamedSoup("extensions");
			if(!cfg)return;
			if(MyType&S_TYPE_ARS)
				cfg=cfg.GetNamedSoup("ars_kits");
			else
				cfg=cfg.GetNamedSoup("ab_kits");
			if(!cfg)return;
			cfg=cfg.GetNamedSoup(cfg.GetNamedTagAsInt("default", 0));
			if(!cfg)return;
			protective=cfg.GetNamedTagAsInt("protective", 1);
			SetLenses(cfg.GetNamedTagAsInt("lenses", 0));
			EnableSign=cfg.GetNamedTagAsInt("enablesign", 0);
			int i;
			lensesA=new int[11];
			lensesB=new int[11];
			for(i=0;i<lensesA.size();i++){
				lensesA[i]=cfg.GetNamedTagAsInt("lensesA"+i, 0);
				lensesB[i]=cfg.GetNamedTagAsInt("lensesB"+i, 0);
			}
			WelcomeBlinker();
			kmLib.Update(me, true, null);
		}
		else{
			SetLenses(sp.GetNamedTagAsInt("lenses"));
			int i;
			lensesA=new int[11];
			for(i=0;i<11;i++)lensesA[i]=sp.GetNamedTagAsInt("lensesA"+i, 0);
			lensesB=new int[11];
			for(i=0;i<11;i++)lensesB[i]=sp.GetNamedTagAsInt("lensesB"+i, 0);
			EnableSign=sp.GetNamedTagAsInt("EnableSign", 41);
//			NewSignal();
			inherited(sp);
			UpdateMyState();
			WelcomeBlinker();
			ApplySignalState();//наверно всё же проще вызвать эту функцию дважды, чем долго думать кто в каком порядке должен выполняться
		}
	}

	public void AutoStopMode(Message msg) {
		Interface.Print("signal: " + name + " (" + nameRC + ") autostop mode " + msg.minor);
	}

	public void Init(Asset asset){
		inherited(asset);
		lensesA=new int[11];
		lensesB=new int[11];
		MU=GetFXAttachment("mu");
//		if(MU)MU.SetMesh("mu_3", 0.0);
AddHandler(me, "kmAutoStopMode", "", "AutoStopMode");
	}
};