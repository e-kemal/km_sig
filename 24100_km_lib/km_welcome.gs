include "km_junction_info.gs"

class kmWelcomePath
{
    public kmRouteAbstract[] conflictRoutes = new kmRouteAbstract[0];
    public kmJunctionInfo[] junctions = new kmJunctionInfo[0];
    public bool[] jDirs = new bool[0];
    public string mu;

    public bool checkPath() {
        int i;
        for (i = 0; i < conflictRoutes.size(); ++i) {
            if (conflictRoutes[i].isKN()) {
                return false;
            }
        }
        for (i = 0; i < junctions.size(); ++i) {
            if (jDirs[i]) {
                if (!junctions[i].isPK()) {
                    return false;
                }
            }
            else {
                if (!junctions[i].isMK()) {
                    return false;
                }
            }
        }

        return true;
    }
};

class kmWelcome isclass kmObserver
{
    kmDscpPost owner;
    int index;
    public string name;
    public kmSignal sign;
    public kmWelcomePath[] paths = new kmWelcomePath[0];
    kmWelcomePath currentPath;
    bool enabled;
    bool active;

    public string getHtml() {
        string ret = index + ". <font color=\"#";
        if (active) {
            ret = ret + "ffffff";
        }
        else {
            ret = ret + "000000";
        }
        ret = ret + "\">*</font>&nbsp;<a href=\"live://property/w" + index + "/open\">" + name + "</a><br>";

        return ret;
    }

    public string getCurrentMU() {
        if (active and currentPath) {
            return currentPath.mu;
        }

        return "";
    }

    void activate(kmWelcomePath path) {
        currentPath = path;
        active = true;
        sign.WelcomeOpen(me);
    }

    void deactivate() {
        active = false;
        currentPath = null;
        sign.WelcomeCancel();
    }

    kmWelcomePath findPath() {
        int i;
        for (i = 0; i < paths.size(); ++i) {
            if (paths[i].checkPath()) {
                return paths[i];
            }
        }

        return null;
    }

    bool isEnabled() {
        return enabled;
    }

    void onEnable() {
        int i, j;
        for (i = 0; i < paths.size(); ++i) {
            for (j = 0; j < paths[i].conflictRoutes.size(); ++j) {
                paths[i].conflictRoutes[j].addObserver(me);
            }
            for (j = 0; j < paths[i].junctions.size(); ++j) {
                paths[i].junctions[j].addObserver(me);
            }
        }
    }

    void onDisable() {
        int i, j;
        for (i = 0; i < paths.size(); ++i) {
            for (j = 0; j < paths[i].conflictRoutes.size(); ++j) {
                paths[i].conflictRoutes[j].removeObserver(me);
            }
            for (j = 0; j < paths[i].junctions.size(); ++j) {
                paths[i].junctions[j].removeObserver(me);
            }
        }
    }

    public bool tryOpen() {
        kmWelcomePath path;
        if (!paths.size() or (path = findPath())) {
            enabled = true;
            onEnable();
            activate(path);

            return true;
        }

        return false;
    }

    public void close() {
        enabled = false;
        if (!isEnabled()) {
            deactivate();
        }
    }

    public void handleEvent(kmObservable source, int type) {
        if ((
            (cast<kmJunctionInfo>source and kmJunctionInfo.EVENT_START_MOVING == type)
            or (cast<kmRouteAbstract>source and kmRouteAbstract.EVENT_ON_KN == type)
        ) and active) {
            if (currentPath and !currentPath.checkPath()) {
                deactivate();
            }
        }
        if ((
            (cast<kmJunctionInfo>source and kmJunctionInfo.EVENT_STOP_MOVING == type)
            or (cast<kmRouteAbstract>source and (kmRouteAbstract.EVENT_OFF_KN == type or kmRouteAbstract.EVENT_OFF_KN_OMO == type))
        ) and !active) {
            kmWelcomePath path = findPath();
            if (path) {
                activate(path);
            }
        }
    }

    public void init(kmDscpPost post, int i) {
        if (!sign) {
            Interface.Exception("post " + post.getStationName() + " (" + post.getStationCode() + "), welcome " + name + " sign is null");
        }
        owner = post;
        index = i;
    }
};

class kmWelcomeAuto isclass kmWelcome
{
    public int timeout = 30;        //45 для выходного
    public kmRouteAutoKN[] autoRoutes = new kmRouteAutoKN[0];
    public int checkPCount = 1;
    bool enabledAuto;
    bool AS, nP, nRO;
    kmRC endPObjectCache;

    bool isEnabled() {
        return enabled or enabledAuto;
    }

    bool checkAS() {
        int i;
        for (i = 0; i < autoRoutes.size(); ++i) {
            if (autoRoutes[i].isKN()) {
                return true;
            }
        }

        return false;
    }

    bool checkP() {
        int n = Math.Abs(checkPCount);
        GSTrackSearch ts = sign.BeginTrackSearch(false);
        bool clear = true;
        while (ts.SearchNextObject()) {
            object obj = ts.GetObject();
            if (cast<Vehicle>obj) {
                clear = false;
                if (endPObjectCache) {
                    return false;
                }
            }
            kmRC rc = cast<kmRC>obj;
            if (rc and !(rc.GetMyType() & kmRC.ST_SKIP_RC) and (ts.GetFacingRelativeToSearchDirection() != (checkPCount > 0)) and !(--n)) {
                if (!endPObjectCache) {
                    endPObjectCache = rc;
                    owner.getExternalObserverContainerForObject(rc).addObserver(me);
                }
                clear = clear and owner.getMainLib().checkOverlap(ts);
                break;
            }
        }

        return clear and owner.getMainLib().checkOverlap(sign.BeginTrackSearch(true));
    }

    bool checkRO() {
        int state = sign.GetMyState();
        return kmSignal.STATE_Y <= state and state <= kmSignal.STATE_B;
    }

    public void onAutoTimer() {
        owner.ClearMessages("kmWelcomeAutoTimer", index);
        if (enabledAuto = AS and nP and nRO) {
            kmWelcomePath path;
            if (!paths.size() or (path = findPath())) {
                onEnable();
                activate(path);
            }
        }
    }

    void tryOpenAuto() {
        if (AS and nP and nRO) {
            owner.PostMessage(owner, "kmWelcomeAutoTimer", index, timeout);
        }
    }

    void closeAuto() {
        owner.ClearMessages("kmWelcomeAutoTimer", index);
        enabledAuto = false;
        if (!isEnabled()) {
            deactivate();
        }
    }

    public void handleEvent(kmObservable source, int type) {
        inherited(source, type);
        if (cast<kmRouteAutoKN>source) {
            if (kmRouteAbstract.EVENT_ON_KN == type and !AS) {
                if (AS = checkAS()) {
                    owner.getExternalObserverContainerForObject(sign).addObserver(me);
                    nP = !checkP();
                    nRO = !checkRO();
                    tryOpenAuto();
                }
            }
            if ((kmRouteAbstract.EVENT_OFF_KN == type or kmRouteAbstract.EVENT_OFF_KN_OMO == type) and AS) {
                if (!(AS = checkAS())) {
                    closeAuto();
                    owner.getExternalObserverContainerForObject(sign).removeObserver(me);
                    if (endPObjectCache) {
                        owner.getExternalObserverContainerForObject(endPObjectCache).removeObserver(me);
                        endPObjectCache = null;
                    }
                }
            }
        }
        if (cast<kmSignal>source and kmSignal.EVENT_STATE_CHANGED == type) {
            if (nRO = !checkRO()) {
                tryOpenAuto();
            }
            else {
                closeAuto();
            }
        }
        else if (source == sign or source == endPObjectCache) {
            if (nP = !checkP()) {
                tryOpenAuto();
            }
            else {
                closeAuto();
            }
        }
    }

    public void init(kmDscpPost post, int i) {
        inherited(post, i);
        int j;
        for (j = 0; j < autoRoutes.size(); ++j) {
            autoRoutes[j].addObserver(me);
        }
    }

    public void loadProperties(Soup soup) {
        if (AS = checkAS()) {
            owner.getExternalObserverContainerForObject(sign).addObserver(me);
            nP = !checkP();
            nRO = !checkRO();
            tryOpenAuto();
        }
    }
};
