include "km_observable.gs"

class kmObserver
{
    public void handleEvent(kmObservable source, int type) {}
};

class kmExternalEvent
{
    public kmObservable source;
    public kmExternalObserverContainer container;
    public int type;
};

class kmExternalObserver
{
    kmExternalEvent[] externalEvents = new kmExternalEvent[0];

    void notifySelfAboutExternalEvent() {
//        PostMessage(me, "kmExternalEvent", "process", 0);
    }

    public void addExternalEvent(kmExternalEvent event) {
        externalEvents[externalEvents.size()] = event;
        notifySelfAboutExternalEvent();
    }

    void processExternalEvents() {
        while (externalEvents.size()) {
            kmExternalEvent event = externalEvents[0];
            externalEvents[0,1] = null;
            int i;
            kmObserver[] observers;
            observers.copy(event.container.getObservers());
            for (i = 0; i < observers.size(); ++i) {
                observers[i].handleEvent(event.source, event.type);
            }
        }
    }

    public kmExternalObserverContainer getExternalObserverContainerForObject(kmObservable observable) {
        int i;
        kmObserver[] observers = observable.getObservers();
        for (i = 0; i < observers.size(); ++i) {
            kmExternalObserverContainer container = cast<kmExternalObserverContainer>observers[i];
            if (container and me == container.owner) {
                return container;
            }
        }

        kmExternalObserverContainer container = new kmExternalObserverContainer();
        container.observable = observable;
        container.owner = me;

        return container;
    }
};
