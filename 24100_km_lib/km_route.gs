include "km_section.gs"
include "km_signal_switch_interface.gs"

class kmKNInterface
{
	public bool isKN() {
		return false;
	}
};

class kmRouteAbstract isclass kmKNInterface, kmObservable
{
	kmDscpPost owner;
	int index;
	public string name;
	public string desctiption;
	public kmKNInterface[] conflictRoutes = new kmKNInterface[0];

	public define int EVENT_ON_KN		= 0;
	public define int EVENT_OFF_KN		= 1;
	public define int EVENT_OFF_KN_OMO	= 2;

	public define int EVENT_ROUTE_ABSTRACT_LAST = 2;

	bool isIndication() {
		return isKN();
	}

	bool hasBtn() {
		return true;
	}

	public string getHtml() {
		string ret = index + ". <font color=\"#";
		if (isIndication()) {
			ret = ret + "ffffff";
		}
		else {
			ret = ret + "000000";
		}
		ret = ret + "\">*</font>&nbsp;";
		if (hasBtn()) {
			ret = ret + "<a href=\"live://property/r" + index + "/kn\"";
			if (desctiption) {
//				ret = ret + " tooltip=\"" + BrowserInterface.Quote(desctiption) + "\"";
				ret = ret + " tooltip='" + TrainUtil.StrSubst(desctiption, "'", "\"") + "'";
			}
			ret = ret + ">" + name + "</a>";
		}
		else {
			ret = ret + name;
		}
		ret = ret + "<br>";

		return ret;
	}

	public void touch() {}

	public void make() {}

	public void cancel() {}

	void onKN() {
		make();
	}

	void offKN(bool OMO) {
		cancel();
	}

	public void guiAction(string cmd) {
		if ("kn" == cmd) {
			if (owner.getGKNO()) {
				bool OMO = owner.getGKNO() > 1;
				if (isKN()) {
					owner.resetGKNO();
				}
				offKN(OMO);
				if (OMO) {
					notifyObservers(EVENT_OFF_KN_OMO);
				}
				else {
					notifyObservers(EVENT_OFF_KN);
				}
			}
			else {
				if (!isKN()) {
					int i;
					for (i = 0; i < conflictRoutes.size(); ++i) {
						if (conflictRoutes[i].isKN()) {
							return;
						}
					}
					onKN();
					if (isKN()) {
						notifyObservers(EVENT_ON_KN);
					}
				}
			}
		}
		else if ("touch" == cmd) {
			touch();
		}
	}

	public void init(kmDscpPost post, int i) {
		owner = post;
		index = i;
	}

	public void saveProperties(Soup soup) {}

	public void loadProperties(Soup soup) {}
};

class kmRoute isclass kmRouteAbstract, kmSignalSwitchInterface, kmObserver
{
	public define int R_NONE	= 0;
	public define int R_KN		= 1;
	public define int R_GS		= 2;
	public define int R_PP		= 4;

	int state = R_NONE;

	public kmRC startObject;
	public bool startDir = true;
	public kmRC checkMNObject;
	public int checkMNCount;
	public int preRouteCount;
	public int routeCount;
	public int routeCountOnce;
	public int preRouteOpenCount;
	public int type;
	public string mu;

	public kmJunctionInfo[] junctions = new kmJunctionInfo[0];
	public bool[] jDirs = new bool[0];
	public kmSection[] sections = new kmSection[0];
	public kmSection[] conflictSections = new kmSection[0];
	public kmRouteAbstract autoAction;
	public kmRoute[] parentRoutes;

	public define int EVENT_OPENED		= kmRouteAbstract.EVENT_ROUTE_ABSTRACT_LAST + 1;

	kmRC endMNObjectCache;
	kmRC endGSObjectCache;
	kmRC endGSOnceObjectCache;
	kmRC endPPObjectCache;
	kmRC[] openedCache = new kmRC[0];

	bool isIndication() {
		return state & R_KN and (!sections[0].isZind() or owner.getMainLib().getBlinkerState());
	}

	bool hasBtn() {
		return !parentRoutes or !parentRoutes.size() or owner.debug;
	}

	public string getHtml() {
		string ret = inherited();
		if (owner.debug) {
			ret = ret + "GS: " + (state & R_GS) + ", PP: " + (state & R_PP) + ", <a href=\"live://property/r" + index + "/touch\">touch</a>";
			if (openedCache.size()) {
				ret = ret + "<br>openedCache: ";
				int i;
				for (i = 0; i < openedCache.size(); ++i) {
					if (i) {
						ret = ret + ", ";
					}
					ret = ret + openedCache[i].GetName();
				}
			}
			ret = ret + "<br>";
		}
		return ret;
	}

	public bool isKN() {
		return !!(state & R_KN);
	}

	public bool isGS() {
		return !!(state & R_GS);
	}

	public bool isLockJunctions() {
		return false;
	}

	public void tryMake() {
//		if (sections[0].route != null and sections[0].route != me) {
//			return;
//		}
//		if (sections[0].state > 1) {
//			return;
//		}
		int i;


		bool GS = true;

		bool endDir = false;
		kmRC start = sections[0].startObject;
		kmRC end = sections[sections.size() - 1].endObject;

		//ToDo: проверить свободность маршрутных секций, в том числе негабаритные
		int postRouteCount = sections[sections.size() - 1].postRouteCount;
		kmRC end2 = end;

		if (GS) {
			GSTrackSearch ts;
			bool inRoute;
			if (postRouteCount > 0) {
				ts = end2.BeginTrackSearch(false);
				inRoute = false;
//				if (arr) {
//					end2.Close(true);//в маршруте приёма конечный объект должен быть закрыт
//				}
//				else {
					bool auto = !(end2.GetMyType() & kmRC.ST_ROUTE_ONLY);
					if (cast<kmSignal>end2) {
						(cast<kmSignal>end2).Open(type & kmRC.O_TYPE, auto, mu, type & kmRC.OT_USE_MU);
					}
					else {
						end2.Open(type & kmRC.O_TYPE, auto);
					}
//				}
			}
			else {
				ts = end.BeginTrackSearch(!endDir);
				inRoute = true;
				if (!endDir) {
//Interface.Print("route: close " + end.GetPropertyValue("name") + " (" + end.GetPropertyValue("nameRC") + ")");
					end.Close(true);//Раз это встречный маршрут, то должен быть и закрытый светофор. Поэтому запускать обновление здесь не нужно
					if (cast<kmSignal>end) {
Interface.Print("route: openAS " + end.GetPropertyValue("name") + " (" + end.GetPropertyValue("nameRC") + ")");
						owner.PostMessage(end, "kmAutoStopMode", "open", 0.1);
					}
				}
				else {

				}
			}
			while (ts.SearchNextObject()) {
				kmRC sig = cast<kmRC>ts.GetObject();
				if (!sig) {
					continue;
				}
				if (sig == end) {
					inRoute = true;
				}
				if (ts.GetFacingRelativeToSearchDirection()) {
					sig.Close(true);//Раз это встречный маршрут, то должен быть и закрытый светофор. Поэтому запускать обновление здесь не нужно
					if (inRoute and cast<kmSignal>sig) {
						owner.PostMessage(sig, "kmAutoStopMode", "open", 0.1);
					}
				}
				else {

				}
				if (sig == start) {
					break;
				}
			}
			if (postRouteCount > 0) {
//				lib.Update(end2, true, start);//ToDo: добавить флаг, при котором светофор с белым будет сообщать предыдущему, что свободных блок участков нет
			}
			else {
//				lib.Update(end, endDir, start);//ToDo: добавить флаг, при котором светофор с белым будет сообщать предыдущему, что свободных блок участков нет
			}
		}
	}

	bool checkMN() {
		if (checkMNObject and checkMNCount) {
			int n = Math.Abs(checkMNCount);
			GSTrackSearch ts = checkMNObject.BeginTrackSearch(false);
			bool clear = true;
			while (ts.SearchNextObject()) {
				object obj = ts.GetObject();
				if (cast<Vehicle>obj) {
					clear = false;
					if (endMNObjectCache) {
						return false;
					}
				}
				kmRC rc = cast<kmRC>obj;
				if (rc and !(rc.GetMyType() & kmRC.ST_SKIP_RC) and (ts.GetFacingRelativeToSearchDirection() != (checkMNCount > 0)) and !(--n)) {
					if (!endMNObjectCache) {
						endMNObjectCache = rc;
						owner.getExternalObserverContainerForObject(rc).addObserver(me);
						owner.getExternalObserverContainerForObject(checkMNObject).addObserver(me);
					}
					clear = clear and owner.getMainLib().checkOverlap(ts);
					break;
				}
			}
			return clear and !n and owner.getMainLib().checkOverlap(checkMNObject.BeginTrackSearch(true));
		}

		return true;
	}

	bool checkJunctions() {
		int i;
		for (i = 0; i < junctions.size(); ++i) {
			if (jDirs[i]) {
				if (!junctions[i].isPK()) {
					return false;
				}
			}
			else {
				if (!junctions[i].isMK()) {
					return false;
				}
			}
		}

		return true;
	}

	void prepareJunctions() {
		if (!checkMN()) {
			return;
		}
		int i;
		for (i = 0; i < junctions.size(); ++i) {
			if (jDirs[i]) {
				junctions[i].PU();
			}
			else {
				junctions[i].MU();
			}
		}
	}

	bool checkGS() {
		if (!checkJunctions()) {
			return false;
		}
		int i;
		for (i = 0; i < conflictSections.size(); ++i) {
			if (!conflictSections[i].isZ()) {
				return false;
			}
		}
		for (i = 0; i < sections.size(); ++i) {
			if (sections[i].route and me != sections[i].route) {
				return false;
			}
			if (!(state & R_PP) and sections[i].isM()) {
				return false;
			}
		}
		if (!routeCount and !routeCountOnce) {
			return true;
		}
		int n = Math.Abs(routeCount);
		GSTrackSearch ts = startObject.BeginTrackSearch(startDir);
		bool clear = true;
		if (n) {
			while (ts.SearchNextObject()) {
				object obj = ts.GetObject();
				if (cast<Vehicle>obj) {//Занято((
					clear = false;
					if (endGSObjectCache) {
						return false;
					}
				}
				kmRC rc = cast<kmRC>obj;
				if (rc and !(rc.GetMyType() & kmRC.ST_SKIP_RC) and (ts.GetFacingRelativeToSearchDirection() == (routeCount > 0)) and !(--n)) {
					if (!endGSObjectCache) {
						endGSObjectCache = rc;
						owner.getExternalObserverContainerForObject(rc).addObserver(me);
					}
					break;
				}
			}
		}
		if (!clear or n) {
			return false;
		}
		if (!(state & R_GS) and routeCountOnce) {
			n = Math.Abs(routeCountOnce);
			while (ts.SearchNextObject()) {
				object obj = ts.GetObject();
				if (cast<Vehicle>obj) {//Занято((
					clear = false;
					if (endGSOnceObjectCache) {
						return false;
					}
				}
				kmRC rc = cast<kmRC>obj;
				if (rc and !(rc.GetMyType() & kmRC.ST_SKIP_RC) and (ts.GetFacingRelativeToSearchDirection() == (routeCountOnce > 0)) and !(--n)) {
					if (!endGSOnceObjectCache) {
						endGSOnceObjectCache = rc;
						owner.getExternalObserverContainerForObject(rc).addObserver(me);
					}
					break;
				}
			}
		}

		if (!clear or n or !owner.getMainLib().checkOverlap(ts) or !owner.getMainLib().checkOverlap(startObject.BeginTrackSearch(!startDir))) {
			return false;
		}
		if (endGSOnceObjectCache) {
			owner.getExternalObserverContainerForObject(endGSOnceObjectCache).removeObserver(me);
			endGSOnceObjectCache = null;
		}

		return true;
	}

	bool checkPP() {
		if (!preRouteCount) {
			return false;
		}
		int n = Math.Abs(preRouteCount);
		GSTrackSearch ts = startObject.BeginTrackSearch(!startDir);
		while (ts.SearchNextObject()) {
			object obj = ts.GetObject();
			if (cast<Vehicle>obj) {//Занято((
				return false;
			}
			kmRC rc = cast<kmRC>obj;
			if (rc and !(rc.GetMyType() & kmRC.ST_SKIP_RC) and (ts.GetFacingRelativeToSearchDirection() != (preRouteCount > 0)) and !(--n)) {
				if (!endPPObjectCache) {
					endPPObjectCache = rc;
					owner.getExternalObserverContainerForObject(rc).addObserver(me);//		если ранее мы уже обнаружили поезд, то сюда мы не дойдём. Но для ПП это и не важно
				}
				break;
			}
		}

		return owner.getMainLib().checkOverlap(ts) and owner.getMainLib().checkOverlap(startObject.BeginTrackSearch(startDir));
	}

	void openSignals() {
		openedCache = new kmRC[0];
		if (startDir) {
			openedCache[0] = startObject;
		}
		kmRC start = startObject;
		if (preRouteOpenCount) {
			int n = Math.Abs(preRouteOpenCount);
			GSTrackSearch ts = startObject.BeginTrackSearch(!startDir);
			while (ts.SearchNextObject()) {
				kmRC sig = cast<kmRC>ts.GetObject();
				if (!sig) {
					continue;
				}
				if (!ts.GetFacingRelativeToSearchDirection()) {
					openedCache[openedCache.size()] = sig;
				}
				start = sig;
				if (!(sig.GetMyType() & kmRC.ST_SKIP_RC) and (ts.GetFacingRelativeToSearchDirection() != (preRouteOpenCount > 0)) and !(--n)) {
					break;
				}
			}
		}
		int i;
		for (i = 0; i < openedCache.size(); ++i) {
			openedCache[i].addSurOnSwithcer(me);
			if (0 == i) {
				openedCache[i].addClearOnSwithcer(me);
			}
			openedCache[i].setOpenState(kmRC.O_SUR | kmRC.O_CLEAR | kmRC.O_CODES | type);
			if (cast<kmSignal>openedCache[i]) {
				(cast<kmSignal>openedCache[i]).setMUstate(mu);
			}
		}

		kmRcAndDirPair updateEnd = new kmRcAndDirPair();
		for (i = 0; i < sections.size(); ++i) {
			sections[i].openSignals(updateEnd);
		}
		owner.getMainLib().Update(updateEnd.sig, updateEnd.dir, start);
		notifyObservers(EVENT_OPENED);
	}

	void closeSignals() {
		int i;
		for (i = 0; i < openedCache.size(); ++i) {
			openedCache[i].removeSurOnSwitcher(me);
			openedCache[i].resetSur();
			if (0 == i) {
				openedCache[i].removeClearOnSwitcher(me);
				openedCache[i].resetClear();
			}
		}
		if (openedCache.size()) {
			owner.getMainLib().Update(openedCache[0], true, openedCache[openedCache.size() - 1]);//todo: метод ядра, обновляющий по массиву объектов
		}
		openedCache = new kmRC[0];

		for (i = 0; i < sections.size(); ++i) {
			if (me == sections[i].route) {
				sections[i].resetGS();
			}
		}
	}

	void tryLock() {
		if (checkGS()) {
			state = state | R_GS;
			int i;
			for (i = 0; i < sections.size(); ++i) {
				sections[i].lock(me);
			}
			openSignals();
			owner.getExternalObserverContainerForObject(startObject).addObserver(me);
			sections[0].addObserver(me);
		}
	}

	bool hasAuto() {
		if (parentRoutes) {
			int i;
			for (i = 0; i < parentRoutes.size(); ++i) {
				if (parentRoutes[i].hasAuto()) {
					return true;
				}
			}
		}
		return !!autoAction;
	}

	public void touch() {
		if (!(state & R_KN)) {
			return;
		}
		if ((state & R_GS) and !checkGS()) {
			state = state & ~R_GS;
			closeSignals();
			return;
		}
		if (!(state & R_GS) and ((state & R_PP) or hasAuto())) {
			tryLock();
		}
		if (!(state & R_GS)) {
			prepareJunctions();
		}
		if ((state & R_GS) and ((state & R_PP) or hasAuto()) and !checkPP()) {
			state = state & ~R_PP;
			int i;
			for (i = 0; i < sections.size(); ++i) {
				sections[i].fullLock();
			}
		}
	}

	public void make() {
		int i;
		for (i = 0; i < junctions.size(); ++i) {
			junctions[i].addObserver(me);
		}
		for (i = 0; i < conflictSections.size(); ++i) {
			conflictSections[i].addObserver(me);
		}
		state = state | R_KN | R_PP;
		touch();
	}

	public void cancel() {
		autoAction = null;
		if (state & R_GS) {
			closeSignals();
		}
		state = R_NONE;
		int i;
		for (i = 0; i < junctions.size(); ++i) {
			junctions[i].removeObserver(me);
		}
		for (i = 0; i < conflictSections.size(); ++i) {
			conflictSections[i].removeObserver(me);
		}
		if (endMNObjectCache) {
			owner.getExternalObserverContainerForObject(checkMNObject).removeObserver(me);
			owner.getExternalObserverContainerForObject(endMNObjectCache).removeObserver(me);
			endMNObjectCache = null;
		}
		owner.getExternalObserverContainerForObject(startObject).removeObserver(me);
		if (endGSObjectCache) {
			owner.getExternalObserverContainerForObject(endGSObjectCache).removeObserver(me);
			endGSObjectCache = null;
		}
		if (endGSOnceObjectCache) {
			owner.getExternalObserverContainerForObject(endGSOnceObjectCache).removeObserver(me);
			endGSOnceObjectCache = null;
		}
		if (endPPObjectCache) {
			owner.getExternalObserverContainerForObject(endPPObjectCache).removeObserver(me);
			endPPObjectCache = null;
		}
		sections[0].removeObserver(me);
	}

	void offKN(bool OMO) {
		if (autoAction) {
			autoAction.cancel();
		}
		if ((state & R_PP) or OMO) {
			cancel();
		}
	}

	bool needRemake() {
		if (parentRoutes) {
			int i;
			for (i = 0; i < parentRoutes.size(); ++i) {
				if (parentRoutes[i].needRemake()) {
					return true;
				}
			}
		}

		return false;
	}

	void firstSectionUnlocked() {
		if (needRemake()) {
			state = state | R_PP;
			state = state & ~R_GS;
			touch();
		}
		else {
			cancel();
		}
	}

	public void handleEvent(kmObservable source, int type) {
		if (cast<kmJunctionInfo>source and (
			((kmJunctionInfo.EVENT_START_MOVING == type) and (state & R_GS))
			or ((kmJunctionInfo.EVENT_STOP_MOVING == type or kmJunctionInfo.EVENT_UNOCCUPY_TRACK == type) and !(state & R_GS))
		)) {
			touch();
		}
		if (cast<kmSection>source and kmSection.EVENT_UNLOCKED == type) {
			touch();
		}
		if (source == checkMNObject or source == endMNObjectCache or source == startObject or source == endGSObjectCache or source == endGSOnceObjectCache or source == endPPObjectCache) {
			touch();
		}
		if (source == sections[0] and kmSection.EVENT_UNLOCKED == type and !(state & R_PP)) {
			firstSectionUnlocked();
		}
		if (cast<kmRoute>source and parentRoutes) {
			int n;
			while (n < parentRoutes.size() and source != parentRoutes[n]) {
				++n;
			}
			if (n < parentRoutes.size()) {
				switch (type) {
					case EVENT_ON_KN:
						onKN();
						break;
					case EVENT_OFF_KN:
						offKN(false);
						break;
					case EVENT_OFF_KN_OMO:
						offKN(true);
						break;
					default:
						break;
				}
			}
		}
	}

	public void init(kmDscpPost post, int i) {
		if (!startObject) {
			Interface.Exception("post " + post.getStationName() + " (" + post.getStationCode() + "), route " + name + " startObject is null");
		}
		if (!sections.size()) {
			Interface.Exception("post " + post.getStationName() + " (" + post.getStationCode() + "), route " + name + " no sections");
		}
		inherited(post, i);
		state = R_NONE;
		if (parentRoutes) {
			int j;
			for (j = 0; j < parentRoutes.size(); ++j) {
				parentRoutes[j].addObserver(me);
			}
		}
	}

	public void saveProperties(Soup soup) {
		soup.SetNamedTag("routes.state." + index, state);
		if (endMNObjectCache) {
			soup.SetNamedTag("routes.endMNObjectCache." + index, endMNObjectCache.GetGameObjectID());
		}
		if (endGSObjectCache) {
			soup.SetNamedTag("routes.endGSObjectCache." + index, endGSObjectCache.GetGameObjectID());
		}
		if (endGSOnceObjectCache) {
			soup.SetNamedTag("routes.endGSOnceObjectCache." + index, endGSOnceObjectCache.GetGameObjectID());
		}
		if (endPPObjectCache) {
			soup.SetNamedTag("routes.endPPObjectCache." + index, endPPObjectCache.GetGameObjectID());
		}
		int i;
		soup.SetNamedTag("routes.openedCacheCount." + index, openedCache.size());
		for (i = 0; i < openedCache.size(); ++i) {
			soup.SetNamedTag("routes.openedCache." + index + "." + i, openedCache[i].GetGameObjectID());
		}
	}

	public void loadProperties(Soup soup) {
		state = soup.GetNamedTagAsInt("routes.state." + index, state);
		GameObjectID id = soup.GetNamedTagAsGameObjectID("routes.endMNObjectCache." + index);
		if (id) {
			endMNObjectCache = cast<kmRC>Router.GetGameObject(id);
			owner.getExternalObserverContainerForObject(endMNObjectCache).addObserver(me);
			owner.getExternalObserverContainerForObject(checkMNObject).addObserver(me);
		}
		else {
			endMNObjectCache = null;
		}
		id = soup.GetNamedTagAsGameObjectID("routes.endGSObjectCache." + index);
		if (id) {
			endGSObjectCache = cast<kmRC>Router.GetGameObject(id);
			owner.getExternalObserverContainerForObject(endGSObjectCache).addObserver(me);
		}
		else {
			endGSObjectCache = null;
		}
		id = soup.GetNamedTagAsGameObjectID("routes.endGSOnceObjectCache." + index);
		if (id) {
			endGSOnceObjectCache = cast<kmRC>Router.GetGameObject(id);
			owner.getExternalObserverContainerForObject(endGSOnceObjectCache).addObserver(me);
		}
		else {
			endGSOnceObjectCache = null;
		}
		id = soup.GetNamedTagAsGameObjectID("routes.endPPObjectCache." + index);
		if (id) {
			endPPObjectCache = cast<kmRC>Router.GetGameObject(id);
			owner.getExternalObserverContainerForObject(endPPObjectCache).addObserver(me);
		}
		else {
			endPPObjectCache = null;
		}
		int i, n;
		n = soup.GetNamedTagAsInt("routes.openedCacheCount." + index, 0);
		openedCache = new kmRC[n];
		for (i = 0; i < n; ++i) {
			id = soup.GetNamedTagAsGameObjectID("routes.openedCache." + index + "." + i);
			openedCache[i] = cast<kmRC>Router.GetGameObject(id);
		}

		if (state & R_KN) {
			for (i = 0; i < junctions.size(); ++i) {
				junctions[i].addObserver(me);
			}
			for (i = 0; i < conflictSections.size(); ++i) {
				conflictSections[i].addObserver(me);
			}
		}
		if (state & R_GS) {
			for (i = 0; i < sections.size(); ++i) {
				sections[i].route = me;
			}
			owner.getExternalObserverContainerForObject(startObject).addObserver(me);
			sections[0].addObserver(me);
		}
		if (startDir and (state & R_GS)) {
			startObject.addClearOnSwithcer(me);
		}
	}
};

class kmRouteAuto isclass kmRoute
{
	public define int R_AUTO	= 8;

	public bool isLockJunctions() {
		return !!(state & R_AUTO);
	}

	bool hasAuto() {
		return (state & R_AUTO) or inherited();
	}

	void offKN(bool OMO) {
		state = state & ~R_AUTO;
		inherited(OMO);
	}

	public void onKNa() {
		if (state & R_KN and checkJunctions()) {
			state = state | R_AUTO;
			int i;
			for (i = 0; i < sections.size(); ++i) {
				sections[i].route = me;
			}
			touch();
		}
	}

	public void offKNa() {
		state = state & ~R_AUTO;
	}

	bool needRemake() {
		return (state & R_AUTO) or inherited();
	}
};

class kmRouteAutoKN isclass kmRouteAbstract, kmObserver
{
	public kmRouteAuto route;

	public bool isKN() {
		return route.isLockJunctions();
	}

	public void onKN() {
		route.onKNa();
	}

	void offKN(bool OMO) {
		route.offKNa();
	}

	public void handleEvent(kmObservable source, int type) {
		if (source == route and (kmRoute.EVENT_OFF_KN == type or kmRoute.EVENT_OFF_KN_OMO == type)) {
			notifyObservers(type);
		}
	}

	public void init(kmDscpPost post, int i) {
		inherited(post, i);
		route.addObserver(me);
	}
};
