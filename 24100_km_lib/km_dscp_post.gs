include "km_auto_actions.gs"
include "km_welcome.gs"

class kmDscpPost isclass SceneryWithTrack, kmExternalObserver {
	string stationName;
	string stationCode;
	kmJunctionInfo[] junctions = new kmJunctionInfo[0];
	bool OSK = true;
	public define float OSK_TIMEOUT = 7.5;
	kmSection[] sections = new kmSection[0];
	kmRouteAbstract[] routes = new kmRoute[0];
	int GKNO = 0;// 1 - ГКНО, 2 - ГКНО+ОМО
	int OMOKS;
	kmWelcome[] welcomes = new kmWelcome[0];
	kmWelcome activeWelcome = null;
	bool objectRunningDriver=false;
	bool DisplayAtStart = true;
	Browser browser = null;
	string confirmCmd;
	Asset libAsset;
	Asset self;
	StringTable ST;
	kmLib mainLib;
	Soup mySoup;
	public bool debug;

	public string getStationName() {
		return stationName;
	}

	public string getStationCode() {
		return stationCode;
	}

	public kmJunctionInfo getJunction(int index) {
		if (index >= 0 and index < junctions.size()) {
			return junctions[index];
		}

		return null;
	}

	public kmJunctionInfo[] getJunctions() {
		return junctions;
	}

	string GetContentViewDetails(void) {
		int i;
		string s = "<font size=\"5\">" + stationName + " (" + stationCode + ")</font><br>";
		s = s + "Стрелки:<br><table><tr>";
		for (i = 0; i < junctions.size(); ++i) {
			s = s + "<td>" + junctions[i].getHtml() + "</td>";
		}
		s = s + "</tr></table>";
		s = s + "ОСК: " + OSK + "<br><br>";

		s = s + "Маршруты:<br>";
		for (i = 0; i < routes.size(); ++i) {
			s = s + routes[i].getHtml();
		}
		s = s + "<a href=\"live://property/gok\">ГОК</a>&nbsp;<font color=\"#";
		if (GKNO > 0 and mainLib.getBlinkerState()) {
			s = s + "ff0000";
		}
		else {
			s = s + "000000";
		}
		s = s + "\">*</font>&nbsp;&nbsp;<a href=\"live://property/omok\">ОМО</a>&nbsp;<font color=\"#";
		if (GKNO > 1) {
			s = s + "ff0000";
		}
		else {
			s = s + "000000";
		}
		s = s + "\">*</font>&nbsp;" + OMOKS + "<br><br>";

		s = s + "Пригласительные сигналы:<br>";
		for (i = 0; i < welcomes.size(); ++i) {
			s = s + welcomes[i].getHtml();
		}
		s = s + "<br>";

		s = s + "Размыкание маршрутов:<br>";
		for (i = 0; i < sections.size(); ++i) {
			s = s + sections[i].getHtml();
		}
		s = s + "<br>";

		s = s + "debug: <a href=\"live://property/debug\">" + debug + "</a>";

		return s;
	}

	public int getGKNO() {
		return GKNO;
	}

	public void resetGKNO() {
		GKNO = 0;
	}

	public void resetPOK() {
		if (activeWelcome) {
			activeWelcome.close();
			activeWelcome = null;
		}
		if (browser) {
			browser.LoadHTMLString(libAsset, GetContentViewDetails());
		}
	}

	void checkOSK() {
		bool newValue = true;
		int i = 0;
		for (i = 0; i < junctions.size(); ++i) {
			if (!junctions[i].isK()) {
				newValue = false;
				break;
			}
		}
		if (newValue) {
			OSK = true;
			ClearMessages("kmTimer", "osk");
			if (browser) {
				browser.LoadHTMLString(libAsset, GetContentViewDetails());
			}
		}
		else {
			PostMessage(me, "kmTimer", "osk", OSK_TIMEOUT);
		}
	}

	void JunctionToggleComplete(Message msg) {
		if (msg.src != me) return;
		int n = Str.ToInt(msg.minor);
		if (n < 0 or n >= junctions.size()) {
			return;
		}
		junctions[n].ToggleComplete();
		checkOSK();

		if (browser) {
			browser.LoadHTMLString(GetContentViewDetails());
		}
	}

	void onJunctionToggled(Message msg) {
		int n = Str.ToInt(msg.minor);
		if (n < 0 or n >= junctions.size()) {
			return;
		}
		junctions[n].Toggled(msg.src);
		if (browser) {
			browser.LoadHTMLString(GetContentViewDetails());
		}
	}

	void onSectionKpTimer(Message msg) {
		int n = Str.ToInt(msg.minor);
		if (n < 0 or n >= sections.size()) {
			return;
		}
		kmSectionWithKP section = cast<kmSectionWithKP>sections[n];
		if (!section) {
			return;
		}
		section.onKpTimer();
		if (browser) {
			browser.LoadHTMLString(GetContentViewDetails());
		}
	}

	void onWelcomeAutoTimer(Message msg) {
		int n = Str.ToInt(msg.minor);
		if (n < 0 or n >= welcomes.size()) {
			return;
		}
		kmWelcomeAuto welcome = cast<kmWelcomeAuto>welcomes[n];
		if (!welcome) {
			return;
		}
		welcome.onAutoTimer();
		if (browser) {
			browser.LoadHTMLString(libAsset, GetContentViewDetails());
		}
	}

	void onTimer(Message msg) {
		if (msg.src != me) return;
		if ("osk" == msg.minor) {
			OSK = false;
			if (browser) {
				browser.LoadHTMLString(libAsset, GetContentViewDetails());
			}
		}
	}

	public void ChangeText(Message msg){
		if (browser and msg.src == browser) {
			if (msg.minor[,17] == "live://property/j") {
				int n = Str.Find(msg.minor, "/", 17);
				if (n < 0) {
					return;
				}
				string cmd = msg.minor[n+1,];
				n = Str.ToInt(msg.minor[17,n]);
				if (n < 0 or n >= junctions.size()) {
					return;
				}
				if (cmd == "pu") {
					junctions[n].PU(mainLib.tryClaimPOK(me, 4));
				}
				else if (cmd == "mu") {
					junctions[n].MU(mainLib.tryClaimPOK(me, 4));
				}
			}
			else if (msg.minor[,17] == "live://property/s") {
				int n = Str.Find(msg.minor, "/", 17);
				if (n < 0) {
					return;
				}
				string cmd = msg.minor[n+1,];
				n = Str.ToInt(msg.minor[17,n]);
				if (n < 0 or n >= sections.size()) {
					return;
				}
				sections[n].guiAction(cmd);
			}
			else if (msg.minor[,17] == "live://property/r") {
				int n = Str.Find(msg.minor, "/", 17);
				if (n < 0) {
					return;
				}
				string cmd = msg.minor[n+1,];
				n = Str.ToInt(msg.minor[17,n]);
				if (n < 0 or n >= routes.size()) {
					return;
				}
				routes[n].guiAction(cmd);
			}
			else if (msg.minor == "live://property/gok") {
				if (GKNO) {
					GKNO = 0;
				}
				else {
					GKNO = 1;
				}
			}
			else if (msg.minor == "live://property/omok") {
				OMOKS++;
				if (OMOKS >= 10000) {
					OMOKS = 0;
				}
				mainLib.updateMapData(me);
				if (GKNO) {
					GKNO = 2;
				}
			}
			else if (msg.minor[,17] == "live://property/w") {
				int n = Str.Find(msg.minor, "/", 17);
				if (n < 0) {
					return;
				}
				string cmd = msg.minor[n+1,];
				n = Str.ToInt(msg.minor[17,n]);
				if (n < 0 or n >= welcomes.size()) {
					return;
				}
				if ("open" == cmd) {
					if (!activeWelcome and mainLib.tryClaimPOK(me, 1)) {
						if (welcomes[n].tryOpen()) {
							activeWelcome = welcomes[n];
						}
					}
				}
			}
			else if (msg.minor == "live://property/debug") {
				debug = !debug;
			}
			browser.LoadHTMLString(GetAsset(), GetContentViewDetails());
		}
	}

	void BrowserClosed(Message msg) {
		if (msg.src == browser) {
			browser = null;
			AddHandler(mainLib, "kmSigBlinker", null, null);
		}
	}

	void SignalChanged(Message msg) {
		if (browser) {
			browser.LoadHTMLString(GetAsset(), GetContentViewDetails());
		}
	}

	void loadObjects() {}

	void initObjects() {
		loadObjects();

		int i;
		for (i = 0; i < junctions.size(); ++i) {
			junctions[i].init(me, i);
		}

		for (i = 0; i < sections.size(); ++i) {
			sections[i].init(me, i);
		}

		for (i = 0; i < routes.size(); ++i) {
			routes[i].init(me, i);
		}

		for (i = 0; i < welcomes.size(); ++i) {
			welcomes[i].init(me, i);
		}
	}

	void onBlinker(Message msg) {
		if (browser) {
			browser.LoadHTMLString(libAsset, GetContentViewDetails());
		}
	}

	void InitMainBrowser(void) {
		int width = Interface.GetDisplayWidth();
		browser = Constructors.NewBrowser();
		browser.SetWindowRect(10, 75, 10 + 200, 75 + 300);
		browser.SetWindowStyle(Browser.STYLE_DEFAULT);
		browser.SetScrollEnabled(true);
		browser.SetCloseEnabled(true);
		browser.SetWindowTitle(stationName + " - dscp");
//		browser.SetButtonOverlayStyle(Browser.BS_OK, Browser.BS_None);
		browser.SetRememberPosition(self, "main-browser");
		browser.SetWindowGrow(200, 400, Interface.GetDisplayWidth(), Interface.GetDisplayHeight());
		AddHandler(mainLib, "kmSigBlinker", null, "onBlinker");
	}

	void GUIRequestMsgHdl(Message msg) {
		if (browser){
			if (browser.IsWindowMinimised()) {
				browser.RestoreWindow();
			}
		}
		else {
			InitMainBrowser();
		}
		browser.LoadHTMLString(GetAsset(), GetContentViewDetails());
		browser.SetWindowVisible(true);
	}

	public void RunInDriver() {
	// NOTE! See part of "World.GetCurrentModule()" 
		if (World.GetCurrentModule() != World.DRIVER_MODULE){
			return;
		}
		initObjects();
		int i;
		for (i=0; i < junctions.size(); ++i) {
			junctions[i].loadProperties(mySoup);
		}
		checkOSK();
		for (i = 0; i < sections.size(); ++i) {
			sections[i].loadProperties(mySoup);
		}
		for (i = 0; i < routes.size(); ++i) {
			routes[i].loadProperties(mySoup);
		}
		mainLib.registerPost(me);
		AddHandler(me, "Browser-URL", "", "ChangeText");
		AddHandler(me, "Browser-Closed", "", "BrowserClosed");

		if (DisplayAtStart){
			if (!browser)
				InitMainBrowser();
			browser.SetWindowVisible(true);
			browser.LoadHTMLString(libAsset, GetContentViewDetails());
		}
	}

	void ModuleInitHandler(Message msg) {
		if (objectRunningDriver) {
			return;
		}

		if (World.GetCurrentModule() == World.DRIVER_MODULE) {
			objectRunningDriver = true;
			RunInDriver();
		}
	}

	public string GetDescriptionHTML(void) {
		return HTMLWindow.CheckBox("live://property/DisplayAtStart", DisplayAtStart)+"<a href='live://property/DisplayAtStart'>"+ST.GetString("DisplayAtStart")+"</a>";
	}

	string GetPropertyType(string id) {
		if (id == "DisplayAtStart") return "link";
		return inherited(id);
	}

	void LinkPropertyValue(string id) {
		if (id == "DisplayAtStart")DisplayAtStart=!DisplayAtStart;
		else inherited(id);
	}

	public Soup getMapData() {
		Soup soup = Constructors.NewSoup();
		soup.SetNamedTag("omoks", OMOKS);
		return soup;
	}

	public void setMapData(Soup soup) {
		OMOKS = soup.GetNamedTagAsInt("omoks", OMOKS);
	}

	public Soup GetProperties(void) {
		mySoup = inherited();
		mySoup.SetNamedTag("DisplayAtStart", DisplayAtStart);
		int i;
		for (i = 0; i < junctions.size(); ++i) {
			junctions[i].saveProperties(mySoup);
		}
		for (i = 0; i < sections.size(); ++i) {
			sections[i].saveProperties(mySoup);
		}
		for (i = 0; i < routes.size(); ++i) {
			routes[i].saveProperties(mySoup);
		}
		return mySoup;
	}

	public void SetProperties(Soup soup) {
//		initObjects();
		inherited(soup);
		DisplayAtStart = soup.GetNamedTagAsBool("DisplayAtStart", true);
		mySoup = soup;
	}

	public kmLib getMainLib() {
		return mainLib;
	}

	void notifySelfAboutExternalEvent() {
		PostMessage(me, "kmExternalEvent", "process", 0);
	}

	void onProcessExternalEvents(Message msg) {
		processExternalEvents();
		if (browser) {
			browser.LoadHTMLString(libAsset, GetContentViewDetails());
		}
	}

	public void Init(Asset asset){
		inherited(self = asset);
		DisplayAtStart = true;
		ST = self.GetStringTable();
		libAsset = self.FindAsset("kmLib");
		if (!libAsset) {
			Interface.Exception("unable load kmLib");
		}
//		ST = libAsset.GetStringTable();
		mainLib = cast<kmLib>World.GetLibrary(libAsset.GetKUID());
		if (!mainLib) {
			Interface.Exception("unable load kmLib");
		}
		AddHandler(me, "World", "ModuleInit", "ModuleInitHandler");
		AddHandler(me, "km_dscp", "RequestGUI", "GUIRequestMsgHdl");
//		initObjects();
		AddHandler(me, "Signal", "StateChanged", "SignalChanged");
		AddHandler(me, "kmJunctionToggleComplete", "", "JunctionToggleComplete");
		AddHandler(me, "kmJunctionToggled", null, "onJunctionToggled");
		AddHandler(me, "kmSectionKpTimer", null, "onSectionKpTimer");
		AddHandler(me, "kmWelcomeAutoTimer", null, "onWelcomeAutoTimer");
		AddHandler(me, "kmTimer", "", "onTimer");
		AddHandler(me, "kmExternalEvent", "process", "onProcessExternalEvents");
//		AddHandler(me, "kmArrTimer", "", "ArrEvent");
	}
};
