include "meshobject.gs"

class km_isojoint isclass MeshObject{
	string[] GetChars(string s){
		string[] ret= new string[0];
//		ret[0]="";
		int i=0;
		int k=-1;
		bool f=false;

		while(i<s.size()){
			if(!f){
				k++;
				ret[k,k+1]=new string[1];
				ret[k]="";
			}

			string s2=s[i,i+2];
			if(s2>="А"){			// кириллица, кодировка UTF-8, отображение как CP1251
				ret[k]=ret[k]+s[i,i+2];
				i++;
			}
			else{
				s2=s2[0,1];

				if(s2=="(")f=true;
				else if(s2==")")f=false;
				else ret[k]=ret[k]+s[i,i+1];
			}
			i++;
		}
		return ret;
	}

	public void SetNameR(string value){}
	public void SetNameL(string value){}
};