include "km_observer.gs"

class kmObservable
{
    kmObserver[] observers = new kmObserver[0];

    public kmObserver[] getObservers() {
        return observers;
    }

    public void addObserver(kmObserver observer) {
        int i;
        for (i = 0; i < observers.size(); ++i) {
            if (observer == observers[i]) {
                return;
            }
        }
        observers[observers.size()] = observer;
    }

    public void removeObserver(kmObserver observer) {
        int i = 0;
        while (i < observers.size()) {
            if (observer == observers[i]) {
                observers[i, i+1] = null;
            }
            else {
                ++i;
            }
        } 
    }

    void notifyObservers(int type) {
        int i;
        kmObserver[] tmp;
        tmp.copy(observers);
        for (i = 0; i < tmp.size(); ++i) {
            tmp[i].handleEvent(me, type);
        }
    }
};

class kmExternalObserverContainer isclass kmObserver, kmObservable
{
    public kmObservable observable;
    public kmExternalObserver owner;

    public void handleEvent(kmObservable source, int type) {
        kmExternalEvent event = new kmExternalEvent();
        event.source = source;
        event.container = me;
        event.type = type;
        owner.addExternalEvent(event);
    }

    public void addObserver(kmObserver observer) {
        if (!observers.size()) {
            observable.addObserver(me);
        }
        inherited(observer);
    }

    public void removeObserver(kmObserver observer) {
        inherited(observer);
        if (!observers.size()) {
            observable.removeObserver(me);
        }
    }
};
