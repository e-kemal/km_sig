@echo off
call make.bat encrypt
rem pause
rename km_double_signal.gs km_double_signal.gs.tmp
rename km_lib.gs km_lib.gs.tmp
rename km_rc.gs km_rc.gs.tmp
rename km_signal.gs km_signal.gs.tmp
rename km_route_utiltes.gs km_route_utiltes.gs.tmp
call make.bat install
call make.bat commit
call make.bat status
rename km_double_signal.gs.tmp km_double_signal.gs
rename km_lib.gs.tmp km_lib.gs
rename km_rc.gs.tmp km_rc.gs
rename km_signal.gs.tmp km_signal.gs
rename km_route_utiltes.gs.tmp km_route_utiltes.gs
del *.gse