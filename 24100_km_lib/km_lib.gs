include "Library.gs"
include "BinarySortedArray.gs"
include "km_signal.gs"
include "km_autostop.gs"
include "km_vehicle.gs"
include "km_dscp_post.gs"

class LocomotiveContainer/* isclass GSObject*/{
	public bool IsStopped;		// стоящий
	public /*Signal*/object frontRC;
	public /*Signal*/object backRC;
	public kmAutoStop AS;
	public bool frontNear;
	public bool backNear;
	public bool ASnear;
	public int seq;
//	public kmRC[] signal;		// внутренний идентификатор светофора
//	public int[] state;  		// 0 - подошедший к светофору, 1 - проехавший головой светофор, 2 - заехавший за светофор
};

class kmLib isclass Library{
	Asset self;
//	bool inited=false;
//	TrainContainer[] train_arr;
	BinarySortedArray train_arr;
	bool MainThreadStarted=false;
	bool blinkerThreadStarted = false;
	bool blinkerState = false;
	int last_edited_type=kmRC.S_TYPE_AB;
	Asset isoAsset=null;
	kmDscpPost[] posts = new kmDscpPost[0];
	kmJunctionInfo[] junctions = new kmJunctionInfo[0];
	Soup mapData = null;
	OnlineAccess onlineAccess;
	int pokButton = 0;
	kmDscpPost pokClaimedPost;
	Browser pokBrowser;
	define float HALF_A_CAR_LENGTH = 20.f;
	define float FIRST_AXLE_OFFSET = 0.4f;

	public bool checkOverlap(GSTrackSearch ts) {
		float baseDistance = ts.GetDistance();
		while (ts.SearchNextObject() and (ts.GetDistance() - baseDistance < HALF_A_CAR_LENGTH)) {
			Vehicle v = cast<Vehicle>ts.GetObject();
			if (!v) {
				continue;
			}
			return v.GetLength() * FIRST_AXLE_OFFSET < ts.GetDistance() - baseDistance;
		}

		return true;
	}

	/**
	  * Функция идёт в заданном направлении к следующему объекту и получает от него данные состояния
	  */
	StateInfo getSignalDataFromNextObject(kmRC sig, bool dir) {
		StateInfo signalData;
		kmRC nextSig = null;
		bool defaultFounded = false;
		GSTrackSearch ts = sig.BeginTrackSearch(dir);
		while (ts.SearchNextObject()) {
			object obj = ts.GetObject();
			if (cast<Vehicle>obj)
				break;
			if (!ts.GetFacingRelativeToSearchDirection())
				continue;
			if (nextSig = cast<kmRC>obj)
				break;
			if (cast<Signal>obj) {
				defaultFounded = true;
				break;//от дефолта мы ничего не получим
			}
		}
		if (!checkOverlap(ts)) {
			nextSig = null;
		}
		if (nextSig)
			signalData = nextSig.GetState();
		else {
			signalData = new StateInfo();
			if (defaultFounded)//Если нашёлся дефолтный светофор, то участок до него считаем одной свободной РЦ. Это позволит светофору открыться на КЖ
				signalData.RCcount = 1;//количество свободных РЦ
			else
				signalData.RCcount = 0;//количество свободных РЦ
			signalData.BUcount = 0;//количество свободных БУ
			signalData.ARS1 = 0;//"частота" АЛС-АРС
			signalData.ARS2 = 0;//предупредительная "частота"
		}
		return signalData;
	}

	void continueUpdate(GSTrackSearch ts, StateInfo signalData, object mandatoryObject) {
		bool overlaped = false;
		while (ts.SearchNextObject()) {
			object obj = ts.GetObject();
			if (mandatoryObject and (mandatoryObject == obj)) {
				mandatoryObject = null;
			}
			if (cast<Vehicle>obj) {
				if (cast<kmVehicle>obj) {
					(cast<kmVehicle>obj).kmSetCode(!ts.GetFacingRelativeToSearchDirection(), signalData.ARS1, signalData.ARS2);
				}
//Interface.Print("veh: "+(cast<Vehicle>obj).GetName()+" ARS: "+SignalData.ARS1+"/"+SignalData.ARS2);
				if (ts.GetFacingRelativeToSearchDirection() != (cast<Vehicle>obj).GetDirectionRelativeToTrain() and signalData.ARS1 >= kmRC.ARS_MIN_SPEED) {
					(cast<Vehicle>obj).GetMyTrain().SetAdvisoryLimit(signalData.ARS1 / 3.6);
				}
				if (mandatoryObject) {
					signalData.RCcount = 0;
					signalData.BUcount = 0;
					signalData.ARS1 = kmRC.ARS_OCh;
					signalData.ARS2 = kmRC.ARS_OCh;
					continue;
				}
				break;
			}
			if (ts.GetFacingRelativeToSearchDirection()) {
				continue;
			}
			if (overlaped or (overlaped = !checkOverlap(ts.CloneSearch()))) {
				continue;
			}
			kmRC sig = cast<kmRC>obj;
			if (!sig) {
				if (cast<Signal>obj) {
					break;
				}
				continue;
			}
			if (!sig.UpdateState(signalData) and !mandatoryObject) {
				break;
			}
			if (ts.GetDistance() > 3000) {
				ts = sig.BeginTrackSearch(ts.GetFacingRelativeToSearchDirection());
			}
		}
	}

	void CloseAndUpdate(Signal sig){//вызывается для закрытой РЦ, чтобы она обновила РЦ перед собой
		StateInfo signalData = new StateInfo();
		signalData.RCcount = 0;
		signalData.BUcount = 0;
		signalData.ARS1 = kmRC.ARS_OCh;
		signalData.ARS2 = kmRC.ARS_OCh;
		kmRC rc;
		if (rc = cast<kmRC>sig) {
			rc.UpdateState(signalData);
		}
		else {
			++signalData.RCcount;
		}
		GSTrackSearch ts = sig.BeginTrackSearch(false);
		continueUpdate(ts, signalData, null);
	}

	public void Update(kmRC sig, bool dir, object mandatoryObject){//вызывается маршрутизацией для обновления объектов и, возможно, обновления предыдущих
		StateInfo signalData = getSignalDataFromNextObject(sig, dir);
		GSTrackSearch ts = sig.BeginTrackSearch(!dir);

		if (dir) {
//Interface.Print("update for: " + sig.GetPropertyValue("name") + " (" + sig.GetPropertyValue("nameRC") + ")");
			sig.UpdateState(signalData);
		}
		if (mandatoryObject == sig) {
			mandatoryObject = null;
		}
		continueUpdate(ts, signalData, mandatoryObject);
	}

	void TrainStarting(Message msg){
		Train curr_train=msg.src;

		if(!curr_train){  // поезд потерян
			Interface.Exception("A train contains a bad vehicle!");
			return;
		}
		int id, nmb;
		Vehicle front, back;
		front=curr_train.GetVehicles()[0];
		back=curr_train.GetVehicles()[curr_train.GetVehicles().size()-1];

		id=front.GetId()<<1;
		if(!front.GetDirectionRelativeToTrain())id++;
		nmb=train_arr.Find(id);
		if(nmb>=0)
			(cast<LocomotiveContainer>(train_arr.DBSE[nmb].value)).IsStopped=false;

		id=back.GetId()<<1;
		if(back.GetDirectionRelativeToTrain())id++;
		nmb=train_arr.Find(id);
		if(nmb>=0)
			(cast<LocomotiveContainer>(train_arr.DBSE[nmb].value)).IsStopped=false;
	}

	void TrainStopping(Message msg){
		Train curr_train=msg.src;

		if(!curr_train){  // поезд потерян
			Interface.Exception("A train contains a bad vehicle!");
			return;
		}
		int id, nmb;
		Vehicle front, back;
		front=curr_train.GetVehicles()[0];
		back=curr_train.GetVehicles()[curr_train.GetVehicles().size()-1];

		id=front.GetId()<<1;
		if(!front.GetDirectionRelativeToTrain())id++;
		nmb=train_arr.Find(id);
		if(nmb>=0)
			(cast<LocomotiveContainer>(train_arr.DBSE[nmb].value)).IsStopped=true;

		id=back.GetId()<<1;
		if(back.GetDirectionRelativeToTrain())id++;
		nmb=train_arr.Find(id);
		if(nmb>=0)
			(cast<LocomotiveContainer>(train_arr.DBSE[nmb].value)).IsStopped=true;
	}

	void RemoveTrain(Message msg){
		Train curr_train=msg.src;

		if(!curr_train){  // поезд потерян
			Interface.Exception("A train contains a bad vehicle!");
			return;
		}
		Vehicle[] vehicles=curr_train.GetVehicles();
		if(vehicles.size()<1)return;
		int id, nmb;
		Vehicle front, back;
		front=vehicles[0];
		back=vehicles[vehicles.size()-1];
		kmRC sig;

		id=front.GetId()<<1;
		if(!front.GetDirectionRelativeToTrain())id++;
		nmb=train_arr.Find(id);
		if(nmb>=0){
			if(sig=cast<kmRC>(cast<LocomotiveContainer>(train_arr.DBSE[nmb].value)).backRC)
				Update(sig, true, null);
			train_arr.DeleteElementByNmb(nmb);
		}

		id=back.GetId()<<1;
		if(back.GetDirectionRelativeToTrain())id++;
		nmb=train_arr.Find(id);
		if(nmb>=0){
			if(sig=cast<kmRC>(cast<LocomotiveContainer>(train_arr.DBSE[nmb].value)).backRC)
				Update(sig, true, null);
			train_arr.DeleteElementByNmb(nmb);
		}
	}

	void CheckLocomotive(Vehicle veh, bool dir, int seq){//вагон и направление, куда от него искать. вызывается так, чтобы поиск ушёл наружу состава
		LocomotiveContainer LC;
		int id=veh.GetId()<<1;
		kmVehicle kmveh=cast<kmVehicle>(cast<object>veh);
		if(!dir)id++;
		int nmb=train_arr.Find(id);
		if(nmb<0){
			LC = new LocomotiveContainer();

			if (!train_arr.AddElement(id, LC)) {
				Interface.Exception("Can't add train "+id);
				return;
			}
			LC.IsStopped=/*veh.GetVelocity()==0;*/veh.GetMyTrain().IsStopped();
			LC.frontRC=LC.backRC=/*null;*/me;//попробуем такой финт ушами. При первом просчёте скрипт должен отработать полностью, даже если поиск ничего не найдёт, поэтому мы не можем использовать null
			LC.AS=null;
			LC.frontNear=LC.backNear=LC.ASnear=true;//поставим флаги, что объекты близко. Это заставит выполнить полный поиск
			Sniff(veh.GetMyTrain(), "Train", "StartedMoving", true);
			Sniff(veh.GetMyTrain(), "Train", "StoppedMoving", true);
			Sniff(veh.GetMyTrain(), "Train", "Cleanup", true);
		}
		else{
			LC=cast<LocomotiveContainer>(train_arr.DBSE[nmb].value);
			if(LC.IsStopped){
				LC.seq=seq;
				return;
			}
		}
		/*Signal*/object newFrontRC=null;
		/*Signal*/object newBackRC=null;
		kmAutoStop newAS=null;
		float distance=Math.Fmax(veh.GetVelocity(), 50.0);
		float frontDistance=0.0, backDistance=0.0, ASdistance=0.0, lastDistance = 0.0;
		GSTrackSearch ts=veh.BeginTrackSearch(dir);
		while (
			ts.SearchNextObject()
			and (!(newFrontRC and newBackRC and newAS) or lastDistance < ts.GetDistance() + HALF_A_CAR_LENGTH)
			and (ts.GetDistance()<distance or (LC.frontNear and !newFrontRC) or (LC.backNear and !newBackRC) or (LC.ASnear and !newAS))
		) {//ищем, пока можем. Прекращаем поиск, если нашли оба объекта. Или если ушли далеко, но при этом в предыдущий раз они не были найдены близко.
			if (ts.GetDistance() < FIRST_AXLE_OFFSET * veh.GetLength()) {
				continue;//сначала нам будут попадаться объекты, находящиеся под нашим вагоном. По идее, их надо пропускать. Но это может привести к преждевременному перекрытию светофора.
			}
			object obj=ts.GetObject();
			if(!obj)continue;
			if(cast<Vehicle>obj){
				float foundedDistance = ts.GetDistance() - FIRST_AXLE_OFFSET * (cast<Vehicle>obj).GetLength();
				if (newFrontRC and frontDistance > foundedDistance) {
					newFrontRC = null;//найденый объект оказывается под вагоном. А значит мы его не нашли.
				}
				if (newBackRC and backDistance > foundedDistance) {
					newBackRC = null;
				}
				if (newAS and ASdistance > foundedDistance) {
					newAS = null;
				}
				break;
			}
			if(ts.GetFacingRelativeToSearchDirection()){
				if(!newFrontRC){//continue;//нашли уже
					if(cast<kmRC>obj or cast<Signal>obj){
						newFrontRC=obj;
						frontDistance = lastDistance = ts.GetDistance();
					}
				}
				if(!newAS){//continue;//нашли уже
					if(cast<kmAutoStop>obj){
						newAS=obj;
						ASdistance = lastDistance = ts.GetDistance();
					}
				}
			}
			else{
				if(newBackRC)continue;//нашли уже
				if(cast<kmRC>obj or cast<Signal>obj){
					newBackRC=obj;
					backDistance = lastDistance = ts.GetDistance();
				}
			}
		}

		kmRC sig;
		StateInfo SignalData;
		if(LC.frontRC!=newFrontRC and !(!newFrontRC and !LC.frontNear)){//Если объект изменился, то нужно принимать меры. Но если объект не найден, а в прошлый раз он был далеко, то меры принимать не нужно.
			if((veh.GetVelocity()>0)==dir){//Если наехали на объект
//Interface.Print("veh: "+veh.GetName()+" front enter");
				if(sig=cast<kmRC>LC.frontRC){
					sig.enterForward();
				}
				if(sig=cast<kmRC>newFrontRC)SignalData=sig.GetState();//А от нового возьмём данные для отправки в состав
				else{//В случае дефолта будет ОЧ
					SignalData=new StateInfo();
					SignalData.RCcount=1;//количество свободных РЦ
					SignalData.BUcount=0;//количество свободных БУ
					SignalData.ARS1=0;//"частота" АЛС-АРС
					SignalData.ARS2=0;//предупредительная "частота"
				}
				if(newFrontRC)
					PostMessage(newFrontRC, "kmSigEvent", "Approach", 0.01);//TODO: если skipRC, то объект должен сам переслать дальше
				//veh.SetCode(SignalData);
				if(kmveh)
					kmveh.kmSetCode(dir, SignalData.ARS1, SignalData.ARS2);
//Interface.Print("veh: "+veh.GetName()+" ARS: "+SignalData.ARS1+"/"+SignalData.ARS2);
if(dir==veh.GetDirectionRelativeToTrain() and SignalData.ARS1>4)veh.GetMyTrain().SetAdvisoryLimit(SignalData.ARS1/3.6);
			}
			else{//Съехали с объекта
//Interface.Print("veh: "+veh.GetName()+" front leave");
				if(sig=cast<kmRC>newFrontRC) {
					Update(sig, true, null);//вызову полное обновление, пусть сам данные в состав передаст
					sig.leaveBackward();
				}
				else{
					SignalData=new StateInfo();
					SignalData.RCcount=1;//количество свободных РЦ
					SignalData.BUcount=0;//количество свободных БУ
					SignalData.ARS1=0;//"частота" АЛС-АРС
					SignalData.ARS2=0;//предупредительная "частота"
					//veh.SetCode(SignalData);
					if(kmveh)
						kmveh.kmSetCode(dir, SignalData.ARS1, SignalData.ARS2);
//Interface.Print("veh: "+veh.GetName()+" ARS: "+SignalData.ARS1+"/"+SignalData.ARS2);
if(dir==veh.GetDirectionRelativeToTrain() and SignalData.ARS1>4)veh.GetMyTrain().SetAdvisoryLimit(SignalData.ARS1/3.6);
				}
			}
			LC.frontRC=newFrontRC;
		}
		LC.frontNear=(newFrontRC and (frontDistance<distance));

		if(LC.backRC!=newBackRC and !(!newBackRC and !LC.backNear)){//Если объект изменился, то нужно принимать меры. Но если объект не найден, а в прошлый раз он был далеко, то меры принимать не нужно.
			if (cast<Signal>newBackRC) {		//В любом случае новый найденный объект теперь ограждает занярую секцию
				CloseAndUpdate(cast<Signal>newBackRC);
			}
			if((veh.GetVelocity()>0)==dir){//Если наехали на объект
				if (sig = cast<kmRC>LC.backRC) {
					sig.enterBackward();
				}
			}
			else{//Съехали с объекта
				if(sig = cast<kmRC>newBackRC) {
					sig.leaveForward();
				}
			}
			LC.backRC=newBackRC;
		}
		LC.backNear=(newBackRC and (backDistance<distance));

		if(LC.AS!=newAS and !(!newAS and !LC.ASnear)){//Если объект изменился, то нужно принимать меры. Но если объект не найден, а в прошлый раз он был далеко, то меры принимать не нужно.
			if((veh.GetVelocity()>0)==dir){//Если наехали на объект
//Interface.Print("veh: "+veh.GetName()+" front enter");
				if(LC.AS and LC.AS.CheckSpeed(Math.Fabs(veh.GetVelocity()))){
					if(dir)
						PostMessage(veh, "kmAutoStop", "triggered", 0.01);
					else
						PostMessage(veh, "kmAutoStop", "triggered_back", 0.01);
				}
			}
			else{//Съехали с объекта
//Interface.Print("veh: "+veh.GetName()+" front leave");
				if(newAS and newAS.CheckSpeed(-Math.Fabs(veh.GetVelocity()))){
					if(dir)
						PostMessage(veh, "kmAutoStop", "triggered", 0.01);
					else
						PostMessage(veh, "kmAutoStop", "triggered_back", 0.01);
				}
			}
			LC.AS=newAS;
		}
		LC.ASnear=(newAS and (ASdistance<distance));
		LC.seq=seq;
	}

	thread void CheckTrainList(){			// проверка поездов, подъезжающих к светофорам
		if(MainThreadStarted)return;//пусть будет...
		MainThreadStarted=true;
		int i;
		int seq=0;
//		LocomotiveContainer LC;
		while(1){
			Train[] newTrains=World.GetTrainList();
			for(i=0;i<newTrains.size();i++){
				if(!newTrains[i]){  // поезд потерян
					Interface.Exception("A train contains a bad vehicle!");
//					return;
					continue;
				}
				Train curr_train=newTrains[i];
				Vehicle front, back;
				Vehicle[] vehicles=curr_train.GetVehicles();
				if(!vehicles or vehicles.size()<1)continue;
//				front=curr_train.GetVehicles()[0];
//				back=curr_train.GetVehicles()[curr_train.GetVehicles().size()-1];
				front=vehicles[0];
				back=vehicles[vehicles.size()-1];
				CheckLocomotive(front, front.GetDirectionRelativeToTrain(), seq);
				CheckLocomotive(back, !back.GetDirectionRelativeToTrain(), seq);
			}
			Sleep(0.5);
			seq++;
			if(seq%100==0){
				for(i=0;i<train_arr.N;i++){
					if((cast<LocomotiveContainer>(train_arr.DBSE[i].value)).seq<seq-50){
//						Vehicle veh=cast<Vehicle>Router.GetGameObject(train_arr.DBSE[i].key>>1);
//if(veh)Interface.Print("del: "+veh.GetName());
						train_arr.DeleteElementByNmb(i);
					}
				}
			}
			if(seq>1000)seq=0;
		}
		MainThreadStarted=false;
	}

	thread void blinker() {
		if (blinkerThreadStarted)
			return;	//пусть будет...
		blinkerThreadStarted = true;
		while (true) {
			PostMessage(me, "kmSigBlinker", blinkerState = !blinkerState, 0.001);
			Sleep(0.5);
		}
		blinkerThreadStarted = false;
	}

	public void OnClickSystemButton(Message msg) {
		int i, n;
		n = posts.size();
		if (n) {
			Menu iconMenu = Constructors.NewMenu();
			for (i=0; i<n; i++)
				iconMenu.AddItem(posts[i].getStationName(), posts[i], "km_dscp", "RequestGUI");
			ShowSystemMenuIconMenu(iconMenu);
		}
	}

	void UpdateSystemIcon(void) {
		if (Interface.GetDeviceFormFactor() == Interface.FormFactor_Phone) {
			return;
		}

		if (World.GetCurrentModule() == World.DRIVER_MODULE and posts.size() > 0)
			AddSystemMenuIcon(self.FindAsset("usericonmenu"), "posts", "help");
		else
			RemoveSystemMenuIcon();
	}

	void loadMapData() {
		Asset profile = World.GetCurrentProfile();
		mapData = Constructors.NewSoup();
		if (!profile) {
			return;
		}
		Asset map = profile.GetParent();
		if (!map) {
			return;
		}
		KUID kuid = map.GetKUID().GetBaseKUID();
//		GetOnlineAccess().Connect();
		onlineAccess.GetLocalData(kuid.GetLogString() + "-km-signals", mapData);
	}

	void saveMapData() {
	//ToDo: не сохранять, если это клиент МП
		if (!mapData) {
			return;
		}
		Asset profile = World.GetCurrentProfile();
		if (!profile) {
			return;
		}
		Asset map = profile.GetParent();
		if (!map) {
			return;
		}
		KUID kuid = map.GetKUID().GetBaseKUID();
		onlineAccess.SetLocalData(kuid.GetLogString() + "-km-signals", mapData);
	}

	public int getLastEditedType() {
		return last_edited_type;
	}

	public void setLastEditedType(int type) {
		last_edited_type = type;
	}

	public Asset getIsojointEdited() {
		return isoAsset;
	}

	public void setIsojointEdited(Asset asset) {
		isoAsset = asset;
	}

	public string LibraryCall(string function, string[] stringParam, GSObject[] objectParam){
		if(function=="ForceUpdate"){
			kmRC sign=null;
			sign=cast<kmRC>objectParam[0];
			object sign2 = null;
			if (objectParam.size() > 1) {
				sign2 = objectParam[1];
			}
			if (sign) {
				Update(sign, true, sign2);
				return "OK";
			}
			return "err";//Не знаю зачем. Но пусть будет.
		}
		return inherited(function, stringParam, objectParam);
	}

	public bool getBlinkerState() {
		return blinkerState;
	}

	public void registerPost(kmDscpPost post) {
		int i;
		for (i = 0; i < posts.size(); ++i) {
			if (posts[i] == post) {
				return;
			}
		}
		posts[posts.size()] = post;
		UpdateSystemIcon();
		if (!mapData) {
			loadMapData();
		}
		if ("" != post.getStationCode()) {
			post.setMapData(mapData.GetNamedSoup("station-" + post.getStationCode()));
		}
		kmJunctionInfo[] jncs = post.getJunctions();
		for (i = 0; i < jncs.size(); ++i) {
			junctions[junctions.size()] = jncs[i];
		}
	}

	public void updateMapData(kmDscpPost post) {
		if (!mapData) {
			return;
		}
		if ("" == post.getStationCode()) {
			return;
		}
		mapData.SetNamedSoup("station-" + post.getStationCode(), post.getMapData());
		saveMapData();
	}

	void SyncHandler(Message msg){
		Soup soup = cast<Soup>msg.paramSoup;
		if(!soup)return;
		string function=soup.GetNamedTag("function");
		Str.ToLower(function);
		Interface.Print("SyncHandler");
		soup.Log();
		GameObjectID id = soup.GetNamedTagAsGameObjectID("id");
		if (!id) {
			return;
		}
		kmRC sign = cast<kmRC>Router.GetGameObject(id);
		if (!sign) {
			Interface.Print("sign not found");
			return;
		}

		if(function=="forceupdate"){
			Update(sign, true, null);
		}
		else if (function=="openstate"){
			sign.setOpenState(soup.GetNamedTagAsInt("openstate", 0));
			if(cast<kmSignal>sign)
				(cast<kmSignal>sign).setMUstate(soup.GetNamedTag("MUstate"));
			Update(sign, true, null);
		}
		else if(function=="welcomestate"){
			kmSignal sig=cast<kmSignal>sign;
			if(!sig)return;
			int state=soup.GetNamedTagAsInt("state", 0);
			if((state&kmRC.O_PRIGL)==kmRC.O_PRIGL) {
				string muState = soup.GetNamedTag("MUstate");
				if (muState and muState.size()) {
					sig.setMUstate(muState);
				}
				sig.WelcomeOpen(null);
			}
			else
				sig.WelcomeCancel();
		}
	}

	void ModuleInitHandler(Message msg){
//		Interface.Print("(\""+msg.major+"\", \""+msg.minor+"\")");
		if(msg.minor!="ModuleInit")return;

		UpdateSystemIcon();

		if (mapData == null) {
			loadMapData();
		}

		CheckTrainList();
	}

	void OnJunction(Message msg){
		JunctionBase JM=cast<JunctionBase>msg.src;
		if(!JM)return;
		int i;
		GameObject JMGO = cast<GameObject>((object)JM);
		if (JMGO) {
			kmJunctionInfo jInfo = cast<kmJunctionInfo>JMGO.extension;
			if (!jInfo) {
				for (i = 0; i < junctions.size(); ++i) {
					if (JM == junctions[i].JM) {
						jInfo = junctions[i];
					}
				}
			}
			if (jInfo) {
				JMGO.PostMessage(jInfo.getOwner(), "kmJunctionToggled", jInfo.getIndex(), 0.01);
				return;		//если стрелка контролируется постом, то он сам обработает все зависимости и нет нужны обрабатывать их на уровне сигнализации
			}
		}
		for (i = JunctionBase.DIRECTION_BACKWARD; i < JunctionBase.DIRECTION_NONE; ++i) {
			GSTrackSearch ts = JM.BeginTrackSearch(i);
			if(!ts)continue;
			kmRC sig2=null;

//			if(i>0 and i!=JM.GetDirection()){//ToDo: найти ближайший светофор и исключить его из кэша поездов. А то перекроется...
//			}

			while(ts.SearchNextObject()){
				object obj=ts.GetObject();
//				if(cast<Vehicle>obj)break;
				if(ts.GetFacingRelativeToSearchDirection())continue;
				if(sig2=cast<kmRC>obj)break;
				if(cast<Signal>obj)break;//от дефолта мы ничего не получим
			}
			if(sig2){
//				Interface.Print("junction: "+JM.GetDebugString()+" search to: "+i+" found: "+sig2.GetPropertyValue("name")+" ("+sig2.GetPropertyValue("nameRC")+")");
				Update(sig2, true, null);//А вообще его надо закрыть!
			}
			kmSignal sig=cast<kmSignal>sig2;
			while(!sig and ts.SearchNextObject()){
				if(ts.GetFacingRelativeToSearchDirection())continue;
				if(sig=cast<kmSignal>ts.GetObject()){
					if(sig.GetEnableSignals()&1)
						break;
					sig=null;
					continue;
				}
				if(cast<kmRC>ts.GetObject())
					continue;
				if(cast<Signal>ts.GetObject()){
					sig=null;//поиск окончен
					break;
				}
			}
			if(sig)
				sig.WelcomeCancel();
		}
	}

	void onControlSet(Message msg) {
		if ("pok-" != msg.minor[,4]) {
			return;
		}
		ClearMessages("ColnrolSet", "pok-0-up");
		if ("-up" == msg.minor[msg.minor.size() - 3,]) {
			pokButton = 0;
			if (pokClaimedPost) {
				pokClaimedPost.resetPOK();
			}
			pokClaimedPost = null;
		}
		else {
			int newButton = Str.ToInt(msg.minor[4,]);
			if (!pokButton or pokButton == newButton) {
				pokButton = newButton;
//				PostMessage(me, "ControlSet", "pok-0-up", 1);
			}
		}

		if (pokButton) {
			string html;
			html = html + "<font color=\"#";
			if (1 == pokButton) {
				html = html + "00ff00";
			}
			else {
				html = html + "AAAAAA";
			}
			html = html + "\">ПС</font>";
			html = html + "&nbsp;<font color=\"#";
			if (2 == pokButton) {
				html = html + "00ff00";
			}
			else {
				html = html + "AAAAAA";
			}
			html = html + "\">ИР</font>";
			html = html + "&nbsp;<font color=\"#";
			if (3 == pokButton) {
				html = html + "00ff00";
			}
			else {
				html = html + "AAAAAA";
			}
			html = html + "\">Курбель/АВ</font>";
			html = html + "&nbsp;<font color=\"#";
			if (4 == pokButton) {
				html = html + "00ff00";
			}
			else {
				html = html + "AAAAAA";
			}
			html = html + "\">ВКС/Макет</font>";
			pokBrowser = Constructors.NewBrowser();
			pokBrowser.SetWindowRect(10, 10, 10 + 200, 10 + 50);
//			pokBrowser.SetWindowCentered(200, 75);
			pokBrowser.SetWindowStyle(Browser.STYLE_SLIM_FRAME);
			pokBrowser.SetScrollEnabled(true);
			pokBrowser.SetCloseEnabled(false);
			pokBrowser.SetWindowTitle("pok");
			pokBrowser.LoadHTMLString(html);
			pokBrowser.BringToFront();
		}
		else {
			pokBrowser = null;
		}
	}

	public bool tryClaimPOK(kmDscpPost post, int button) {
		if (pokClaimedPost) {
			return false;
		}
		if (button != pokButton) {
			return false;
		}
		pokClaimedPost = post;

		return true;
	}

	public Soup GetProperties() {
		saveMapData();
		return inherited();
	}

	public void Init(Asset asset){
		inherited(self=asset);
		train_arr=new BinarySortedArray();
		train_arr.UdgradeArraySize(20);
		AddHandler(me, "World", "", "ModuleInitHandler");
		AddHandler(me, "Interface", "ClickSystemButton", "OnClickSystemButton");
		AddHandler(me, "Junction", "Toggled", "OnJunction");
//		AddHandler(me, "Object", "Enter", "Enter");
//		AddHandler(me, "Object", "Leave", "Leave");
		AddHandler(me, "Train", "StartedMoving", "TrainStarting");
		AddHandler(me, "Train", "StoppedMoving", "TrainStopping");
		AddHandler(me, "Train", "Cleanup", "RemoveTrain");
		AddHandler(me, "kmSigMP", "Sync", "SyncHandler");
		AddHandler(me, "ControlSet", null, "onControlSet");
		onlineAccess = GetOnlineAccess();
		onlineAccess.Connect();
		blinker();
	}
};