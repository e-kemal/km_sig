include "signal.gs"
include "km_isojoint.gs"
include "km_signal_switch_interface.gs"
include "multiplayergame.gs"
include "km_observable.gs"
include "km_lib.gs"

class StateInfo{
	public int RCcount=0;//количество свободных РЦ
	public int BUcount=0;//количество свободных БУ
	public int ARS1=0;//"частота" АЛС-АРС
	public int ARS2=0;//предупредительная "частота"
};

class kmRC isclass Signal, kmObservable {
	Asset self;
	Browser mn;
	kmLib kmLib;
	string nameRC="";
	StringTable ST;

	public define int EVENT_ENTER_FORWARD	= 0;
	public define int EVENT_LEAVE_FORWARD	= 1;
	public define int EVENT_ENTER_BACKWARD	= 2;
	public define int EVENT_LEAVE_BACKWARD	= 3;
	public define int EVENT_RC_LAST			= 3;

//deprecated
	public define int O_TYPE		= 3;
	public define int O_TYPE_NONE	= 0;
	public define int O_TYPE_MAIN	= 1;
	public define int O_TYPE_MRT	= 2;
	public define int O_TYPE_SHUNT	= 3;
	public define int OT_AUTO		= 4;
	public define int OT_WELCOME	= 8;
	public define int OT_ZAGRAD		= 16;
	public define int OT_KGU		= 32;//aka УКСПС
	public define int OT_USE_MU		= 64+128+256;
	public define int OT_MU_ON_AB	= 64;
	public define int OT_MU_ON_ARS	= 128;
	public define int OT_MU_ON_PS	= 256;

	int RCcount=0;//количество свободных РЦ за светофором
	int BUcount=0;//количество свободных БУ за светофором

	int limitRCcount = 0;	//ограничение счётчика свободных РС при снятом флаге O_CLEAR

	public define int ARS_OCh		= 0;	//ОЧ (НЧ)
	public define int ARS_AO		= 1;	//АО (чередование 0 и ОЧ)
	public define int ARS_0			= 2;	//0
	public define int ARS_DIRECTION	= 3;	//признак направления (40/РС)
	public define int ARS_RESERVED	= 4;	//зарезервировано
	public define int ARS_MIN_SPEED	= 5;	//
	int ARS2=0;//кэшируем предупредительную частоту
	int[] RCrule;//в последствии будет подставляться нужное правило из нескольких.
	int[] RCruleMain, RCruleT, RCruleS, RCruleTS;

	public define int S_TYPE		= 3;
//	public define int S_MAIN_TYPE	= 1;
//	public define int S_ADD_TYPE	= 2;
	public define int S_TYPE_AB		= 0;
	public define int S_TYPE_ARS	= 1;
	public define int S_TYPE_AB_ARS	= 2;
	public define int S_TYPE_ARS_AB	= 3;
	public define int ST_SKIP_RC	= 4;
	public define int ST_ARS2		= 8;//наличие предупредительной частоты
	//deprecated пригласительные управляются из ЭЦ
	public define int ST_WELCOME_AUTO= 16;//автооткрытие пригласительного
	//deprecated разделка или её отсутствие реализуются на стороне ЭЦ
	public define int ST_ARS_NO_R	= 32;//при АЛС-АРС и автомаршруте не перекрывается на красный (для Киева)
	public define int ST_NO_PS		= 64;//неправильный путь, ПС запрещён
	public define int ST_ROUTE_ONLY	= 128;//перекрываться при разделке, иначе автомат
	public define int ST_SAUTO		= 128;	//полуавтомат, остаётся закрытым при отмене/разделке
	public define int ST_PUM		= 256;	//

	public define int O_SUR		= 1;	//Открытие светофора (реле СУ)
	public define int O_CLEAR	= 2;	//Открытие светофора (и передача информации предыдущим объектам)
	public define int O_CODES	= 4;	//Наличие кодов
	public define int O_RAB		= 8;	//Включение резервной АБ
	public define int O_MRT		= 16;	//Маршрут по отклонению (жСУ)
	public define int O_SHUNT	= 32;	//Маневровый маршрут (бСУ)
	public define int O_PRIGL	= 64;	//Пригласительный сигнал
	public define int O_MK		= 128;	//Ограждение МК
	public define int O_MU_AB	= 256;	//Использование МУ при сигналах АБ
	public define int O_MU_ARS	= 512;	//Использование МУ при синем показании
	public define int O_MU_PS	= 1024;	//Использование МУ при пригласительном сигнале
	public define int O_ROUTE_TYPE = O_RAB | O_MRT | O_SHUNT | O_MU_AB | O_MU_ARS | O_MU_PS;	//флаги, отвечающие за тип маршрута
	int openState = O_SUR | O_CLEAR | O_CODES;

	kmSignalSwitchInterface[] surOnSwithcers = new kmSignalSwitchInterface[0];
	kmSignalSwitchInterface[] surOffSwithcers = new kmSignalSwitchInterface[0];
	kmSignalSwitchInterface[] clearOnSwithcers = new kmSignalSwitchInterface[0];
	kmSignalSwitchInterface[] clearOffSwithcers = new kmSignalSwitchInterface[0];

	int MyType;
	int ReverseSigId=0;//вынесено сюда для реализации светофоров без изостыков (изостык принадлежит встречному светофору)
//	KUID 
	km_isojoint isojoint=null;
	Soup NullSoup;
	bool debug=false;

	void updateBrowser(Browser browser);

	public int GetMyType(){
		return MyType;
	}

	void choiceRCrule() {
		switch (openState & (O_MRT | O_SHUNT)) {
			case O_MRT | O_SHUNT:
				RCrule = RCruleTS;
				break;
			case O_SHUNT:
				RCrule = RCruleS;
				break;
			case O_MRT:
				RCrule = RCruleT;
				break;
			default:
				RCrule = RCruleMain;
				break;
		}
	}

	public void addSurOnSwithcer(kmSignalSwitchInterface switcher) {
		int i;
		int n = surOnSwithcers.size();
		for (i = 0; i < n; ++i) {
			if (switcher == surOnSwithcers[i]) {
				return;
			}
		}
		surOnSwithcers[n] = switcher;
	}

	public void removeSurOnSwitcher(kmSignalSwitchInterface switcher) {
		int i = 0;
		while (i < surOnSwithcers.size()) {
			if (switcher == surOnSwithcers[i]) {
				surOnSwithcers[i, i+1] = null;
			}
			else {
				++i;
			}
		}
	}

	public void addSurOffSwithcer(kmSignalSwitchInterface switcher) {
		int i;
		int n = surOffSwithcers.size();
		for (i = 0; i < n; ++i) {
			if (switcher == surOffSwithcers[i]) {
				return;
			}
		}
		surOffSwithcers[n] = switcher;
	}

	public void removeSurOffSwitcher(kmSignalSwitchInterface switcher) {
		int i = 0;
		while (i < surOffSwithcers.size()) {
			if (switcher == surOffSwithcers[i]) {
				surOffSwithcers[i, i+1] = null;
			}
			else {
				++i;
			}
		}
	}

	public bool hasClearOnSwitchers() {
		return clearOnSwithcers.size() > 0;
	}

	public void addClearOnSwithcer(kmSignalSwitchInterface switcher) {
		int i;
		int n = clearOnSwithcers.size();
		for (i = 0; i < n; ++i) {
			if (switcher == clearOnSwithcers[i]) {
				return;
			}
		}
		clearOnSwithcers[n] = switcher;
	}

	public void removeClearOnSwitcher(kmSignalSwitchInterface switcher) {
		int i = 0;
		while (i < clearOnSwithcers.size()) {
			if (switcher == clearOnSwithcers[i]) {
				clearOnSwithcers[i, i+1] = null;
			}
			else {
				++i;
			}
		}
	}

	public void addClearOffSwithcer(kmSignalSwitchInterface switcher) {
		int i;
		int n = clearOffSwithcers.size();
		for (i = 0; i < n; ++i) {
			if (switcher == clearOffSwithcers[i]) {
				return;
			}
		}
		clearOffSwithcers[n] = switcher;
	}

	public void removeClearOffSwitcher(kmSignalSwitchInterface switcher) {
		int i = 0;
		while (i < clearOffSwithcers.size()) {
			if (switcher == clearOffSwithcers[i]) {
				clearOffSwithcers[i, i+1] = null;
			}
			else {
				++i;
			}
		}
	}

	public int getOpenState() {
		return openState;
	}

	public void setOpenState(int state) {
		int flags = O_SUR | O_CLEAR | O_CODES | O_ROUTE_TYPE;
		state = state & flags;
		openState = openState & ~flags;
		openState = openState | state;
		choiceRCrule();
		if (mn) {
			updateBrowser(mn);
		}
	}

	public void resetSur() {
		bool newState = !(surOffSwithcers.size() > 0) and ((surOnSwithcers.size() > 0) or !(MyType & ST_SAUTO));

		if (newState) {
			openState = openState | O_SUR;
		}
		else {
			openState = openState & ~O_SUR;
		}
		if (mn) {
			updateBrowser(mn);
		}
	}

	public void resetClear() {
		bool newState = !(clearOffSwithcers.size() > 0) and ((clearOnSwithcers.size() > 0) or !(MyType & ST_PUM));

		if (newState) {
			openState = openState | O_CLEAR;
		}
		else {
			openState = openState & ~O_CLEAR;
		}
		if (!clearOffSwithcers.size() and !clearOnSwithcers.size()) {
			openState = openState & ~(O_MRT | O_SHUNT);
		}
		if (mn) {
			updateBrowser(mn);
		}
	}

	int GetARS(){
		if (!(openState & O_CODES)) {
			return ARS_OCh;
		}
		if (!(openState & O_CLEAR)) {
			return ARS_AO;	//АО, ToDo: отдельную настройку для кода при незаданном маршруте
		}
//		if (!RCrule) {
			choiceRCrule();
//		}
		int n = RCcount;
		if (n >= RCrule.size()) {
			n = RCrule.size() - 1;
		}
		if (n < 0)
			return ARS_OCh;
		return RCrule[n];
	}

	void ApplySignalState(){
		int sigstate=EX_STOP;
		int ars = GetARS();
		if(MyType&S_TYPE_ARS){
			if (ars != ARS_AO and ars != ARS_0){
				if (ars == ARS_OCh or ARS2 == ARS_OCh)
					sigstate = EX_STOP_THEN_CONTINUE;
				else if (ARS2 > 50)
					sigstate = EX_PROCEED;
				else if (ARS2 >= ARS_MIN_SPEED or ARS2 == ARS_DIRECTION)
					sigstate = EX_CAUTION;
			}
		}
		else{
			if(openState & O_SUR){
				if(BUcount>2)sigstate=EX_PROCEED;
				else if(BUcount>1)sigstate=EX_ADVANCE_CAUTION;
				else if(BUcount>0)sigstate=EX_CAUTION;
			}
		}
		SetSignalStateEx(null, sigstate, "ARS=" + ars);
	}

	public StateInfo GetState(){
		StateInfo ret;
		if(MyType&ST_SKIP_RC){//учитывая, что таких светофоров ожидается не много, проще сделать так
			kmRC sig2=null;
			bool DefaultFounded=false;
			GSTrackSearch ts=BeginTrackSearch(true);
			while(ts.SearchNextObject()){
				if(!ts.GetFacingRelativeToSearchDirection())continue;
				object obj=ts.GetObject();
				if(sig2=cast<kmRC>obj)break;
				if(cast<Vehicle>obj)break;
				if(cast<Signal>obj){
					DefaultFounded=true;
					break;//от дефолта мы ничего не получим
				}
			}
			if(sig2)ret=sig2.GetState();
			else{
				ret=new StateInfo();
				if(DefaultFounded)//Если нашёлся дефолтный светофор, то участок до него считаем одной свободной РЦ. Это позволит светофору открыться на КЖ
					ret.RCcount=1;//количество свободных РЦ
				else
					ret.RCcount=0;//количество свободных РЦ
				ret.BUcount=0;//количество свободных БУ
				ret.ARS1 = ARS_OCh;//"частота" АЛС-АРС
				ret.ARS2 = ARS_OCh;//предупредительная "частота"
			}
			if (!(openState & O_CLEAR) or openState & O_MK) {
				if (ret.RCcount > limitRCcount) {
					ret.RCcount = limitRCcount;
				}
				ret.BUcount = 0;
				ret.ARS1 = ARS_AO;
				ret.ARS2 = ARS_OCh;
			}
			return ret;
		}
		ret = new StateInfo();
		ret.RCcount = RCcount + 1;
		if (openState & O_CLEAR and !(openState & O_MK)) {
			if (openState & O_SUR)
				ret.BUcount = BUcount;//для светофоров +1
			else
				ret.BUcount = 0;
		}
		else {
			if (ret.RCcount > limitRCcount + 1) {
				ret.RCcount = limitRCcount + 1;
			}
			ret.BUcount = 0;
		}
		ret.ARS1 = GetARS();
		if (MyType & ST_ARS2 and openState & O_CODES)
			ret.ARS2 = ARS2;
		else
			ret.ARS2 = ARS_OCh;
		if(debug)
			Interface.Print(nameRC+".GetState() RC: "+ret.RCcount+" BU: "+ret.BUcount+" ARS1: "+ret.ARS1+" ARS2: "+ret.ARS2);
		return ret;
	}

	bool UpdateStateBU(StateInfo state) {
		bool ret = false;
		if (state.BUcount != BUcount) {
			ret = true;	//впереди открылся светофор. предыдущий светофор должен узнать об этом, даже если обновление РЦ уже завершено
		}
		BUcount = state.BUcount;
		if (!(openState & O_CLEAR) or !(openState & O_SUR)) {
			state.BUcount = 0;
		}
		return ret;
	}

	public bool UpdateState(StateInfo state){
		if(debug)
			Interface.Print(nameRC+".UpdateState() RC: "+state.RCcount+" BU: "+state.BUcount+" ARS1: "+state.ARS1+" ARS2: "+state.ARS2);
		if (!RCrule) {
			choiceRCrule();
		}
		bool ret = RCcount != state.RCcount and Math.Max(state.RCcount, RCcount) <= RCrule.size();
		RCcount = state.RCcount;
		if (UpdateStateBU(state))
			ret = true;
		ARS2 = state.ARS1;
		if (openState & O_CLEAR and !(openState & O_MK)) {
			if (MyType & ST_SKIP_RC) {
				ret = true;
			}
			else {
				if (MyType & ST_ARS2 and openState & O_CODES)
					state.ARS2 = ARS2;
				else
					state.ARS2 = ARS_OCh;
				state.ARS1 = GetARS();
			}
		}
		else {
			if (state.RCcount > limitRCcount) {
				state.RCcount = limitRCcount;
			}
			state.ARS1 = GetARS();
			state.ARS2 = ARS_OCh;
			ret = true;
		}
		if (!(MyType & ST_SKIP_RC)) {
			++state.RCcount;
		}
		ApplySignalState();
		
		if(debug)
			Interface.Print("RC: "+state.RCcount+" BU: "+state.BUcount+" ARS1: "+state.ARS1+" ARS2: "+state.ARS2);
		if (mn) {
			updateBrowser(mn);
		}
		return ret;
	}

	//deprecated
	public void Open(int type, bool auto){//Вызывается маршрутизацией при задании маршрута
		type=type&O_TYPE;
		if(auto and !type)type=1;
		openState = openState | O_SUR | O_CLEAR | O_CODES;
		openState = openState & ~(O_MRT | O_SHUNT);
		if (type == O_TYPE_MRT) {
			openState = openState | O_MRT;
		}
		if (type == O_TYPE_SHUNT) {
			openState = openState | O_SHUNT;
		}
		choiceRCrule();
		if (mn) {
			updateBrowser(mn);
		}
	}

	//deprecated
	public void Close(bool force){//force - закрыть и разобрать маршрут
		RCcount=0;
		BUcount=0;
		ARS2=0;
		if (force)
			openState = openState & ~(O_SUR | O_CLEAR);
		ApplySignalState();
		if (mn) {
			updateBrowser(mn);
		}
	}

	public void Close(){//вызывается библиотекой при наезде на РЦ
		Close(false);
	}

	public StateInfo ReOpen(){//Съезд по направлению
		
		RCcount=0;
		BUcount=0;
		return GetState();
	}

	public void enterForward() {
		Close(false);
		notifyObservers(EVENT_ENTER_FORWARD);
	}

	public void leaveForward() {
		notifyObservers(EVENT_LEAVE_FORWARD);
	}

	public void enterBackward() {
		notifyObservers(EVENT_ENTER_BACKWARD);
	}

	public void leaveBackward() {
		notifyObservers(EVENT_LEAVE_BACKWARD);
	}

	public float GetOverlap(){//TODO: для совместимости с дефолтом
		//вернём расстояние до ближайшего светофора
		//светофоры будут вычислять исходя из своего защитного участка
		return 100.0;
	}

	string[] GetChars(string s){
		string[] ret= new string[0];
//		ret[0]="";
		int i=0;
		int k=-1;
		bool f=false;

		while(i<s.size()){
			if(!f){
				k++;
				ret[k,k+1]=new string[1];
				ret[k]="";
			}

			string s2=s[i,i+2];
			if(s2>="А"){			// кириллица, кодировка UTF-8, отображение как CP1251
				ret[k]=ret[k]+s[i,i+2];
				i++;
			}
			else{
				s2=s2[0,1];

				if(s2=="(")f=true;
				else if(s2==")")f=false;
				else ret[k]=ret[k]+s[i,i+1];
			}
			i++;
		}
		return ret;
	}

	public void SetNameRC(string value){
		nameRC=value;
		if(isojoint)isojoint.SetNameR(value);
//		if(World.GetCurrentModule()==World.SURVEYOR_MODULE){
		MeshObject marker;
		string[] tmp=GetChars(value);
		value="";
		int i;
		for(i=0;i<tmp.size();i++)
			value=value+tmp[i];
		if(value=="")value=" ";
		if(marker=GetFXAttachment("marker"))//А всё почему? Потому что тег surveyor-only доступен только в эффектах, но не в субмешах
			marker.SetFXNameText("nameRC", value);
//		}
	}

	public void SetBackNameRC(string value){
		if(isojoint)isojoint.SetNameL(value);
	}

	public string GetDescriptionHTML(void){
		string s="<font size=7 color='#00EFBF'><b>"+self.GetStringTable().GetString("object_desc")+"</b></font><br>";
		s=s+"<table border='1' width=90%>";

		s=s+"<tr><td bgcolor='#666666'>MainType:</td><td bgcolor='#AAAAAA'><a href=\"live://property/typeAB\">"+HTMLWindow.RadioButton("", !(MyType&S_TYPE_ARS))+"AB</a>&nbsp;<a href=\"live://property/typeARS\">"+HTMLWindow.RadioButton("", MyType&S_TYPE_ARS)+"ALS-ARS</a></td></tr>";

		if (MyType & S_TYPE_ARS) {
			s = s + "<tr><td bgcolor='#666666'>AddType:</td><td bgcolor='#AAAAAA'><a href='live://property/openState^" + O_RAB + "'>" + HTMLWindow.CheckBox("", openState & O_RAB) + " + RAB</a></td></tr>";
		}

		s = s + "<tr><td bgcolor='#666666'>s auto:</td><td bgcolor='#AAAAAA'><a href='live://property/sauto'>" + HTMLWindow.CheckBox("", MyType & ST_SAUTO) + "s auto</a></td></tr>";
		s = s + "<tr><td bgcolor='#666666'>pum:</td><td bgcolor='#AAAAAA'><a href='live://property/pum'>" + HTMLWindow.CheckBox("", MyType & ST_PUM) + "pum</a></td></tr>";

		s=s+"<tr><td bgcolor='#666666'>ARS2:</td><td bgcolor='#AAAAAA'><a href='live://property/ARS2'>"+HTMLWindow.CheckBox("", MyType&ST_ARS2)+"enable</a></td></tr>";

		s=s+"<tr><td bgcolor='#666666'>nameRC:</td><td bgcolor='#AAAAAA'><a href='live://property/nameRC'>";
		if(nameRC!="")s=s+nameRC;
		else s=s+"<i>none</i>";
		s=s+"</a></td></tr>";

		s=s+"<tr><td bgcolor='#666666'>isojoint:</td><td bgcolor='#AAAAAA'><a href='live://property/isojoint'>";
		if(isojoint!=null)s=s+isojoint.GetAsset().GetLocalisedName();
		else s=s+"<i>none</i>";
		s=s+"</a></td></tr>";

		s = s + "<tr><td bgcolor='#666666'>Open:</td><td bgcolor='#AAAAAA'>";
		s = s + "<a href=\"live://property/openState^" + O_SUR + "\">" + HTMLWindow.CheckBox("", openState & O_SUR) + "SUR</a>&nbsp;";
		s = s + "<a href=\"live://property/openState^" + O_CLEAR + "\">" + HTMLWindow.CheckBox("", openState & O_CLEAR) + "CLEAR</a>&nbsp;";
		s = s + "<a href=\"live://property/openState^" + O_CODES + "\">" + HTMLWindow.CheckBox("", openState & O_CODES) + "CODES</a>";
		s = s + "</td></tr>";

		s = s + "<tr><td bgcolor='#666666'>Route type:</td><td bgcolor='#AAAAAA'>";
		s = s + "<a href=\"live://property/openState^" + O_MRT + "\">" + HTMLWindow.CheckBox("", openState & O_MRT) + "MRT</a>&nbsp;";
		s = s + "<a href=\"live://property/openState^" + O_SHUNT + "\">" + HTMLWindow.CheckBox("", openState & O_SHUNT) + "SHUNT</a>";
		s = s + "</td></tr>";

		s=s+"<tr><td bgcolor='#666666'>Skip RC:</td><td bgcolor='#AAAAAA'><a href='live://property/skipRC'>"+HTMLWindow.CheckBox("", MyType&ST_SKIP_RC)+"skip</a></td></tr>";

		s = s + "<tr><td bgcolor='#666666'>limit RCcount:</td><td bgcolor='#AAAAAA'><a href='live://property/limitRCcount'>" + limitRCcount + "</a></td></tr>";
		s=s+"<tr><td bgcolor='#666666'>RCcount:</td><td bgcolor='#AAAAAA'>"+RCcount+"</td></tr>";
		s=s+"<tr><td bgcolor='#666666'>BUcount:</td><td bgcolor='#AAAAAA'>"+BUcount+"</td></tr>";
		s=s+"<tr><td bgcolor='#666666'>Force update:</td><td bgcolor='#AAAAAA'><a href='live://property/ForceUpdate'>Update</a></td></tr>";

		s=s+"</table>";
		s=s+"debug: <a href='live://property/debug'>"+debug+"</a><br>";
		s = s + "<table border='1' width=90%>";
		s = s + "<tr><td bgcolor='#666666'>RCcount</td><td bgcolor='#666666'>main</td><td bgcolor='#666666'>mrt</td><td bgcolor='#666666'>shunt</td><td bgcolor='#666666'>shunt mrt</td></tr>";
		int i, n = Math.Max(Math.Max(RCruleMain.size(), RCruleT.size()), Math.Max(RCruleS.size(), RCruleTS.size()));
		GSTrackSearch ts = BeginTrackSearch(true);
		kmRC sig = null;
		for (i = 0; i <= n; i++) {
			s = s + "<tr><td bgcolor='#666666'>" + i;
			if(i and ts) {
				while(true) {
					if (!ts.SearchNextObject()) {
						ts = null;//поиск окончен
						sig = null;
						break;
					}
					if (!ts.GetFacingRelativeToSearchDirection())
						continue;
					if (sig = cast<kmRC>ts.GetObject()){
						if (sig.GetMyType() & ST_SKIP_RC)
							continue;
						string str = sig.GetPropertyValue("nameRC_clean");
						if (str == "")
							str = "<i>uncknown</i>";
						s = s + "&nbsp;(" + str + ",&nbsp;" + ts.GetDistance() + "m)";
						break;
					}
					if (cast<Signal>ts.GetObject()){
						s = s + "&nbsp;(<i>uncknown</i>,&nbsp;" + ts.GetDistance() + "m)";
						ts = null;//поиск окончен
						sig = null;
						break;
					}
				}
			}
			s = s + "</td><td bgcolor='#AAAAAA'>";
			if (i < RCruleMain.size())
				s = s + "<a href='live://property/ARS/edit/main/" + i + "'>" + RCruleMain[i] + "</a> (<a href='live://property/ARS/ins/main/" + i + "'>+</a>&nbsp;<a href='live://property/ARS/del/main/" + i + "'>-</a>)";
			else if (i == RCruleMain.size())
				s = s + "<a href='live://property/ARS/edit/main/" + i + "'>add</a>";
			else
				s = s + "&nbsp;";
			s = s + "</td><td bgcolor='#AAAAAA'>";
			if (i < RCruleT.size())
				s = s + "<a href='live://property/ARS/edit/mrt/" + i + "'>" + RCruleT[i] + "</a> (<a href='live://property/ARS/ins/mrt/" + i + "'>+</a>&nbsp;<a href='live://property/ARS/del/mrt/" + i + "'>-</a>)";
			else if (i == RCruleT.size())
				s = s + "<a href='live://property/ARS/edit/mrt/" + i + "'>add</a>";
			else
				s = s + "&nbsp;";
			s = s + "</td><td bgcolor='#AAAAAA'>";
			if (i < RCruleS.size())
				s = s + "<a href='live://property/ARS/edit/shunt/" + i + "'>" + RCruleS[i] + "</a> (<a href='live://property/ARS/ins/shunt/" + i + "'>+</a>&nbsp;<a href='live://property/ARS/del/shunt/" + i + "'>-</a>)";
			else if (i == RCruleS.size())
				s = s + "<a href='live://property/ARS/edit/shunt/" + i + "'>add</a>";
			else
				s = s + "&nbsp;";
			s = s + "</td><td bgcolor='#AAAAAA'>";
			if (i < RCruleTS.size())
				s = s + "<a href='live://property/ARS/edit/ts/" + i + "'>" + RCruleTS[i] + "</a> (<a href='live://property/ARS/ins/ts/" + i + "'>+</a>&nbsp;<a href='live://property/ARS/del/ts/" + i + "'>-</a>)";
			else if (i == RCruleTS.size())
				s = s + "<a href='live://property/ARS/edit/ts/" + i + "'>add</a>";
			else
				s = s + "&nbsp;";
			s = s + "</td></tr>";
		}
		s = s + "</table>";
		return s;
	}

	string GetContentViewDetails(void){
		string s="<font size=7 color='#00EFBF'><b>"+self.GetStringTable().GetString("object_desc")+"</b></font><br>";
		/*s=s+"Type: "+MyType+"<br>";
		StateInfo state=GetState();
		s=s+"StateInfo:<br>";
		s=s+"RCcount: "+state.RCcount+"<br>";
		s=s+"BUcount: "+state.BUcount+"<br>";
		s=s+"ARS1: "+state.ARS1+"<br>";
		s=s+"ARS2: "+state.ARS2+"<br>";*/
		s=s+"<table border='1' width=90%>";

		s=s+"<tr><td bgcolor='#666666'>MainType:</td><td bgcolor='#AAAAAA'>"+HTMLWindow.RadioButton("", !(MyType&S_TYPE_ARS))+"AB&nbsp;"+HTMLWindow.RadioButton("", MyType&S_TYPE_ARS)+"ALS-ARS</td></tr>";

		if (MyType & S_TYPE_ARS) {
			s = s + "<tr><td bgcolor='#666666'>AddType:</td><td bgcolor='#AAAAAA'><a href='live://property/openState^" + O_RAB + "'>" + HTMLWindow.CheckBox("", openState & O_RAB) + " + RAB</a></td></tr>";
		}

		s=s+"<tr><td bgcolor='#666666'>nameRC:</td><td bgcolor='#AAAAAA'>";
		if(nameRC!="")s=s+nameRC;
		else s=s+"<i>none</i>";
		s=s+"</td></tr>";

		s = s + "<tr><td bgcolor='#666666'>Open:</td><td bgcolor='#AAAAAA'>";
		s = s + "<a href=\"live://property/openState^" + O_SUR + "\">" + HTMLWindow.CheckBox("", openState & O_SUR) + "SUR</a>&nbsp;";
		s = s + "<a href=\"live://property/openState^" + O_CLEAR + "\">" + HTMLWindow.CheckBox("", openState & O_CLEAR) + "CLEAR</a>&nbsp;";
		s = s + "<a href=\"live://property/openState^" + O_CODES + "\">" + HTMLWindow.CheckBox("", openState & O_CODES) + "CODES</a>";
		s = s + "</td></tr>";

		s = s + "<tr><td bgcolor='#666666'>Route type:</td><td bgcolor='#AAAAAA'>";
		s = s + "<a href=\"live://property/openState^" + O_MRT + "\">" + HTMLWindow.CheckBox("", openState & O_MRT) + "MRT</a>&nbsp;";
		s = s + "<a href=\"live://property/openState^" + O_SHUNT + "\">" + HTMLWindow.CheckBox("", openState & O_SHUNT) + "SHUNT</a>";
		s = s + "</td></tr>";

		s=s+"<tr><td bgcolor='#666666'>RCcount:</td><td bgcolor='#AAAAAA'>"+RCcount+"</td></tr>";
		s=s+"<tr><td bgcolor='#666666'>BUcount:</td><td bgcolor='#AAAAAA'>"+BUcount+"</td></tr>";
		s=s+"<tr><td bgcolor='#666666'>Force update:</td><td bgcolor='#AAAAAA'><a href='live://property/ForceUpdate'>Update</a></td></tr>";
		s=s+"<tr><td bgcolor='#666666'>Next object:</td><td bgcolor='#AAAAAA'>";
		kmRC sig=null;
		GSTrackSearch ts=BeginTrackSearch(true);
		while(!sig and ts.SearchNextObject()){
			if(!ts.GetFacingRelativeToSearchDirection())continue;
			object obj=ts.GetObject();
			if(sig=cast<kmRC>obj)
				continue;
			if(cast<Signal>obj)break;//от дефолта мы ничего не получим
		}
		if(sig)
			s=s+"<a href='live://property/NextObject'>"+sig.GetPropertyValue("name_clean")+"&nbsp;("+sig.GetPropertyValue("nameRC_clean")+")</a>,&nbsp;"+ts.GetDistance()+"m";
		else s=s+"<i>none</i>";
		s=s+"</td></tr>";

		s=s+"</table>";
		s=s+"debug: <a href='live://property/debug'>"+debug+"</a><br>";
		return s;
	}

	string GetPropertyType(string id){
		if(id=="nameRC")return "string";
		if(id=="isojoint")return "asset-list,#KMISJ";
		if(id=="typeAB")return "link";
		if(id=="typeARS")return "link";
		if(id=="addtype")return "link";
		if(id=="sauto") return "link";
		if(id=="pum") return "link";
		if(id=="ARS2")return "link";
		if (id[,10] == "openState^") return "link";
		if(id[,4]=="Open")return "link";
		if(id=="AutoOpen")return "link";
		if(id=="skipRC")return "link";
		if (id == "limitRCcount") return "int,0,100,1";
		if(id=="ForceUpdate")return "link";
		if(id=="debug")return "link";
//		if(id[,10]=="RCmainRule")return "int,0,100,1";
//		if(id[,9]=="RCmrtRule")return "int,0,100,1";
//		if(id[,11]=="RCshuntRule")return "int,0,100,1";
		if(id[,9]=="ARS/edit/")return "int,0,100,1";
		if(id[,8]=="ARS/ins/")return "int,0,100,1";
		if(id[,8]=="ARS/del/")return "link";
//		if(id[,7]=="ARSmrt/")return "link";
//		if(id[,9]=="ARSshunt/")return "link";
		return inherited(id);
	}

	public bool FilterPropertyElementList(string id, GSObject[] listObjects, string[] listNames) {
		if ("isojoint" == id) {
			listObjects[0, 0] = new GSObject[1];
			listObjects[0] = self;		//К сожалению, нельзя вернуть null

			listNames[0, 0] = new string[1];
			listNames[0] = "none";

			return true;
		}

		return inherited(id, listObjects, listNames);
	}

	void SetPropertyValue(string id, GSObject value, string readableName) {
		if ("isojoint" == id) {
			Asset isoAsset = cast<Asset>value;
			if (isoAsset) {
				isojoint = cast<km_isojoint>SetFXAttachment("isojoint", isoAsset);
			}
			if (!isojoint) {
				SetFXAttachment("isojoint", null);//мало ли
			}
			if (isojoint or value == self) {
				kmLib.setIsojointEdited(isoAsset);
			}
			SetNameRC(nameRC);
		}
		else {
			inherited(id, value, readableName);
		}
	}

	void LinkPropertyValue(string id){
		if(id=="typeAB"){
			MyType = MyType & ~S_TYPE_ARS;
			kmLib.Update(me, true, null);
		}
		else if(id=="typeARS"){
			MyType = MyType | S_TYPE_ARS;
			kmLib.Update(me, true, null);
		}
		else if(id=="ARS2")MyType=MyType^ST_ARS2;
		else if (id == "sauto") MyType = MyType ^ ST_SAUTO;
		else if (id == "pum") MyType = MyType ^ ST_PUM;
		else if(id[,10] == "openState^") {
			int n = Str.ToInt(id[10,]);
			openState = openState ^ n;
			if (n & (O_MRT | O_SHUNT))
				choiceRCrule();

			kmLib.Update(me, true, null);
			if(MultiplayerGame.IsActive()){
				Soup SyncSoup=Constructors.NewSoup();
				SyncSoup.SetNamedTag("function", "openState");
				SyncSoup.SetNamedTag("id", GetGameObjectID());
				SyncSoup.SetNamedTag("openState", openState);
				MultiplayerGame.BroadcastGameplayMessage("kmSigMP", "Sync", SyncSoup);
			}
		}
		else if(id=="skipRC")MyType=MyType^ST_SKIP_RC;
		else if(id=="ForceUpdate"){
			kmLib.Update(me, true, null);
			if(MultiplayerGame.IsActive()){
				Soup SyncSoup=Constructors.NewSoup();
				SyncSoup.SetNamedTag("function", "ForceUpdate");
				SyncSoup.SetNamedTag("id", GetGameObjectID());
				MultiplayerGame.BroadcastGameplayMessage("kmSigMP", "Sync", SyncSoup);
			}
		}
		else if(id=="NextObject"){
			kmRC sig=null;
			GSTrackSearch ts=BeginTrackSearch(true);
			while(!sig and ts.SearchNextObject()){
				if(!ts.GetFacingRelativeToSearchDirection())continue;
				object obj=ts.GetObject();
				if(sig=cast<kmRC>obj)
					continue;
				if(cast<Signal>obj)break;//от дефолта мы ничего не получим
			}
			if(sig)
				PostMessage(sig, "MapObject", "View-Details", 0.1);
		}
		else if(id=="debug")debug=!debug;
		else if(id[,8]=="ARS/del/"){
			string[] tok=Str.Tokens(id, "/");
			if(tok.size()>3){
				int n=Str.ToInt(tok[3]);

				if(tok[2]=="main"){
					if (n < RCruleMain.size())
						RCruleMain[n, n+1] = null;
				}
				else if(tok[2]=="mrt"){
					if (n < RCruleT.size())
						RCruleT[n, n+1] = null;
				}
				else if(tok[2]=="shunt"){
					if (n < RCruleS.size())
						RCruleS[n,n+1] = null;
				}
				else if (tok[2] == "ts") {
					if (n < RCruleTS.size())
						RCruleTS[n,n+1] = null;
				}
			}
		}
		else inherited(id);
		if (id=="typeAB" or id=="typeARS" or id=="addtype" or id=="ARS2" or id=="skipRC") {
			kmLib.setLastEditedType(MyType);
		}
	}

	void SetPropertyValue(string id, int value){
		if (id == "limitRCcount")
			limitRCcount = value;
		else if (id[,4]=="ARS/") {
			string[] tok=Str.Tokens(id, "/");
			if(tok.size()>3){
				int n=Str.ToInt(tok[3]);

				if(tok[2]=="main"){
					if (n > RCruleMain.size())
						n = RCruleMain.size();
					if(tok[1]=="ins")
						RCruleMain[n, n] = new int[1];
					RCruleMain[n] = value;
				}
				else if(tok[2]=="mrt"){
					if (n > RCruleT.size())
						n = RCruleT.size();
					if(tok[1]=="ins")
						RCruleT[n, n] = new int[1];
					RCruleT[n] = value;
				}
				else if(tok[2]=="shunt"){
					if (n > RCruleS.size())
						n = RCruleS.size();
					if(tok[1]=="ins")
						RCruleS[n, n] = new int[1];
					RCruleS[n] = value;
				}
				else if(tok[2]=="ts"){
					if (n > RCruleTS.size())
						n = RCruleTS.size();
					if(tok[1]=="ins")
						RCruleTS[n, n] = new int[1];
					RCruleTS[n] = value;
				}
			}
		}/*
		else if(id[,6]=="RCrule"){
			int n=Str.ToInt(id[6,]);
			if(n>RCrule.size())
				n=RCrule.size();
			RCrule[n]=value;
		}/*
		else if(id[,10]=="RCmainRule"){
			int n=Str.ToInt(id[10,]);
			if(n>RCmainRule.size())
				n=RCmainRule.size();
			RCmainRule[n]=value;
		}
		else if(id[,9]=="RCmrtRule"){
			int n=Str.ToInt(id[9,]);
			if(n>RCmrtRule.size())
				n=RCmrtRule.size();
			RCmrtRule[n]=value;
		}
		else if(id[,11]=="RCshuntRule"){
			int n=Str.ToInt(id[11,]);
			if(n>RCshuntRule.size())
				n=RCshuntRule.size();
			RCshuntRule[n]=value;
		}*/
		else inherited(id, value);
	}

	void SetPropertyValue(string id, string value){
		if(id=="nameRC"){
			SetNameRC(value);
//			GSTrackSearch ts=BeginTrackSearch(!dir);
//			kmRC sig=null;
//			while(ts.SearchNextObject() and !(sig=cast<kmRC>ts.GetObject()));
//			if(sig)sig.SetNameRC(ts.GetFacingRelativeToSearchDirection(), value);
		}
		else inherited(id, value);
	}

	public string GetPropertyValue(string id){
		if(id=="nameRC")return nameRC;
		if(id=="nameRC_clean"){
			string[] tmp=GetChars(nameRC);
			string ret="";
			int i;
			for(i=0;i<tmp.size();i++)
				ret=ret+tmp[i];
			return ret;
		}
		if (id == "limitRCcount")
			return (string)limitRCcount;
		if(id[,4]=="ARS/"){
			string[] tok=Str.Tokens(id, "/");
			if(tok.size()>3){
				int n=Str.ToInt(tok[3]);
				if(tok[2]=="main"){
					if (n < RCruleMain.size())
						return (string)RCruleMain[n];
					if (RCruleMain.size())
						return (string)RCruleMain[RCruleMain.size() - 1];
					return "0";
				}
				else if(tok[2]=="mrt"){
					if (n < RCruleT.size())
						return (string)RCruleT[n];
					if (RCruleT.size())
						return (string)RCruleT[RCruleT.size() - 1];
					return "0";
				}
				else if(tok[2]=="shunt"){
					if (n < RCruleS.size())
						return (string)RCruleS[n];
					if (RCruleS.size())
						return (string)RCruleS[RCruleS.size() - 1];
					return "0";
				}
				else if(tok[2] == "ts") {
					if (n < RCruleTS.size())
						return (string)RCruleTS[n];
					if (RCruleTS.size())
						return (string)RCruleTS[RCruleTS.size() - 1];
					return "0";
				}
			}
		}
		return inherited(id);
	}

	public void AppendDependencies(KUIDList io_dependencies){
		inherited(io_dependencies);
		if(isojoint and isojoint.GetAsset())
			io_dependencies.AddKUID(isojoint.GetAsset().GetKUID());
	}

	public Soup GetProperties(void){
		Soup sp=inherited();
		sp.SetNamedTag("MyType", MyType);
		sp.SetNamedTag("openState", openState);
		sp.SetNamedTag("limitRCcount", limitRCcount);
		sp.SetNamedTag("RCcount", RCcount);
		sp.SetNamedTag("BUcount", BUcount);
		sp.SetNamedTag("ARS2", ARS2);
		if(isojoint)
			sp.SetNamedTag("isojoint", isojoint.GetAsset().GetKUID());
		sp.SetNamedTag("nameRC", nameRC);
		int i;
		sp.SetNamedTag("RCmainRuleN", RCruleMain.size());
		for (i = 0; i < RCruleMain.size(); ++i)
			sp.SetNamedTag("RCmainRule" + i, RCruleMain[i]);
		sp.SetNamedTag("RCmrtRuleN", RCruleT.size());
		for (i = 0; i < RCruleT.size(); ++i)
			sp.SetNamedTag("RCmrtRule" + i, RCruleT[i]);
		sp.SetNamedTag("RCshuntRuleN", RCruleS.size());
		for (i = 0; i < RCruleS.size(); ++i)
			sp.SetNamedTag("RCshuntRule" + i, RCruleS[i]);
		sp.SetNamedTag("RCmrtshuntRuleN", RCruleTS.size());
		for (i = 0; i < RCruleTS.size(); ++i)
			sp.SetNamedTag("RCmrtshuntRule" + i, RCruleTS[i]);

		if(true){//MSig compatibility
			sp.SetNamedTag("MSig-type", "DNEPR");
			int code=0;
			if(MyType&S_TYPE)
				code=GetARS();
			if(code>29)
				sp.SetNamedTag("MSig-als-fq", code*0.1);
			else if(code==2)
				sp.SetNamedTag("MSig-als-fq", 0);
			else if(code==1)
				sp.SetNamedTag("MSig-als-fq", 2);
			else
				sp.SetNamedTag("MSig-als-fq", 1);
			if(!((MyType&S_TYPE) and (MyType&ST_ARS2)))
				sp.SetNamedTag("MSig-als-fq-next", 1);
			else if(ARS2>29)
				sp.SetNamedTag("MSig-als-fq-next", ARS2*0.1);
			else if(ARS2==2)
				sp.SetNamedTag("MSig-als-fq-next", 0);
			else if(ARS2==1)
				sp.SetNamedTag("MSig-als-fq-next", 2);
			else
				sp.SetNamedTag("MSig-als-fq-next", 1);
		}
		return sp;
	}

	public void SetProperties(Soup sp){
		inherited(sp);
		KUID isoKUID=null;
		MyType=sp.GetNamedTagAsInt("MyType", -1);//S_TYPE_AB_ARS
		if(MyType==-1){
			MyType = kmLib.getLastEditedType();
			Asset isoAsset = kmLib.getIsojointEdited();
			if (isoAsset) {
				isojoint = cast<km_isojoint>SetFXAttachment("isojoint", isoAsset);
			}
			if(!isojoint)SetFXAttachment("isojoint", null);//мало ли
			//и прочие вещи, характерные для только что установленных объектов.
		//	isoKUID=self.LookupKUIDTable("isojoint");//Сцуко! Ну почему нельзя молча вернуть null?!
		}
		else{
			isoKUID=sp.GetNamedTagAsKUID("isojoint");
			Asset isoAsset;
			if(isoKUID and (isoAsset=World.FindAsset(isoKUID)))isojoint=cast<km_isojoint>SetFXAttachment("isojoint", isoAsset);
			if(!isojoint)SetFXAttachment("isojoint", null);//мало ли
		}

		int oldOpenState = openState;
		openState = sp.GetNamedTagAsInt("openState", -1);
		if (openState == -1) {
			int opened = sp.GetNamedTagAsBool("Opened", -1);
			if (opened == -1) {
				openState = oldOpenState;
			}
			else {
				openState = 0;
				if (opened & O_TYPE)
					openState = O_SUR | O_CLEAR | O_CODES;
				if ((MyType & S_TYPE) == S_TYPE_ARS_AB)
					openState = openState | O_RAB;
				if ((opened & O_TYPE) == O_TYPE_MRT)
					openState = openState | O_MRT;
				if ((opened & O_TYPE) == O_TYPE_SHUNT)
					openState = openState | O_SHUNT;
				if (opened & OT_WELCOME)
					openState = openState | O_PRIGL;
				if (opened & OT_ZAGRAD)
					openState = openState | O_MK;
				if (opened & OT_MU_ON_AB)
					openState = openState | O_MU_AB;
				if (opened & OT_MU_ON_ARS)
					openState = openState | O_MU_ARS;
				if (opened & OT_MU_ON_PS)
					openState = openState | O_MU_PS;
			}
		}
		limitRCcount = sp.GetNamedTagAsInt("limitRCcount", 0);
		RCcount=sp.GetNamedTagAsInt("RCcount", 0);
		BUcount=sp.GetNamedTagAsInt("BUcount", 0);
		ARS2=sp.GetNamedTagAsInt("ARS2", ARS_OCh);
		int i, n;

		n=sp.GetNamedTagAsInt("RCruleN", 0);
		if(n){
			RCruleMain = new int[n];
			RCruleT = new int[0];
			RCruleS = new int[0];
			RCruleTS = new int[0];
			int m=sp.GetNamedTagAsInt("RCrule_mrt", 1);
			int s=sp.GetNamedTagAsInt("RCrule_shunt", 1);
			for(i=0;i<n;i++){
				RCruleMain[i] = sp.GetNamedTagAsInt("RCrule" + i, 0);
				if (i <= m)
					RCruleT[i] = RCruleMain[i];
				if (i <= s)
					RCruleS[i] = RCruleMain[i];
				if (i <= m and i <= s)
					RCruleTS[i] = RCruleMain[i];
			}
		}
		else{
			n=sp.GetNamedTagAsInt("RCmainRuleN", 0);
			if(n){
				RCruleMain = new int[n];
				for(i=0;i<n;i++)
					RCruleMain[i] = sp.GetNamedTagAsInt("RCmainRule" + i, 0);

				n=sp.GetNamedTagAsInt("RCmrtRuleN", 0);
				RCruleT = new int[n];
				for(i=0;i<n;i++)
					RCruleT[i] = sp.GetNamedTagAsInt("RCmrtRule" + i, 0);

				n=sp.GetNamedTagAsInt("RCshuntRuleN", 0);
				RCruleS = new int[n];
				for(i=0;i<n;i++)
					RCruleS[i] = sp.GetNamedTagAsInt("RCshuntRule" + i, 0);

				n = sp.GetNamedTagAsInt("RCmrtshuntRuleN", -1);
				if (n < 0) {
					n = Math.Min(RCruleT.size(), RCruleS.size());
					RCruleTS = new int[n];
					for (i = 0; i < n; ++i)
						RCruleTS[i] = Math.Min(RCruleT[i], RCruleS[i]);
				}
				else {
					RCruleTS = new int[n];
					for (i = 0; i < n; ++i)
						RCruleTS[i] = sp.GetNamedTagAsInt("RCmrtshuntRule" + i, 0);
				}
			}
			else{
				RCruleMain = new int[6];
				RCruleMain[0] = ARS_0;
				RCruleMain[1] = ARS_0;
				RCruleMain[2] = 40;
				RCruleMain[3] = 60;
				RCruleMain[4] = 70;
				RCruleMain[5] = 80;
				RCruleT = new int[3];
				RCruleT[0] = ARS_0;
				RCruleT[1] = ARS_0;
				RCruleT[2] = 40;
				RCruleS = new int[3];
				RCruleS[0] = ARS_0;
				RCruleS[1] = ARS_0;
				RCruleS[2] = 40;
				RCruleTS = new int[3];
				RCruleTS[0] = ARS_0;
				RCruleTS[1] = ARS_0;
				RCruleTS[2] = 40;
			}	
		}

		ApplySignalState();

		SetNameRC(sp.GetNamedTag("nameRC"));

		if (mn) {
			updateBrowser(mn);
		}
	}

	void LoadBrowserParam(Browser b){}

	void updateBrowser(Browser browser) {
		browser.LoadHTMLString(GetContentViewDetails());
		LoadBrowserParam(browser);
	}

	public void PropertyBrowserRefresh(Browser browser){
		inherited(browser);
		LoadBrowserParam(browser);
		mn=browser;//?
	}

	thread void ShowBrowser(void){
		Message msg;
		wait(){
			on "Browser-URL","",msg: {
				if(msg.src==mn and msg.minor[0,16]=="live://property/"){
					LinkPropertyValue(msg.minor[16,]);
					updateBrowser(mn);
				}
				continue;
			}
			on "Browser-Closed","",msg: if (msg.src!=mn) continue;
		}
		mn=null;
	}

	void ViewDetails(Message msg){
		mn=Constructors.NewBrowser();
		updateBrowser(mn);
		mn.SetWindowRect(100, 100, 600, 500);
		ShowBrowser();
	}

	public void Init(Asset asset){
		inherited(self=asset);
		Asset LibAsset=self.FindAsset("kmLib");
		if(!LibAsset)Interface.Exception("unable load kmLib");
		ST = LibAsset.GetStringTable();
		kmLib = cast<kmLib>World.GetLibrary(LibAsset.GetKUID());
		if (!kmLib) {
			Interface.Exception("unable load kmLib");
		}

		RCruleMain = new int[0];
		RCruleMain[0] = ARS_0;
		RCruleMain[1] = ARS_0;
		RCruleMain[2] = 40;
		RCruleMain[3] = 60;
		RCruleMain[4] = 70;
		RCruleMain[5] = 80;

		RCruleT = new int[0];
		RCruleT[0] = ARS_0;
		RCruleT[1] = ARS_0;
		RCruleT[2] = 40;

		RCruleS = new int[0];
		RCruleS[0] = ARS_0;
		RCruleS[1] = ARS_0;
		RCruleS[2] = 40;

		RCruleTS = new int[0];
		RCruleTS[0] = ARS_0;
		RCruleTS[1] = ARS_0;
		RCruleTS[2] = 40;

		AddHandler(me, "MapObject", "View-Details", "ViewDetails");
		NullSoup = Constructors.NewSoup();
	}

	public void Init(void){
	}

	public Soup DetermineUpdatedState(void){
		return NullSoup;
	}

	public void ApplySpeedLimitForStateEx(int state){
	}

	void ApplyUpdatedState(Soup signalStateSoup){
	}
};