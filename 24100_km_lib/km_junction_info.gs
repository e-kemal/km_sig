include "JunctionBase.gs"
include "km_signal.gs"
include "km_dscp_post.gs"
include "km_observable.gs"

class kmJunctionInfo isclass kmObservable, kmObserver
{
	kmDscpPost owner;
	int index;
	public string name;
	public JunctionBase JM;
	public int defaultDirection;
	public define float J_MOVING_TIME_MIN = 2.8;
	public define float J_MOVING_TIME_MAX = 3.4;
	float movingTime = Math.Rand(J_MOVING_TIME_MIN, J_MOVING_TIME_MAX);

	public define int J_PS_MINUS	= 1; //состояние поляризованного реле ПС
	public define int J_MOVING		= 2; //стрелка переводится
	public define int J_BREAK		= 4; //стрелка сломана
	public define int J_TRAIN		= 8; //секция занята подвижным составом

	int state;

	public kmSection[] sections = new kmSection[0];

	public define int EVENT_START_MOVING	= 0; //стрелка начала переводиться
	public define int EVENT_STOP_MOVING		= 1; //стрелка дошла до крайнего положения
	public define int EVENT_OCCUPY_TRACK	= 2; //секция занялась
	public define int EVENT_UNOCCUPY_TRACK	= 3; //секция освободилась

	public kmDscpPost getOwner() {
		return owner;
	}

	public int getIndex() {
		return index;
	}

	public string getHtml() {
		string ret = "<table>";
		ret = ret + "<tr><td align=\"center\">";
		ret = ret + "<font color=\"#";
		if (!(state & (J_MOVING|J_BREAK)) and (JM.GetDirection() == defaultDirection))
			ret = ret + "00ff00";
		else
			ret = ret + "000000";
		ret = ret + "\">*</font></td>";
		ret = ret + "<td align=\"center\"><font color=\"#";
		if (state & J_MOVING or ((state & J_BREAK or (JM.GetDirection() != JunctionBase.DIRECTION_LEFT and JM.GetDirection() != JunctionBase.DIRECTION_RIGHT)) and owner.getMainLib().getBlinkerState()))
			ret = ret + "ff0000";
		else
			ret = ret + "000000";
		ret = ret + "\">*</font></td>";
		ret = ret + "<td align=\"center\"><font color=\"#";
		if (!(state & (J_MOVING|J_BREAK)) and (JM.GetDirection() == JunctionBase.DIRECTION_LEFT or JM.GetDirection() == JunctionBase.DIRECTION_RIGHT) and (JM.GetDirection() != defaultDirection))
			ret = ret + "ffff00";
		else
			ret = ret + "000000";
		ret = ret + "\">*</font>";
		ret = ret + "</td></tr>";

		ret = ret + "<tr><td align=\"center\"><a href=\"live://property/j" + index + "/pu\">+</a></td>";
		ret = ret + "<td>&nbsp;</td>";
		ret = ret + "<td align=\"center\"><a href=\"live://property/j" + index + "/mu\">-</a></td></tr>";

		ret = ret + "<tr><td align=\"center\" colspan=\"3\">"+name+"</td></tr>";
		if (owner.debug) {
			ret = ret + "<tr><td align=\"center\" colspan=\"3\">PS: " + (state & J_PS_MINUS) + "</td></tr>";
			ret = ret + "<tr><td align=\"center\" colspan=\"3\">MOV: " + (state & J_MOVING) + "</td></tr>";
			ret = ret + "<tr><td align=\"center\" colspan=\"3\">BRK: " + (state & J_BREAK) + "</td></tr>";
			ret = ret + "<tr><td align=\"center\" colspan=\"3\">TR: " + (state & J_TRAIN) + "</td></tr>";
		}
		ret = ret + "</table>";
		return ret;
	}

	public bool isK() {
		if (state & (J_MOVING|J_BREAK)) {
			return false;
		}
		if (JM.GetDirection() != JunctionBase.DIRECTION_LEFT and JM.GetDirection() != JunctionBase.DIRECTION_RIGHT) {
			return false;
		}

		return true;
	}

	public bool isPK() {
		return (JM.GetDirection() == defaultDirection) and isK();
	}

	public bool isMK() {
		return (JM.GetDirection() != defaultDirection) and isK();
	}

	bool checkP(bool andSubscribe) {
		bool ret = true;
		int i;
		for (i = JunctionBase.DIRECTION_BACKWARD; i < JunctionBase.DIRECTION_NONE; ++i) {
			GSTrackSearch ts = JM.BeginTrackSearch(i);
			if (!ts) {
				continue;
			}
			while (ts.SearchNextObject()) {
				object obj = ts.GetObject();
				if (cast<Vehicle>obj) {
					ret = false;//Занято((
					if (!andSubscribe) {
						return false;
					}
				}
				kmRC sig = cast<kmRC>obj;
				if (!sig or (sig.GetMyType() & kmRC.ST_SKIP_RC)) {
					continue;
				}
				if (andSubscribe) {
					owner.getExternalObserverContainerForObject(sig).addObserver(me);
				}
				if (ret and !owner.getMainLib().checkOverlap(ts)) {
					ret = false;
					if (!andSubscribe) {
						return false;
					}
				}
				break;
			}
		}

		return ret;
	}

	public bool isP() {
		return !(state & J_TRAIN);
	}

	bool isZ() {
		int i = 0;
		while (i < sections.size()) {
			if (!sections[i].isZ()) {
				return false;
			}
			++i;
		}
		return true;
	}

	public void PU(bool force) {
		if (!(state & J_PS_MINUS)) {
			return;
		}
		if (!force and !isP())
			return;
		if (!isZ())
			return;
		state = state & ~J_PS_MINUS;
		if (JM.GetDirection() == defaultDirection)
			return;
		owner.ClearMessages("kmJunctionToggleComplete", index);
		state = state | J_MOVING;
		JM.SetDirection(null, defaultDirection);
		JM.SetDefaultDirection(null, defaultDirection);
		notifyObservers(EVENT_START_MOVING);
		owner.PostMessage(owner, "kmJunctionToggleComplete", index, movingTime);
	}
	public void PU() {
		PU(false);
	}

	public void MU(bool force) {
		if (state & J_PS_MINUS) {
			return;
		}
		if (!force and !isP())
			return;
		if (!isZ())
			return;
		state = state | J_PS_MINUS;
		if (JM.GetDirection() != defaultDirection)
			return;
		owner.ClearMessages("kmJunctionToggleComplete", index);
		state = state | J_MOVING;
		if (defaultDirection == JunctionBase.DIRECTION_RIGHT) {
			JM.SetDirection(null, JunctionBase.DIRECTION_LEFT);
			JM.SetDefaultDirection(null, JunctionBase.DIRECTION_LEFT);
		}
		else {
			JM.SetDirection(null, JunctionBase.DIRECTION_RIGHT);
			JM.SetDefaultDirection(null, JunctionBase.DIRECTION_RIGHT);
		}
		notifyObservers(EVENT_START_MOVING);
		owner.PostMessage(owner, "kmJunctionToggleComplete", index, movingTime);
	}
	public void MU() {
		MU(false);
	}

	public void ToggleComplete() {
		state = state & ~J_MOVING;
		notifyObservers(EVENT_STOP_MOVING);
	}

	public void Toggled(GameObject src) {
		if (state & J_MOVING) {		//если флаг уже стоит, значит стрелку переводим мы сами
			return;
		}
		//наче - отработаем как при обычном старте перевода
		state = state | J_MOVING;
		notifyObservers(EVENT_START_MOVING);
		owner.PostMessage(owner, "kmJunctionToggleComplete", index, movingTime);
	}

	public void handleEvent(kmObservable source, int type) {
		if (cast<kmRC>source) {
			if (state & J_TRAIN and checkP(false)) {
				state = state & ~J_TRAIN;
				notifyObservers(EVENT_UNOCCUPY_TRACK);
			}
			else if (!(state & J_TRAIN) and !checkP(false)) {
				state = state | J_TRAIN;
				notifyObservers(EVENT_OCCUPY_TRACK);
			}
		}
	}

	public void init(kmDscpPost post, int i) {
		if (!JM) {
			Interface.Exception("post " + post.getStationName() + " (" + post.getStationCode() + "), junction " + name + " object is null");
		}
		owner = post;
		index = i;
		state = 0;
		JM.AllowManualControl(false);
		GameObject JMGO = cast<GameObject>((object)JM);
		if (!JMGO.extension) {
			JMGO.extension = me;
		}
	}

	public void saveProperties(Soup soup) {
		soup.SetNamedTag("junctions.state." + index, state);
		soup.SetNamedTag("junctions.moving_time." + index, movingTime);
	}

	public void loadProperties(Soup soup) {
		state = soup.GetNamedTagAsInt("junctions.state." + index, state);
		movingTime = soup.GetNamedTagAsFloat("junctions.moving_time." + index, movingTime);

		if (state & J_MOVING) {//сохранились в момент перевода стрелки
			owner.PostMessage(owner, "kmJunctionToggleComplete", index, movingTime);
		}
		checkP(true);
	}
};
