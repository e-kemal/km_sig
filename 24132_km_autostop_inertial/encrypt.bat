@echo off
call make.bat encrypt
rename km_autostop_inertial.gs km_autostop_inertial.gs.tmp
call make.bat install
call make.bat commit
call make.bat status
rename km_autostop_inertial.gs.tmp km_autostop_inertial.gs
del *.gse