include "km_autostop.gs"

class kmAutoStopInertial isclass kmAutoStop{
	Asset self;
	float limit;

	public bool CheckSpeed(float speed){
		return Math.Fabs(speed)>limit;
	}

	public string GetDescriptionHTML(void){
		string s=inherited();
		s=s+"<table border='1' width=90%>";

		s=s+"<tr><td bgcolor='#666666'>Limit:</td><td bgcolor='#AAAAAA'><a href='live://property/limitkps'>"+(limit*3.6)+"</a>&nbsp;km/h&nbsp;(<a href='live://property/limit'>"+limit+"&nbsp;m/s</a>)</td></tr>";

		s=s+"</table>";
		return s;
	}

	string GetPropertyType(string id){
		if(id=="limitkps")return "float";
		if(id=="limit")return "float";
		return inherited(id);
	}

	void SetPropertyValue(string id, float value){
		if(id=="limitkps")limit=value/3.6;
		else if(id=="limit")limit=value;
		else inherited(id, value);
	}

	public string GetPropertyValue(string id){
		if(id=="limitkps")return (string)(limit*3.6);
		if(id=="limit")return (string)limit;
		return inherited(id);
	}

	public Soup GetProperties(){
		Soup sp=inherited();
		sp.SetNamedTag("limit", limit);
		return sp;
	}

	public void SetProperties(Soup sp){
		inherited(sp);
		limit=sp.GetNamedTagAsFloat("limit", 5.55556);
	}

	public void Init(Asset asset){
		inherited(self=asset);
	}
};