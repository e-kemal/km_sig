@echo off
call make.bat encrypt
rename km_autostop_permanent.gs km_autostop_permanent.gs.tmp
call make.bat install
call make.bat commit
call make.bat status
rename km_autostop_permanent.gs.tmp km_autostop_permanent.gs
del *.gse