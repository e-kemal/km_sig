
kuid                                    <kuid:216981:24131>
kind                                    "scenery"
category-class                          "WA"
category-region                         "BY;RU;SU;UA"
category-era                            "1930s;1940s;1950s;1960s;1970s;1980s;1990s;2000s;2010s"
username                                "km_AutoStop_permanent"
script                                  "km_autostop_permanent.gs"
class                                   "kmAutoStopPermanent"
trainz-build                            3.5
trackside                               1

mesh-table
{
  default
  {
    mesh                                "km_autostop_permanent.im"
    auto-create                         1
  }
}

script-include-table
{
  kmLib                                 <kuid:216981:24100>
}

kuid-table
{
  kmLib                                 <kuid:216981:24100>
}

thumbnails
{
}
